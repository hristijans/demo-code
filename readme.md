#   Why this workflow
- Code should be separated and be responsible only what is ment for.
- Codebase in this project is separated in few sections
1. Controllers
    - The first request always hits the controller. Controller responsibility is to intercept the request and pass to the next logic. After the request is recived it is validated

2. Requests
    - Requests are responsible to validate the response. They only validate presence of data, not bussines logic
    
3. Services
    - After request being validated it is passed to the service. Service is responsible for all bussines logic regarding the request. Services comunicate with repositories (database, external api ...). When data is processed it is returned to the controller

4. Repositories
    - Repositories are responsible for data manipulation. With repositories data is read, stored or updated into DB, or external APIs are called. Then data is returned to the service for additional manipulation
    
5. Responses
    - Responses are used only to return data in json format. Whether collection or entity should be displayed resources handle it.
    
6. ServiceProviders
    - Service Providers are very powerfull feature of Laravel. IN the service providers we determinate implementation of concrete classes with interfaces. Dependency injection make code more testable.    
