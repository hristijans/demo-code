<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }


    public function testCannotCreateUnauthorizedUser()
    {
        $data = [
            'title' => 'Test Product',
            'category_id' => 1
        ];

        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/products', $data);

      

        $response
            ->assertStatus(404);
    }

    public function testValidationFailWhenCreateNewProduct()
    {
        $user = \App\User::first();

       
        $data = [
            'title' => 'Test title',
            'content' => 'Test desc',
        ];

        
        $this->actingAs($user, 'api')
            ->post(route('products.create'), $data)
            ->assertStatus(422);
    }

    public function testCreateNewProduct()
    {
        $user = \App\User::first();

        $category = \App\Category::first();

       
        $data = [
            'title' => 'Test title',
            'category_id' => $category->id,
            'lat' => '41.3285454',
            'lng' => '21.5499996',
            'price_per_day' => '200',
            'deposit' => '100',
            'available_from' => '2018-01-01',
            'available_to' => '2018-05-05',
            'hour_from' => '10:00',
            'hour_to' => '15:00',
           //'images' => ''
        ];

        
        $this->actingAs($user, 'api')
            ->post(route('products.create'), $data)
            ->assertStatus(201);
    }
}
