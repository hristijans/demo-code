<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Help;
use Faker\Generator as Faker;

$factory->define(Help::class, function (Faker $faker) {
    return [
        'title'         => ucfirst($faker->domainWord),
        'content'       => $faker->randomHtml(2,3),
        'display_order' => $faker->randomDigitNotNull
    ];
});
