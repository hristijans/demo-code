<?php

use App\About;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory('App\About', 5)->create();

        DB::table('abouts')->truncate();
        DB::table('about_translations')->truncate();

        $about = new About();
        $about->title = 'About rentout';
        $about->content = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa, temporibus mollitia animi sit in voluptatum non dolore officia numquam quo pariatur reprehenderit fuga, repudiandae labore cum at ratione blanditiis.';
        $about->display_order = 1;
        $about->save();
        
    }
}
