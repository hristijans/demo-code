<?php

use App\Help;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HelpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('helps')->truncate();
        DB::table('help_translations')->truncate();
        
        factory(Help::class, 8)->create();
    }
}
