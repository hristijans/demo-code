<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        //Plumbing Tools, Sewing Tools, Carpenter Tools etc. 

        $categories = [
            'Plumbing Tools',
            'Sewing Tools',
            'Carpenter Tools',
            'Industrial Machines',
            'Computer tooles'
        ];

        foreach ($categories as $key => $category) {
            Category::create(['name' => $category , 'description' => 'Lorem Ipsum', 'icon'=>'null']);
        }
    }
}
