<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppFeedbackCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_feedback_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('feedback_id');
            $table->integer('user_id');
            $table->text('comment');
            $table->timestamps();


            // $table->foreign('user_id')
            //   ->references('id')->on('users')
            //   ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_feedback_comments');
    }
}
