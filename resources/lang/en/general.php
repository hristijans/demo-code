<?php

return [
    'resource_exists' => 'Resource exists',
    'resource_deleted' => 'Resource deleted',
    'resource_created' => 'Resource created',
    'resource_updated' => 'Resource updated',
    'resource_not_exists' => 'Resource does not exist',

    'position_updated' => 'Position was updated',
    'category_has_child' => 'Category cannot be deleted. It has subcategories',
    'category_has_product' => 'Category cannot be deleted. It has products',

    'upload_error_big_image' => 'Upload limit exceeded',
];
