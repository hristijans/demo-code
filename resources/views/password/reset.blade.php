<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

    <title>Password reset</title>
</head>
<body>

    <div class="flex container mx-auto mt-10">
        <form action="/password/reset" method="post">
        @csrf
            <input type="hidden" name="token" value="{{$token}}" />

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">

                <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    Password
                </label>
                <input name="password" class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3" id="password" type="password" placeholder="******************">

                </div>

                <div class="mb-6">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                    Password confirm
                </label>
                <input name="password_confirm" class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3" id="password" type="password" placeholder="******************">

                </div>
                
                
                <div class="flex items-center justify-between">
                <button class="bg-blue-300 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded" type="submit">
                    Submit
                </button>
                </div>
            </div>
        </form>
    </div>
    
</body>
</html>