<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Help\Contracts\HelpInterface;
use App\Repositories\Help\Eloquent\HelpRepository;
use App\Repositories\User\Contracts\UserInterface;
use App\Repositories\User\Eloquent\UserRepository;

use App\Repositories\About\Contracts\AboutInterface;
use App\Repositories\About\Eloquent\AboutRepository;
#use App\Repositories\User\File\UserRepository;

use App\Repositories\Order\Contracts\OrderInterface;
use App\Repositories\Order\Eloquent\OrderRepository;
use App\Repositories\Charge\Contracts\ChargeInterface;
use App\Repositories\Charge\Eloquent\ChargeRepository;
use App\Repositories\Chat\Room\Contracts\RoomInterface;
use App\Repositories\Chat\Room\Eloquent\RoomRepository;
use App\Services\Billing\Checkout as CheckoutInterface;
use App\Repositories\Address\Contracts\AddressInterface;
use App\Repositories\Address\Eloquent\AddressRepository;
use App\Repositories\Product\Contracts\ProductInterface;
use App\Repositories\Product\Eloquent\ProductRepository;
use App\Repositories\Category\Contracts\CategoryInterface;
use App\Repositories\Category\Eloquent\CategoryRepository;
use App\Repositories\Chat\Room\Contracts\MessageInterface;

//* billing */
use App\Repositories\Chat\Room\Eloquent\MessageRepository;
use App\Repositories\Favorite\Contracts\FavoriteInterface;
use App\Repositories\Favorite\Eloquent\FavoriteRepository;
use App\Repositories\Feedback\Contracts\FeedbackInterface;
use App\Repositories\Feedback\Eloquent\FeedbackRepository;
use App\Services\Billing\Stripe\Checkout as StripeCheckout;
use App\Repositories\Statistics\Contracts\StatisticInterface;
use App\Repositories\Statistics\Eloquent\StatisticRepository;
use App\Repositories\CreditCard\Contracts\CreditCardInterface;
use App\Repositories\CreditCard\Eloquent\CreditCardRepository;
use App\Repositories\Feedback\Contracts\CommentInterface as FeedbackCommentInterface;
use App\Repositories\Feedback\Eloquent\CommentRepository as FeedbackCommentRepository;

class RepositoriesServiceProvider extends ServiceProvider
{

    protected $repositories = [
        UserInterface::class        => UserRepository::class,
        AddressInterface::class     => AddressRepository::class,
        CategoryInterface::class    => CategoryRepository::class,
        ProductInterface::class     => ProductRepository::class,
        AboutInterface::class       => AboutRepository::class,
        HelpInterface::class        => HelpRepository::class,
        FavoriteInterface::class    => FavoriteRepository::class,
        FeedbackInterface::class    => FeedbackRepository::class,
        OrderInterface::class       => OrderRepository::class,
        ChargeInterface::class      => ChargeRepository::class,
        //CheckoutInterface::class    => StripeCheckout::class,
        CreditCardInterface::class  => CreditCardRepository::class,
        FeedbackCommentInterface::class     => FeedbackCommentRepository::class,
        RoomInterface::class            => RoomRepository::class,
        MessageInterface::class         => MessageRepository::class,
        StatisticInterface::class       => StatisticRepository::class,

    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $interface => $repository) {
            $this->app->bind($interface, function ($app) use ($repository) {
                return new $repository;
            });
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
