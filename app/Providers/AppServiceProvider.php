<?php

namespace App\Providers;

use App\User;
use App\Product;
use App\Feedback;
use App\Observers\UserObserver;
use App\Observers\ProductObserver;
use App\Observers\FeedbackObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Product::observe(ProductObserver::class);
        Feedback::observe(FeedbackObserver::class);
    }
}
