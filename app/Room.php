<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['user_id', 'message_id', 'slug', 'title', 'created_by', 'identifier'];

    public function room_slug($emails)
    {
        asort($emails);

        return implode("", $emails);
    }

    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('id', 'desc');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'room_users');
    }

}
