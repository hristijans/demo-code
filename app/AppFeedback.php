<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppFeedback extends Model
{
    protected $filable = ['user_id', 'rating', 'comment'];
    

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(AppFeedbackComment::class, 'feedback_id');
    }
}
