<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'description', 'password' , 'api_token', 'username', 'show_location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'rating'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'room_users');
    }

    public function address()
    {
        return $this->belongsToMany(Address::class, 'user_addresses', 'user_id', 'address_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getRatingAttribute()
    {
        $num_products = $this->products()->withTrashed()->whereNotNull('avg_rating')->count();
        $sum_rating   = $this->products()->withTrashed()->whereNotNull('avg_rating')->sum('avg_rating');

        return [
            'rating'        => $num_products > 0 ? ceil($sum_rating/$num_products) : 0,
            'num_ratings'   => $num_products
        ];

    }

    public function getIsReportedAttribute()
    {
        if (\Auth::user()) {
           return UserReport::where('user_id', \Auth::user()->id)
                ->where('reported_user_id', $this->id)
                ->get()
                ->count();
        }
        return 0;
    }

    // public function rooms()
    // {
    //     return $this->belongsToMany(Room::class, 'room_users');
    // }

}
