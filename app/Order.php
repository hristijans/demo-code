<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Feedback;
use Carbon\Carbon;
class Order extends Model
{
    /**
     * in_progress - order is created before it's charged
     * piad        - order is updated  after successful charge
     * failed      - charge failed 
     * completed   - order is done - product can be released to be listed again
     */ 
    const IN_PROGRESS = 'in_progress';
    const PAID        = 'paid';
    const COMPLETED   = 'completed';
    const FAILED      = 'failed';

    

    protected $fillable = [
        'product_id',
        'user_id',
        'from_date',
        'to_date',
        'amount',
        'deposit',
        'fee',
        'status',
        'charge_id',
        'message',
    ];

    protected $appends = ['total_paid', 'is_rated', 'number_of_days'];

    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function product() 
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function getTotalPaidAttribute()
    {
        return (int) $this->amount + $this->deposit + $this->fee;
    }

    public function getIsRatedAttribute()
    {
        return Feedback::where([
            'user_id' => $this->user_id, 
            'product_id' => $this->product_id
        ])->count();
    }

    public function getNumberOfDaysAttribute()
    {
        $_from_date = Carbon::parse($this->from_date);
        $_to_date = Carbon::parse($this->to_date);

       return $_to_date->diffInDays($_from_date);
    }
}
