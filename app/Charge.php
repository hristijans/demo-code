<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = [
        'charge_id',
        'amount',
        'fee',
        'transaction_id',
        'description',
        'payment_method',
    ];
}
