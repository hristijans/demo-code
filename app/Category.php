<?php

namespace App;


use App\CategoryTranslation;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;
    
    public $translatedAttributes = ['name'];
    public $translationModel = CategoryTranslation::class;
    
    protected $fillable = ['parent_id', 'description', 'icon', 'status'];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function tools()
    {
        return $this->hasMany(Tool::class);
    }
}
