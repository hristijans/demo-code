<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use Translatable;
    
    public $translatedAttributes = ['title', 'content'];
    public $translationModel = AboutTranslation::class;
}
