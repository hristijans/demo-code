<?php 
namespace App\QueryFilters;
use Closure;

class OrderBy extends Filter
{
    protected $field;

    public function __construct($field = 'id')
    {
        $this->field = $field;
    }
    public function setFilter($builder)
    {   
       dd($builder->orderBy($this->field, request($this->getFilter())));
    }
}