<?php 
namespace App\QueryFilters;
use Closure;
use Illuminate\Support\Str;

abstract class Filter 
{

    public function handle($request, Closure $next)
    {
     
        if ( ! request()->has($this->getFilter()) ) {
            return $next($request);
        }   

        $builder = $next($request);

        return $this->setFilter($builder);
    }



    public function getFilter()
    {
       return Str::snake(class_basename($this));
    }
}