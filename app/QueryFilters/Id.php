<?php 
namespace App\QueryFilters;
use Closure;

class OrderBy extends Filter
{
    public function setFilter($builder)
    {   
        return $builder->where($this->getColumn(), request('order_by'));
    }
}