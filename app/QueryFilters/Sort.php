<?php
namespace App\QueryFilters;
use Closure;

class Sort extends Filter
{


    public function setFilter($builder)
    {
        return $builder->orderBy($this->getColumn(), request('sort'));
    }
}
