<?php 

namespace App\Services;

use App\Repositories\Help\Eloquent\HelpRepository;


class HelpService {

    protected $helpRepository;

    public function __construct(HelpRepository $helpRepository)
    {
        $this->helpRepository = $helpRepository;
    }

    public function get()
    {
        return $this->helpRepository->get();
    }

    public function search($term)
    {
        return $this->helpRepository->search($term);
    }
 
}