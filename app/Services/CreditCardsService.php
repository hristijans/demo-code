<?php

namespace App\Services;

use Stripe\Stripe;
use App\Repositories\CreditCard\Eloquent\CreditCardRepository;

class CreditCardsService {

    protected $creditCardRepository;

    public function __construct(CreditCardRepository $creditCardRepository)
    {
        $this->creditCardRepository = $creditCardRepository;
    }

    public function create($data)
    {

        $customer = \Auth::user()->stripe_id;

        $token = $data['token'];

        $card = \Stripe\Customer::createSource(
            $customer,
            [
              'source' => $token,
            ]
        );

        $card_data = [
             'user_id' => \Auth::user()->id,
             'token' => $card['id'],
             'brand' => $card['brand'],
             'type' => $card['type'],
             'last4' => $card['last4'],
             'exp_year' => $card['exp_year'],
             'exp_month' => $card['exp_month'],
        ];

        return $this->creditCardRepository->create($card_data);


    }

    public function delete($id)
    {
        return $this->creditCardRepository->delete($id);
    }

    public function search($params)
    {
        return $this->creditCardRepository->search($params);
    }

}
