<?php

namespace App\Services;




use App\Repositories\Feedback\Eloquent\FeedbackRepository;

class FeedbacksService {

    protected $feedbackRepository;

    public function __construct(FeedbackRepository $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }


    public function find($id)
    {
        return $this->feedbackRepository->find($id);
    }

    public function search($params)
    {
        $params['product_id'] = $params['id'];
        return $this->feedbackRepository->search($params);
    }

    public function get()
    {
        $params = [];
        $params['user_id'] = \Auth::user()->id;

        return $this->feedbackRepository->search($params);
    }

    public function tenant()
    {
        $params = [];
        $params['type'] = 'tenant';
        $params['user_id']  = \Auth::user()->id;

        return $this->feedbackRepository->search($params);
    }

    public function owner()
    {
        $params = [];
        $params['type'] = 'owner';
        $params['user_id']  = \Auth::user()->id;

        return $this->feedbackRepository->search($params);
    }

    public function create($data)
    {
        $data['user_id'] = \Auth::user()->id;
        return $this->feedbackRepository->store($data);
    }


}
