<?php

namespace App\Services;

use App\Repositories\About\Eloquent\AboutRepository;


class AboutService {

    protected $aboutRepository;

    public function __construct(AboutRepository $aboutRepository)
    {
        $this->aboutRepository = $aboutRepository;
    }

    public function get()
    {
        return $this->aboutRepository->get();
    }


}
