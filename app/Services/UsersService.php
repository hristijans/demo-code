<?php

namespace App\Services;

use App\Repositories\Chat\Room\Contracts\RoomInterface;
use App\Repositories\Order\Contracts\OrderInterface;
use App\Repositories\Product\Contracts\ProductInterface;
use App\User;
use App\Role;
use DB;

use App\Repositories\User\Contracts\UserInterface;

class UsersService {


    protected $userRepository;
    protected $productRepository;
    protected $orderRepository;
    protected $roomRepository;

    public function __construct(
        UserInterface $userRepository,
        ProductInterface $productRepository,
        OrderInterface $orderRepository,
        RoomInterface $roomRepository
        )
    {
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->roomRepository = $roomRepository;
    }

    public function all()
    {
        return $this->userRepository->getAllUsers();
    }

    public function create( $data )
    {
        return $this->userRepository->create($data);
    }

    public function admin()
    {
        return $this->userRepository->getAdmin();
    }

    /**
     * Login user with email and password
     * @param string email - $email that is logged with
     * @return string - token
     */
    public function login($email)
    {
        return $user = User::where([
            'email' => $email,
        ])->orWhere(['username'=> $email])->get()->first();
    }

    /**
     * Attach address recort for user
     *
     * @param App\User $user
     * @param App\Address $address
     * @return void
     */
    public function mapAddress($user, $address)
    {
        $this->userRepository->mapAddress($user, $address);
    }


    /**
     * Update user's profile
     *
     * @param Array $data
     * @return void
     */
    public function update(Array $data)
    {
        $user = $this->userRepository->current();

        return $this->userRepository->update($user->id, $data);

    }

    /**
     * Update user's avatar
     *
     * @param string $avatar
     * @return string avatar path
     */
    public function avatar(string $avatar)
    {
        $user = $this->userRepository->current();

        return $this->userRepository->avatar($user->id, $avatar);
    }

    /**
     * Update user's phone
     *
     * @param string $phone
     * @return string phone path
     */
    public function updatePhone(string $phone)
    {
        $user = $this->userRepository->current();

        return $this->userRepository->updatePhone($user->id, $phone);
    }


    /**
     * Undocumented function
     *
     * @param Array $data
     * @return void
     */
    public function updatePassword(Array $data)
    {
        return $this->userRepository->updatePassword($data);
    }


    /**
     * updatePosition
     *
     * @param Array $data
     * @return void
     */
    public function updatePosition($data)
    {
        $user = $this->userRepository->current();

        return $this->userRepository->updatePosition($user->id, $data['show_location']);
    }

    public function details($id)
    {
        return $this->userRepository->details($id);
    }

    public function getOwnerHomeData()
    {
        $user_id = \Auth::user()->id;

        return [
            'ongoing_rent' => $this->orderRepository->getOngoingOrdersByUserId($user_id),
            'products' => $this->productRepository->getProductsByUserId($user_id),
            'messages' => $this->roomRepository->getUserRooms($user_id)->load('user', 'message')->take(5)
        ];

    }
}
