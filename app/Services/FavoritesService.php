<?php 

namespace App\Services;

use App\Services\UsersService;


use App\Exceptions\ExistsException;

use App\Exceptions\GeneralException;
use App\Repositories\User\Eloquent\UserRepository;
use App\Repositories\Favorite\Eloquent\FavoriteRepository;


class FavoritesService {

    protected $favoriteRepository;
    protected $userRepository;

    public function __construct(FavoriteRepository $favoriteRepository, UserRepository $userRepository)
    {
        $this->favoriteRepository = $favoriteRepository;
        $this->userRepository = $userRepository;
    }

    public function get($params)
    {
        $user = $this->userRepository->current();
        $params['user_id'] = $user->id;

        return $this->favoriteRepository->get($params);
    }

    public function create($product_id)
    {
        $user = $this->userRepository->current();

        if ($this->favoriteRepository->exists($user->id, $product_id) > 0) {
            throw (new GeneralException(__('general.resource_exists')));        
        }

        return $this->favoriteRepository->create($user->id, $product_id);
    }

    public function delete($product_id)
    {
        $user = $this->userRepository->current();

        return $this->favoriteRepository->delete($user->id, $product_id);
    }

 
}