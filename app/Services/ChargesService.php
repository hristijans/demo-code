<?php
namespace App\Services;

use App\Services\Billing\Checkout;
use App\Services\Billing\PaymentManager;
use App\Repositories\Charge\Contracts\ChargeInterface;

class ChargesService
{
    protected $chargeRepository;
    protected $checkout;

    public function __construct(ChargeInterface $chargeRepository)
    {
        $this->chargeRepository = $chargeRepository;
    }

    public function create($data)
    {
        $payment = PaymentManager::create($data['payment_type']);

        $payment->amount($data['amount']);
        $payment->token($data['payment_token']);
        $payment->user(\Auth::user());

        $checkout = $payment->pay();

        $charge_data = [
            'charge_id' => $checkout->id,
            'amount'    => $checkout->amount,
            'fee'       => 0,
            'transaction_id' => $checkout->balance_transaction,
            'description'   => $checkout->description ? $checkout->description : "n/a",
            'payment_method' => $checkout->payment_method
        ];


        $charge = $this->chargeRepository->create($charge_data);

        return $charge->id;
    }
}
