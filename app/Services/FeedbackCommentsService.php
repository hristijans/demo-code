<?php 
namespace App\Services;

use App\Repositories\Feedback\Contracts\CommentInterface;

class FeedbackCommentsService
{
    protected $feedbackCommentRepository;

    public function __construct(CommentInterface $feedbackCommentRepository)
    {
        $this->feedbackCommentRepository = $feedbackCommentRepository;
    }

    public function search($params)
    {

        return $this->feedbackCommentRepository->search($params);
    }

    public function create($params)
    {
        
        $params['user_id'] = \Auth::user()->id;
        $params['feedback_id'] = $params['id'];

        return $this->feedbackCommentRepository->create($params);
    }
}