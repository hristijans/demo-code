<?php 

namespace App\Services;

use App\Repositories\Product\Contracts\ProductInterface;
use App\Repositories\Favorite\Contracts\FavoriteInterface;




class ProductsService {


    protected $productRepository;
    protected $favoriteRepository;

    public function __construct(ProductInterface $productRepository, FavoriteInterface $favoriteRepository)
    {
        $this->productRepository = $productRepository;
        $this->favoriteRepository = $favoriteRepository; 
    }


    public function find($id)
    {
        return $this->productRepository->find($id);
    }

    public function get($id)
    {
        return $this->productRepository->search($params);
    }


    public function create($data)
    {
        
        $product = $this->productRepository->create($data);

        //upload images for this product
        $this->productRepository->addImages($product->id, $data);

        return $product;
    }

    public function update($id, $data)
    {
        $product = $this->productRepository->update($id, $data); 

        return $product;
        //return $this->find($product->id);
    }


    public function search($params)
    {
        return $this->productRepository->search($params);
    }

    public function delete($product_id)
    {
        $this->favoriteRepository->deleteByProductId($product_id);
        $this->productRepository->delete($product_id);
    }

    public function addImage($params)
    {  
        return $this->productRepository->addImage($params['product_id'], $params['image']);
    }

    public function removeImage($params)
    {
        return $this->productRepository->removeImage($params['product_id'], $params['id']);
    }
}