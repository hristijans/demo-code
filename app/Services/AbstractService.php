<?php 

namespace App\Services;

abstract class AbstractService
{
    protected $repository;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    public function findById($id)
    {
        return $this->repository->findOrFail($id);
    }

    public function findByColumn($column, $value)
    {
        return $this->repository->where($column, $value);
    }

    public function create($data)
    {
        return $this->repository->create($data);
    }

    public function update($id, $data)
    {
        return $this->repository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->repository->delete($delete);
    }



}