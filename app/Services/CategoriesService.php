<?php

namespace App\Services;

use App\Repositories\Category\Contracts\CategoryInterface;

class CategoriesService {

    protected $categoryRepository;
    
    public function __construct(CategoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function find($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function create($data)
    {    
        return $this->categoryRepository->create($data);
    }

    public function update($id, $data)
    {
        return $this->categoryRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }

    public function search($params)
    {
        return $this->categoryRepository->search($params);
    }
}