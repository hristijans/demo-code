<?php

namespace App\Services;

use Carbon\Carbon;
use App\Services\ChargesService;
use App\Exceptions\GeneralException;
use App\Repositories\User\Contracts\UserInterface;
use App\Repositories\Order\Contracts\OrderInterface;
use App\Repositories\Charge\Contracts\ChargeInterface;
use App\Repositories\Product\Contracts\ProductInterface;


class OrdersService {

    protected $chargesService;

    protected $orderRepository;
    protected $chargeRepository;
    protected $userRepository;
    protected $productRepository;

    private $_charge_id;
    private $_product;
    private $_user;

    public function __construct(
        OrderInterface $orderRepository,
        ChargesService $chargesService,
        UserInterface $userRepository,
        ProductInterface $productRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->chargesService = $chargesService;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;

    }

    /**
     * Search for orders
     * @param Array $params
     * @return void
     */
    public function search($params)
    {
      return  $this->orderRepository->search($params);
    }

    /**
     * Get order by id
     * @param Array $params
     * @return void
     */
    public function find($id)
    {
      return  $this->orderRepository->find($id);
    }

    /**
     * Create new order
     * @param Array $data
     * @return void
     */
    public function create($data)
    {
        $this->_product = $this->productRepository->find($data['product_id']);

        $this->_user = $this->userRepository->current();

        // if($this->_product->is_rented != 0) {
        //     throw (new GeneralException(__('Product is already rented.')));
        //     return;
        // }

        $order = $this->orderRepository->create($this->_get_order_data($data));

        $_payment_data = [];
        $_payment_data['payment_token'] = $data['payment_token'];
        $_payment_data['amount']        = $this->_get_order_amount($order);
        $_payment_data['payment_type']  = $data['payment_type'];

        $this->_charge_id = $this->chargesService->create($_payment_data);

        if(!$this->_charge_id) {
            $this->orderRepository->update($order->id, ['status' => 'fail']);
            throw (new GeneralException(__('Payment failed.')));
        }

        $this->orderRepository->update($order->id, ['status' => 'paid', 'charge_id' => $this->_charge_id]);

        $this->productRepository->update($this->_product->id, ['is_rented' => '1']);

        return $order;
    }

    public function tenant_orders()
    {
        $user = $this->userRepository->current();
        return $this->orderRepository->get_tenant_orders($user->id);
    }

    public function owner_orders()
    {
        $user = $this->userRepository->current();
        return $this->orderRepository->get_owner_orders($user->id);
    }

    /**
     * Delete order by id
     * @param Integer $id
     * @return void
     */
    public function delete($id)
    {

    }

    public function preview($data)
    {
        $this->_product = $this->productRepository->find($data['product_id']);

        return [
            'product' => $this->_product,
            'from_date' => $data['from_date'],
            'to_date' => $data['to_date'],
            'number_of_days' => $this->_calculate_num_of_days($data['from_date'], $data['to_date']),
            'price_per_day' => $this->_product->price_per_day,
            'deposit' => $this->_product->deposit,
            'rating' => $this->_product->avg_rating,
            'fee' => 0,
            'total' => $this->_calculate_amount($data['from_date'], $data['to_date']) + $this->_product->deposit

        ];
    }


    /**
     * busy_dates get order dates for the product
     *
     * @param int $id
     * @return $dates array
     */
    public function busy_dates($id)
    {
        $dates = [];

        $results = $this->orderRepository->get_reserved_dates_by_product($id);

        foreach ($results as $key => $date) {
            $dates[] = ['from' => $date['from_date'], 'to' => $date['to_date']];
        }

        return $dates;
    }

    private function _get_order_data($data)
    {
        return [
            'product_id' => $data['product_id'],
            'user_id' => $this->_user->id,
            'from_date' => $data['from_date'],
            'to_date' => $data['to_date'],
            'amount'=> $this->_calculate_amount($data['from_date'], $data['to_date']),
            'deposit' => $this->_product->deposit,
            'fee'=> 0,
            'status' => 'in_progress',
            'message' => $data['message']
        ];
    }

    private function _calculate_amount($from_date, $to_date)
    {
        $diff = $this->_calculate_num_of_days($from_date, $to_date);

        return  abs($diff)*$this->_product->price_per_day;
    }

    private function _calculate_num_of_days($from_date, $to_date)
    {
        $_from_date = Carbon::parse($from_date);
        $_to_date = Carbon::parse($to_date);

       return $_to_date->diffInDays($_from_date);

    }

    private function _get_order_amount($order)
    {
        $from_date = $order['from_date'];
        $to_date = $order['to_date'];

        $diff = $this->_calculate_num_of_days($from_date, $to_date);
        $diff = $diff == 1 ? 2 : $diff;
        return  abs($diff)*$order['amount'] + $order['deposit'];
    }



}
