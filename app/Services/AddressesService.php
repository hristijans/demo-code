<?php 

namespace App\Services;

use App\Repositories\Address\Eloquent\AddressRepository;
use App\Repositories\User\Eloquent\UserRepository;

class AddressesService {

    protected $addressRepository;
    protected $userRepository;

    public function __construct(AddressRepository $addressRepository, UserRepository $userRepository)
    {
        $this->addressRepository = $addressRepository;
        $this->userRepository = $userRepository;
    }

    public function get()
    {
        $user = $this->userRepository->current();

        return $user->address;
    }

    public function find($id)
    {
        return $this->addressRepository->find($id);
    }

    public function create( $data )
    {
        return $this->addressRepository->create($data);
    }

    public function update($id, $data)
    {
        return $this->addressRepository->update($id, $data);
    }



 
}