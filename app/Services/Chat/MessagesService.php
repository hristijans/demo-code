<?php 
namespace App\Services\Chat;

use App\Repositories\Chat\Room\Contracts\MessageInterface;

class MessagesService
{
    protected $messageRepository;

    public function __construct(MessageInterface $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function create($data)
    {
        return $this->messageRepository->create($data);
    }
}