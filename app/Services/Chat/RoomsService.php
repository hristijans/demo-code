<?php 
namespace App\Services\Chat;

use App\Room;
use App\Message;
use Illuminate\Support\Facades\DB;
use App\Repositories\Chat\Room\Contracts\RoomInterface;
use App\Repositories\Chat\Room\Contracts\MessageInterface;
#use App\Repositories\Chat\Room\Eloquent\RoomRepository;

class RoomsService
{

    protected $participants = [];
    protected $roomRepository;
    protected $messageRepository;

    public function __construct(RoomInterface $roomRepository, MessageInterface $messageRepository)
    {
        $this->roomRepository = $roomRepository;
        $this->messageRepository = $messageRepository;
    }

    public function userRooms($user_id)
    {
        return $this->roomRepository->getUserRooms($user_id);
    }

    public function search($params)
    {

    }

    public function create($data)
    {
        $user_id = $data['user_id'];
      
        $uniq       = $this->_get_uniq_key($user_id);
        $room       = [];
        $message    = [];
        $room       = $this->roomRepository->getByIdentifier(md5($uniq));
    

        if ($room->count() == 0) {

            $room_data = [
                'identifier' =>  md5($uniq),
                'user_id' => 0,
                'message_id' => 0,
                'title' => 'Rentout chat room',
                'slug' => $uniq,
                'created_by' => \Auth::user()->id
            ];

            $room = $this->roomRepository->create($room_data); 
            
            foreach ($this->participants as $key => $participant) {
              
                DB::table('room_users')->insert([
                    'user_id' => $participant,
                    'room_id' => $room->id
                ]);
            }
        }  else {
            $room = $room->first();
        }

        $message = $this->messageRepository->create([
            'user_id' => \Auth::user()->id,
            'room_id' => $room->id,
            'message' => $data['message'],
        ]);
    

        $update_room_data = [
            'user_id'       => \Auth::user()->id,
            'message_id'    => $message->id
        ];
        
        $room = $this->roomRepository->update($room->id, $update_room_data);

        return $room;
    }

    public function update($id, $data)
    {

    }

    public function delete($id)
    {

    }

    private function _get_uniq_key($participants)
    {
        $current_user = \Auth::user()->id;
        $users = [];

        if (is_array($participants)) {     
            array_push($participants, $current_user);
            $users = $participants;
        } else {
            $users = [$current_user, $participants];
        }

        $this->participants = $users;
        sort($users);

        return  implode("_", $users);
    }
}