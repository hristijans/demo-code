<?php 

namespace App\Services\Billing;

use App\Services\Billing\Stripe\BancontactPayment;
use App\Services\Billing\Stripe\CreditCardPayment;

class PaymentManager
{
    static public function create($method)
    {
        switch ($method) {
            case "creditcard":
                return new CreditCardPayment();
                break;
            case "bancontact":
                return new BancontactPayment();
                break;
          
            default:
              
        }
    }
}