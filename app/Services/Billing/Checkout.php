<?php 

namespace App\Services\Billing;

interface Checkout
{
    public function pay();

    public function amount($amount);

    public function token($token);

    public function user($data);
}