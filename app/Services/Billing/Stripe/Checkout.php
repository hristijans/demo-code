<?php

namespace App\Services\Billing\Stripe;

use Stripe\Stripe;
use App\Services\Billing\Checkout as CheckoutInterface;


class Checkout implements CheckoutInterface
{
    protected $token;
    protected $amount;
    protected $user;

    public function pay()
    {

        // `source` is obtained with Stripe.js; see https://stripe.com/docs/payments/accept-a-payment-charges#web-create-token
       $charge = \Stripe\Charge::create([
            'amount' => $this->amount,
            'currency' => 'eur',
            'source' => $this->token,
            'customer' => $this->user->stripe_id,
            'description' => 'Rentout payment',
        ]);

        return $charge;

    }

    public function method($method) {}

    public function amount($amount)
    {
        $this->amount = $amount;
    }

    public function token($token)
    {
        $this->token = $token;
    }

    public function user($user)
    {
        $this->user = $user;
    }
}
