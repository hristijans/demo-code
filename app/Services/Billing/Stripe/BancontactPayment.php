<?php

namespace App\Services\Billing\Stripe;

use Stripe\Stripe;
use App\Services\Billing\AbstractPayment;


class BancontactPayment extends  AbstractPayment
{


    public function pay()
    {

        // `source` is obtained with Stripe.js; see https://stripe.com/docs/payments/accept-a-payment-charges#web-create-token
       $charge = \Stripe\Charge::create([
            'amount'    => $this->amount,
            'currency'  => 'eur',
            'source'    => $this->token,
        ]);

        return $charge;

    }

}
