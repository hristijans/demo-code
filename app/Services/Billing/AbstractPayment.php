<?php 
namespace App\Services\Billing;
use App\Services\Billing\Checkout as CheckoutInterface;

abstract class AbstractPayment implements CheckoutInterface
{
    protected $token;
    protected $amount;
    protected $user;

    
    abstract function pay();

    public function amount($amount)
    {
        $this->amount = $amount;
    }

    public function token($token) 
    {
        $this->token = $token;
    }

    public function user($user)
    {
        $this->user = $user;
    }



}