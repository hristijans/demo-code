<?php 

namespace App\Services;

use App\Repositories\Country\Eloquent\CountryRepository;

class CountriesService {

    protected $coutryRepository;

    public function __construct(CountryRepository $coutryRepository)
    {
        $this->coutryRepository = $coutryRepository;
    }

    public function get($params)
    {
        return $this->coutryRepository->get($params);
    }

 
}