<?php

namespace App;
use App\Favorite;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    
    
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'category_id',
        'is_rented',
        'lat',
        'lng',
        'price_per_day',
        'deposit',
        'available_from',
        'available_to',
        'hour_from',
        'hour_to',
        'status',
    ];
 
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class);
    }

    public function scopeDistance($query, $lat, $lng, $radius = 25)
    {          
            
        $haversine = "( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) )";           
       
        $query->select("*");
        $query->selectRaw("{$haversine} AS distance");
        $query->whereRaw("{$haversine} < ?", [$radius]);

    }

    public function scopeWithRented($query)
    {     
        $query->whereIn('is_rented', [0, 1]);
    }

    public function getIsFavoriteAttribute()
    {
        $user_id = \Auth::user()->id;
        $product_id = $this->id;

        return (bool)(Favorite::where([
            'user_id'       => $user_id,
            'product_id'    => $product_id
        ])->count()) ? true : false;
    }

    public function getDistanceAttribute()
    {
        $lat = \Auth::user()->address()->first()->lat;
        $lng = \Auth::user()->address()->first()->lng;

        $haversine = "( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) )"; 
    
        $query = Product::query();
        $query->where('id', $this->id);
        $query->selectRaw("{$haversine} AS distance_km");
        $result = $query->get()->first();

        return round($result->distance_km, 2);
    }

    public function getArchived()
    {
       // return is_null($this->deleted_at) ? true : false;
       return true;
    }

    // public function setIsFavoriteAttribute($value)
    // {
    //     $this->is_favorite = $value;
    // }
    // public function setDistanceAttribute($attr)
    // {
    //     $this->distance = $attr;
    // }    


}
