<?php

namespace App\Console\Commands;

use App\Role;
use App\User;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data= [];

        $firstname      = $this->ask('Please enter your name');
        $lastname       = $this->ask('Please enter your last name');
        $email          = $this->ask('Please enter your email');
        $password       = $this->secret('Please enter your password');


        $data['first_name']     = $firstname;
        $data['last_name']      = $lastname;
        $data['username']       = $email;
        $data['email']          = $email;

        $data['password']       = \Hash::make($password);
        $data['api_token']      = \Str::random(60);

        $user = User::create($data);

        $user->roles()->attach(Role::find(1));

        $this->line('User was succesfully created. You may log in now.');

    }
}
