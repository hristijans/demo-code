<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['product_id', 'user_id', 'rating', 'comment'];

    protected $table = 'feedback';

    protected $appends = ['is_mine'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(FeedbackComment::class);
    }

    public function getIsMineAttribute()
    {
        return (int)\Auth::user()->id == $this->user_id ? 1 : 0;
    }
}
