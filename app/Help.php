<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    use Translatable;
    
    public $translatedAttributes = ['title', 'content'];
    public $translationModel = HelpTranslation::class;
}
