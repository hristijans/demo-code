<?php

namespace App\Observers;

use App\Product;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $product->lat = $product->user->address->first()->lat;
        $product->lng = $product->user->address->first()->lng;

        $product->save();
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        $dispatcher = Product::getEventDispatcher();
        Product::unsetEventDispatcher();

        $product->lat = $product->user->address->first()->lat;
        $product->lng = $product->user->address->first()->lng;

        $product->save();

        Product::setEventDispatcher($dispatcher);
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
