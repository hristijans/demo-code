<?php

namespace App\Observers;

use App\Product;
use App\Feedback;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FeedbackObserver
{
    /**
     * Handle the feedback "created" event.
     *
     * @param  \App\Feedback  $feedback
     * @return void
     */
    public function created(Feedback $feedback)
    {  
        $product = Product::find($feedback->product_id);
        $product->avg_rating = $this->calculateAvgRating($feedback);
        $product->feedbacks_count = DB::table('feedback')->where('product_id', $feedback->product_id)->count();
        $product->save();
    }

    /**
     * Handle the feedback "updated" event.
     *
     * @param  \App\Feedback  $feedback
     * @return void
     */
    public function updated(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the feedback "deleted" event.
     *
     * @param  \App\Feedback  $feedback
     * @return void
     */
    public function deleted(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the feedback "restored" event.
     *
     * @param  \App\Feedback  $feedback
     * @return void
     */
    public function restored(Feedback $feedback)
    {
        //
    }

    /**
     * Handle the feedback "force deleted" event.
     *
     * @param  \App\Feedback  $feedback
     * @return void
     */
    public function forceDeleted(Feedback $feedback)
    {
        //
    }

    private function calculateAvgRating($feedback) 
    {
        $count = DB::table('feedback')->where('product_id', $feedback->product_id)->count();
        $sum = DB::table('feedback')->where('product_id', $feedback->product_id)->sum('rating');

        return ceil($sum/$count);
    }
}
