<?php 

namespace App\Repositories\User\Eloquent;

use App\Repositories\User\Contracts\UserInterface;

use App\User;
use App\Role;
use DB;
use Hash;


class UserRepository implements UserInterface {

    public function current()
    {
        return \Auth::user();
    }


    public function create( $data )
    {
        $user = User::create($data);

        $user->roles()->attach(Role::find(2));

        return $user;
    }

    public function update($id, $data)
    {
    
        $user = User::where('id', $id)->update($data);
        
        return User::find($id);
     
    }

    public function avatar($id, $avatar)
    {
        $prefix = 'avatar_';

        /**
         * @TODO This should be moved in separate class
         */
        list($type, $avatar) = explode(';', $avatar);
        list(,$extension) = explode('/',$type);
        list(,$avatar)      = explode(',', $avatar);
        $fileName = md5(microtime()).'.'.$extension;
        $avatar = base64_decode($avatar);
        $tmp_image = tempnam(sys_get_temp_dir(), 'avatar_') . $fileName; 
        
        file_put_contents($tmp_image, $avatar);

        
        \Storage::disk('public')->put($fileName, file_get_contents($tmp_image));                  

        $avatar_path = "/images/" . $fileName;

        $user = User::where('id', $id)->update(['avatar' => $avatar_path]);

        return  $avatar_path;
    }

    public function updatePhone($id, $phone)
    {
        $user = User::where(['id' => $id])
        ->update(['phone' => $phone]);

        return $phone;
    }


    public function mapAddress($user, $address) 
    {
        DB::table('user_addresses')->insert([
            'user_id' => $user->id,
            'address_id' => $address->id
        ]);
    }

    public function updatePassword($data)
    {
        $current_password = $data['old_password'];

        $user = \Auth::user();

        if (Hash::check($current_password, $user->password)) {
            $user->password = Hash::make($data['new_password']);
            $user->save();
        
            return ['changed' => true];
        } 
     
        return ['changed' => false, 'message' => 'Incorrect old password'];
    }
    

    public function updatePosition($user_id, $show_location)
    {
        $user = User::where(['id' => $user_id])
            ->update(['show_location' => $show_location]);

        return $user;
    }

    public function details($id)
    {
        return User::find($id);
    }

    public function getAllUsers()
    {
        return User::select(["id","first_name", "last_name", "email", "avatar"])->orderBy("id", "desc")->get();
    }

    public function getAdmin()
    {
        return  User::with(['roles' => function($q){
            $q->where('name', 'admin');
        }])->get()->first();
    }
}