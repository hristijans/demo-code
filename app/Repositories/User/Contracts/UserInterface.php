<?php 

namespace App\Repositories\User\Contracts;


interface UserInterface {

    public function current();

    public function create($data);

    public function update($id, $data);

    public function avatar($id, $avatar);

    public function updatePhone($id, $phone);

    public function mapAddress($user, $address);

    public function updatePassword($data);

    public function details($id);

}