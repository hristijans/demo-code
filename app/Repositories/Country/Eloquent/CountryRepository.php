<?php 

namespace App\Repositories\Country\Eloquent;
use App\Repositories\Country\Contracts\CountryInterface;

use App\Country;

class CountryRepository implements CountryInterface {

    public function get($params) 
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : '10';

        $orderBy = isset($params['order_by']) ? $params['order_by'] : 'id';
        $sort    = isset($params['sort']) ? $params['sort'] : 'desc';  

        $query = Country::query();

        if (isset($params['name'])) {
            $query->where('name','like', "%".$params['name']."%");
        }

        $query->orderBy($orderBy, $sort);

        return $query->get();
    }

}