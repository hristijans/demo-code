<?php 

namespace App\Repositories\Country\Contracts;

interface CountryInterface {

    public function get($params);
}