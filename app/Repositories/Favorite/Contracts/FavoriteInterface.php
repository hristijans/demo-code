<?php 
namespace App\Repositories\Favorite\Contracts;

interface FavoriteInterface {

    public function get($params);

    public function create($product_id, $user_id);

    public function delete($product_id, $user_id);

    public function exists($product_id, $user_id);

    public function deleteByProductId($product_id);
}