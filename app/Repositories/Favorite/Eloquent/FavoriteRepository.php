<?php 

namespace App\Repositories\Favorite\Eloquent;


use App\Favorite;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Favorite\Contracts\FavoriteInterface;


class FavoriteRepository implements FavoriteInterface {

    public function get($params) 
    {   
        $perPage = isset($params['per_page']) ? $params['per_page'] : '100';

        $orderBy = isset($params['order_by']) ? $params['order_by'] : 'id';
        $sort    = isset($params['sort']) ? $params['sort'] : 'desc';   

        $query = Favorite::query();

          
        $query->where('user_id', '=', Auth::user()->id);
       
        $query->orderBy($orderBy, $sort);      

        return $query->paginate($perPage);
      
    }

    public function create($user_id, $product_id)
    {
        return Favorite::create([
            'user_id'       => $user_id,
            'product_id'    => $product_id
        ]);
    }

    public function delete($user_id, $product_id)
    {
        $favorite = Favorite::where([
            'user_id' => $user_id,
            'product_id' => $product_id
        ])->get()->first();

        if ($favorite)
            $favorite->delete();

        return 1;
    }

    public function exists($user_id, $product_id)
    {
        return Favorite::where([
            'user_id' => $user_id,
            'product_id' => $product_id
        ])->get()->count();
    }

    public function deleteByProductId($product_id)
    {
        return Favorite::where('product_id', $product_id)->delete();
    }

}