<?php 
namespace App\Repositories\Charge\Eloquent;

use App\Charge;
use App\Repositories\Charge\Contracts\ChargeInterface;

class ChargeRepository implements ChargeInterface
{
    public function create($data)
    {
        return Charge::create($data);
    }
}