<?php 
namespace App\Repositories\Charge\Contracts;

interface ChargeInterface
{
    public function create($data);
}