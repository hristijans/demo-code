<?php 
namespace App\Repositories\CreditCard\Eloquent;

use App\CreditCard;
use App\Repositories\CreditCard\Contracts\CreditCardInterface;

class CreditCardRepository implements CreditCardInterface
{

    public function search($params)
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : '10';

        $orderBy = isset($params['order_by']) ? $params['order_by'] : 'id';
        $sort    = isset($params['sort']) ? $params['sort'] : 'desc';        

        $query = CreditCard::query();

        $query->where('user_id', '=', \Auth::user()->id);        

        $query->orderBy($orderBy, $sort);      
  
        return $query->paginate($perPage);
    }

    public function create($data)
    {
        return CreditCard::create($data);
    }

    public function delete($id)
    {
        return CreditCard::where(['id' => $id])->delete();
    }
}