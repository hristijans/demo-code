<?php 
namespace App\Repositories\CreditCard\Contracts;

interface CreditCardInterface {

    public function search($params);

    public function create($data);

    public function delete($id);

}