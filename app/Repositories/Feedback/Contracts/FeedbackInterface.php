<?php

namespace App\Repositories\Feedback\Contracts;

interface FeedbackInterface {

    public function find($id);

    public function store($data);

    public function search($params);
}
