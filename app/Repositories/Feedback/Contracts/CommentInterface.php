<?php

namespace App\Repositories\Feedback\Contracts;

interface CommentInterface {
  
    public function create($data);

    public function search($params);
}
