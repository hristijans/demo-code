<?php

namespace App\Repositories\Feedback\Eloquent;

use App\Feedback;
use App\FeedbackComment;
use App\Repositories\Feedback\Contracts\CommentInterface;
use App\Repositories\Feedback\Contracts\FeedbackInterface;

class CommentRepository implements CommentInterface {

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return FeedbackComment::create($data);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function search($params)
    {
        $query = FeedbackComment::query();

        $query->whereFeedbackId($params['feedback_id']);   

        return $query->paginate();
    }


}
