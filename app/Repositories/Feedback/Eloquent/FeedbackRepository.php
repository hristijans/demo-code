<?php

namespace App\Repositories\Feedback\Eloquent;

use App\Repositories\Feedback\Contracts\FeedbackInterface;
use App\Feedback;
class FeedbackRepository implements FeedbackInterface {
    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Feedback::find($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return Feedback::create($data);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function search($params)
    {
        $query = Feedback::query();

        $query->select("feedback.*");

        $perPage = isset($params['per_page']) ? $params['per_page'] : '10';

        $orderBy = isset($params['order_by']) ? $params['order_by'] : 'id';
        $sort    = isset($params['sort']) ? $params['sort'] : 'desc';

        if (isset($params['user_id']) && isset($params['type']) && $params['type'] == 'tenant') {
            $query->whereUserId($params['user_id']);
        }

        if (isset($params['user_id']) && isset($params['type']) && $params['type'] == 'owner') {
         
            $query->join('products as p', function($join) use ($params) {
                $join->on('p.id', '=', 'feedback.product_id');
                $join->where('p.user_id', '=', $params['user_id']);
            });
        }

        if (isset($params['product_id'])) {
            $query->whereProductId($params['product_id']);
        }

        $query->orderBy($orderBy, $sort);

        return $query->paginate();
    }


}
