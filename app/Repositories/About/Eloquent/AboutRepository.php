<?php

namespace App\Repositories\About\Eloquent;
use App\Repositories\About\Contracts\AboutInterface;

use App\About;

class AboutRepository implements AboutInterface {

    /**
     * @return mixed
     */
    public function get()
    {
        return About::latest()->first();
    }

}
