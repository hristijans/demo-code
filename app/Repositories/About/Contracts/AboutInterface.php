<?php

namespace App\Repositories\About\Contracts;

interface AboutInterface {

    /**
     * @return mixed
     */
    public function get();
}
