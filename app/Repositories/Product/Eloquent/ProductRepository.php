<?php
namespace App\Repositories\Product\Eloquent;

use App\Product;
use App\ProductImage;
use Illuminate\Routing\Pipeline;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Product\Contracts\ProductInterface;
use File;

class ProductRepository implements ProductInterface
{

    public function find($id)
    {
        return Product::find($id);
    }

    public function get($params)
    {
        return $this->search($params);
    }

    public function create($data)
    {
        $data['user_id'] = Auth::user()->id;
        return Product::create($data);
    }

    public function update($id, $data)
    {
        $product = Product::find($id);
        $product->fill($data);
        $product->save();

        return $product;
    }

    public function delete($product_id)
    {
        $product = Product::findOrFail($product_id);

        $product->delete();

        return 1;
    }

    public function search($params)
    {
        DB::enableQueryLog();

        $perPage = isset($params['per_page']) ? $params['per_page'] : '10';

        $orderBy = isset($params['order_by']) ? $params['order_by'] : 'id';
        $sort    = isset($params['sort']) ? $params['sort'] : 'desc';

        if ($orderBy == 'price') {
            $orderBy = 'price_per_day';
        }

        if ($orderBy == 'recent') {
            $orderBy = 'id';
        }

        if ($orderBy == 'rating') {
            $orderBy = 'avg_rating';
        }


        $query = Product::query();

        // do not get products that are in current renting


        if (isset($params['distance'])) {
            $user_address = Auth::user()->address->first();
            $query->distance($user_address->lat, $user_address->lng, $params['distance']);
        }

        if (isset($params['date_from'])) {
            $query->whereDate('available_from', '>=', $params['date_from']);
        }

        if (isset($params['date_to'])) {
            $query->whereDate('available_to', '<=', $params['date_to']);
        }

        if (isset($params['category_id'])) {
            $query->where('category_id', '=', $params['category_id']);
        }

        if (isset($params['id'])) {
            $query->where('id', '=', $params['id']);
        }

        if (isset($params['current'])) {
            $query->where('user_id', '=', Auth::user()->id);
            //$query->withRented();
        } else {
           // $query->where('is_rented', '!=', '1');
        }

        if (isset($params['not_mine'])) {
            $query->where('user_id', '!=', Auth::user()->id);
        }

        if (isset($params['user_id'])) {
            $query->where('user_id', '!=', $params['user_id']);
        }



        if (isset($params['status'])) {
            $query->where('status', '=', $params['status']);
        }


        $query->orderBy($orderBy, $sort);

        return $query->paginate($perPage);


    }

    public function addImages($product_id, $data)
    {

        if (!isset($data['images'])) return false;

        if (is_null($data['images'])) return false;

        //$this->removeImages($product_id);

        foreach($data['images'] as $image)
        {
            $this->addImage($product_id, $image);

        }

    }


    public function addImage($product_id, $image)
    {
        $prefix = 'product_image_';


        list($type, $image) = explode(';', $image);
        list(,$extension)   = explode('/',$type);
        list(,$image)       = explode(',', $image);
        $fileName = md5(microtime()).'.'.$extension;
        $image = base64_decode($image);
        $tmp_image = tempnam(sys_get_temp_dir(), 'product_image_') . $fileName;


        file_put_contents($tmp_image, $image);

        //throw (new GeneralException(__('general.resource_exists')));
        $size = File::size($tmp_image)/1000; // in kb
        if ($size > 800) {
            unlink($tmp_image);
            throw (new GeneralException(__('general.upload_error_big_image')));
        }


        \Storage::disk('public')->put($fileName, file_get_contents($tmp_image));

        $image_path = "/images/" . $fileName;


        $image = ProductImage::create([
            'product_id'    => $product_id,
            'image'         => $image_path
        ]);

        return $image;
    }


    public function removeImage($product_id, $image_id)
    {
        return ProductImage::where([
            'id'            => $image_id,
            'product_id'    => $product_id
        ])->delete();
    }

    public function getProductsByUserId($user_id)
    {
        return Product::orderBy('id', 'desc')
            ->where('user_id', $user_id)
            ->get()
            ->load('category', 'user')
            ->take(5);
    }
}
