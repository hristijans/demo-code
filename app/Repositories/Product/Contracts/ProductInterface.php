<?php 

namespace App\Repositories\Product\Contracts;

interface ProductInterface {

    public function find($id);

    public function get($params);

    public function search($params);

    public function create($data);

    public function update($id, $data);

    public function delete($product_id);
}