<?php 

namespace App\Repositories\Help\Contracts;

interface HelpInterface {

    public function get();

    public function search($term);
}