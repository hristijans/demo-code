<?php 

namespace App\Repositories\Help\Eloquent;
use App\Repositories\Help\Contracts\HelpInterface;

use App\Help;

class HelpRepository implements HelpInterface {

    public function get() 
    {   
        return Help::orderBy('display_order', 'asc')->get();
    }

    public function search($term)
    {
        return Help::orderBy('display_order', 'asc')
            ->whereTranslationLike('title', '%'.$term.'%')
            ->orWhereTranslationLike('content', '%'.$term.'%')
            ->get();
    }

}