<?php 
namespace App\Repositories\Chat\Room\Eloquent;

use App\Message;
use App\Repositories\Chat\Room\Contracts\MessageInterface;

class MessageRepository implements MessageInterface
{
    public function create($data)
    {
        return Message::create($data);
    }
}