<?php 
namespace App\Repositories\Chat\Room\Eloquent;

use App\Room;
use App\Repositories\Chat\Room\Contracts\RoomInterface;


class RoomRepository implements RoomInterface
{
    public function create($data)
    {
        return Room::create($data);
    }

    public function getUserRooms($user_id)
    {
        $room = Room::query();

        $room->select("rooms.*");

        $user_id = \Auth::user()->id;

        $room->join('room_users as ru', function($join) use ($user_id) {
            $join->on('ru.room_id', '=', 'rooms.id');
            $join->where('ru.user_id', $user_id);
        });
        
        $rooms = $room->get();

        return $rooms;
    }

    public function update($id, $data)
    {
        $room = Room::find($id);
        $room->fill($data);
        $room->save();

        return $room;
    }

    public function search($data)
    {

    }

    public function getByIdentifier($idf)
    {
        return Room::where('identifier', $idf)->get();
    }

    public function delete($id)
    {

    }
}