<?php 
namespace App\Repositories\Chat\Room\Contracts;

interface RoomInterface 
{
    public function create($data);

    public function update($id, $data);

    public function search($params);

    public function delete($id);

    public function getByIdentifier($idf);

    public function getUserRooms($id);
}