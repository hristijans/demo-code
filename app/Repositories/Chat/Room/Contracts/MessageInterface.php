<?php 
namespace App\Repositories\Chat\Room\Contracts;

interface MessageInterface
{
    public function create($data);
}