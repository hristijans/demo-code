<?php

namespace App\Repositories\Address\Eloquent;
use App\Repositories\Address\Contracts\AddressInterface;


use App\Address;

class AddressRepository implements AddressInterface {

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Address::find($id);
    }

    public function create( $data)
    {
        return Address::create($data);
    }

    public function update($id, $data)
    {
        $address = Address::find($id);

        $address->fill($data);

        $address->save();

        return $address;
    }

}
