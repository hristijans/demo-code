<?php 

namespace App\Repositories\Address\Contracts;

interface AddressInterface {

    public function find($id);

    public function update($id, $data);

    public function create( $data );
}