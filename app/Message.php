<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['user_id', 'room_id', 'message'];
    protected $appends = ['is_mine'];


    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function getIsMineAttribute()
    {
        return (int)\Auth::user()->id == $this->user_id ? 1 : 0;
    }
}
