<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppFeedbackComment extends Model
{

    public function feedback()
    {
        return $this->belongsTo(AppFeedback::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
