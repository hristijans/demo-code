<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\CountriesResource;
use App\Http\Controllers\Api\ApiController;

use App\Services\CountriesService;

class CountriesController extends ApiController
{
    protected $countriesService;

    public function __construct(CountriesService $countriesService) 
    {
        $this->countriesService = $countriesService;
    }

/**
     * @api {get} /countries?name=Germany&order_by=name&sort=desc List
     * @apiDescription List all available countries
     * @apiName ListCountries
     * @apiGroup Countries
     * @apiVersion 1.0.0
     *
     * 
     *
  

     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *{
     *    "data": [
     *       {
     *           "id": 1,
     *           "name": "Belgium",
     *           "code": "BE"
     *       },
     *       {
     *           "id": 2,
     *           "name": "France",
     *           "code": "FR"
     *       },
     *       {
     *           "id": 3,
     *           "name": "Germany",
     *           "code": "DE"
     *       }
     *   ]
     *}
     *
     * @apiError UnprocessableEntity Validation errors.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *       {
     *           
     *       }
     */
    public function index()
    {
        try {
            $countries = $this->countriesService->get(request()->all());

            return $this->respond( CountriesResource::collection($countries));
        } catch (\Throwable $th) {
            return $th;
        }

    }
}
