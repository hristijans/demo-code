<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RegisterRequest;
use Illuminate\Http\JsonResponse;

use App\Http\Resources\UserResource;

use App\Services\UsersService;
use App\Services\AddressesService;


use DB;

class RegisterController extends Controller
{
    protected $usersService;
    protected $addressService;

    public function __construct(UsersService $usersService, AddressesService $addressService)
    {
        $this->usersService = $usersService;
        $this->addressService = $addressService;
    }

    /**
     * @api {post} /register Register
     * @apiDescription Register new user
     * @apiName RegisterUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} first_name First name.
     * @apiParam {String} Last_name Last name.
     * @apiParam {String} email Email.
     * @apiParam {String} username Username.
     * @apiParam {String} password Password.
     * @apiParam {String} password_confirm Password confirmation.
     * @apiParam {Number} country_id Country ID.
     * @apiParam {String} address_1 Address.
     * @apiParam {Number} zip Postal code.
     * @apiParam {String} city City.
     * @apiParam {Number} city City.
     * @apiParam {Number} city City.
     *
     *
     * @apiSuccess {Number} id User ID.
     * @apiSuccess {String} first_name  First name.
     * @apiSuccess {String} last_name  Last name.
     * @apiSuccess {String} email  Email.
     * @apiSuccess {String} username  Username.
     * @apiSuccess {String} token  Authentication token.
     * @apiSuccess {Object} roles  User roles information.
     * @apiSuccess {Number} roles.id  Role ID.
     * @apiSuccess {String} roles.name  Role name.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "data": {
     *               "id": 12,
     *               "first_name": "John",
     *               "last_name": "Snow",
     *               "email": "johnsnow@winterfell.com",
     *               "username": "john.snow",
     *               "token": "OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg",
     *               "roles": [
     *                   {
     *                       "id": 2,
     *                       "name": "user"
     *                   }
     *               ]
     *           }
     *       }
     *
     * @apiError UnprocessableEntity Validation errors.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *       {
     *           "errors": {
     *               "first_name": [
     *                   "The first name field is required."
     *               ],
     *               "last_name": [
     *                   "The last name field is required."
     *               ],
     *               "email": [
     *                   "The email field is required."
     *               ],
     *               "username": [
     *                   "The username field is required."
     *               ],
     *               "password": [
     *                   "The password field is required."
     *               ],
     *               "password_confirm": [
     *                   "The password confirm field is required."
     *               ],
     *               "country_id": [
     *                   "The country id field is required."
     *               ],
     *               "address_1": [
     *                   "The address 1 field is required."
     *               ],
     *               "zip": [
     *                   "The zip field is required."
     *               ],
     *               "city": [
     *                   "The city field is required."
     *               ]
     *           }
     *       }
     */
    public function store(RegisterRequest $request)
    {
        try {

           DB::beginTransaction();

           $data = $request->all();
           $data['password']    = \Hash::make($data['password']);
           $data['api_token']   = \Str::random(60);
           $data['show_location'] = 1;

           $user = $this->usersService->create($data);

           $data['type'] = 'regular';
           $address = $this->addressService->create($data);
           $this->usersService->mapAddress($user, $address);

            $data['type'] = 'meeting';
            $address = $this->addressService->create($data);
            $this->usersService->mapAddress($user, $address);

           DB::commit();

           return response()->json(['data' => new UserResource($user)], JsonResponse::HTTP_CREATED);

        } catch (\Throwable $th) {
            DB::rollBack();
            return $th;
        }
    }
}
