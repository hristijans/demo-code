<?php

namespace App\Http\Controllers\Api\Chat;

use App\Room;
use App\Message;
use Illuminate\Http\Request;
use App\Services\UsersService;
use Illuminate\Support\Facades\DB;
use App\Services\Chat\RoomsService;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoomResource;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\CreateRoomRequest;
use App\Http\Requests\Api\CreateAdminRoomRequest;

class RoomsController extends ApiController
{
    protected $participants = [];
    protected $roomsService;
    protected $usersService;

    public function __construct(RoomsService $roomsService, UsersService $usersService)
    {
        $this->roomsService = $roomsService;
        $this->usersService = $usersService;
    }

    /**
     * @api {get} /chat/rooms All rooms
     * @apiDescription Show all rooms for the user
     * @apiName ShowAllRoom
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccess {Number} id room ID
     * @apiSuccess {String} last_user Last user who send message
     * @apiSuccess {String} last_message  Lsat message in the room.
     * @apiSuccess {Array}  messages List of messages for this room.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *  "data": [
     *       {
     *           "id": 1,
     *           "last_user": "John Quigley",
     *           "last_message": "Hello",
     *           "messages": [
     *           {
     *               "id": 1,
     *               "user_id": 11,
     *               "room_id": 1,
     *               "message": "Hello",
     *               "created_at": "2019-11-22 17:32:28",
     *               "updated_at": "2019-11-22 17:32:28"
     *           }
     *           ],
     *       },
     *       {
     *           "id": 2,
     *           "last_user": "John Quigley",
     *           "last_message": "Hello",
     *           "messages": [
     *           {
     *               "id": 2,
     *               "user_id": 11,
     *               "room_id": 2,
     *               "message": "Hello",
     *               "created_at": "2019-11-22 17:35:59",
     *               "updated_at": "2019-11-22 17:35:59"
     *           }
     *           ],
     *       }
     *   ],
     *}
     */
    public function index(Request $request)
    {
        try {

            $rooms = $this->roomsService->userRooms(\Auth::user()->id);

            return  RoomResource::collection($rooms);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * @api {get} /chat/rooms/{id} Show Room
     * @apiDescription Show room by id
     * @apiName ShowRoom
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccess {Number} id room ID
     * @apiSuccess {String} last_user Last user who send message
     * @apiSuccess {String} last_message  Lsat message in the room.
     * @apiSuccess {Array}  messages List of messages for this room.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *   "data": {
     *       "id": 2,
     *       "last_user": "John Quigley",
     *       "last_message": "Hello",
     *       "messages": [
     *           {
     *               "id": 2,
     *               "user_id": 11,
     *               "room_id": 2,
     *               "message": "Hello",
     *               "created_at": "2019-11-22 17:35:59",
     *               "updated_at": "2019-11-22 17:35:59"
     *           }
     *       ],
     *   }
     *}
     */
    public function show(Request $request)
    {
        try {
            $room = Room::find($request->id);

            return new RoomResource($room);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }



    /**
     * @api {post} /chat/rooms Create Room
     * @apiDescription Create chat room
     * @apiName CreateChatRoom
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *

     * @apiParam {Integer} user_id User to chat with.
     * @apiParam {String} message Message

     *
     * @apiSuccess {Number} id Category ID.
     * @apiSuccess {String} name  Category name.
     * @apiSuccess {String} description  Category description.
     * @apiSuccess {String} icon  Icon.
     * @apiSuccess {Number} status Active status.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *           {
     *               "id": 1,
     *               "identifier": "1a46910f3d6e04d7cd45c7caff38c63b",
     *               "user_id": 11,
     *               "message_id": 1,
     *               "title": "Rentout chat room",
     *               "slug": "4_11",
     *               "created_by": 11,
     *               "created_at": "2019-11-22 17:32:28",
     *               "updated_at": "2019-11-22 17:32:28"
     *           }
     *       ],
     *       "message": "Resource was succesfuly created"
     *   }

     * @apiError UnprocessableEntity Validation errors.
     *

     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *       "errors": {
     *           "user_id": [
     *               "The name field is required."
     *           ]
     *       }
     *   }
     */

    public function store(CreateRoomRequest $request)
    {
        try {

            $room = $this->roomsService->create($request->all());

            return $this->respond($room, self::RESOURCE_CREATED);

        } catch (\Throwable $th) {
            throw $th;
        }
    }



    /**
     * @api {post} /chat/rooms/bulk Create bulk rooms
     * @apiDescription Create multiple cchat rooms in one request
     * @apiName CreateBulkChatRoom
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *

     * @apiParam {Array} users Array with user ids to start chat with.
     * @apiParam {String} message Message

     *
     * @apiSuccess {Number} id Category ID.
     * @apiSuccess {String} name  Category name.
     * @apiSuccess {String} description  Category description.
     * @apiSuccess {String} icon  Icon.
     * @apiSuccess {Number} status Active status.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *           {
     *              "status":"succes"
     *           }
     *       ],
     *
     *   }
     */
    public function bulk(Request $request)
    {
        try {

            $users = $request->users;

            foreach ($users as $key => $user) {
                $room = $this->roomsService->create(['user_id' => $user, 'message' => $request->message]);
            }

            return $this->respond(["status" => 'success']);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * @api {delete} /chat/rooms/:id Delete Room
     * @apiDescription Delete chat room
     * @apiName DeleteChatRoom
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [],
     *       "message": "Resource was succesfuly deleted"
     *   }

     *
     */
    public function destroy(Request $request)
    {
        try {

            $room = Room::find($request->id);
            $messages = Message::where('room_id', $request->id)->delete();
            \DB::table('room_users')->where('room_id', $request->id)->delete();

            $room->delete();

            return $this->respond([], self::RESOURCE_DELETED);
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    /**
     * @api {post} /chat/rooms/admin Create Admin Room
     * @apiDescription Create chat with the admin
     * @apiName CreateAdminChatRoom
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *

     * @apiParam {String} message Message

     *
     * @apiSuccess {Number} id Category ID.
     * @apiSuccess {String} name  Category name.
     * @apiSuccess {String} description  Category description.
     * @apiSuccess {String} icon  Icon.
     * @apiSuccess {Number} status Active status.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *           {
     *               "id": 1,
     *               "identifier": "1a46910f3d6e04d7cd45c7caff38c63b",
     *               "user_id": 11,
     *               "message_id": 1,
     *               "title": "Rentout chat room",
     *               "slug": "4_11",
     *               "created_by": 11,
     *               "created_at": "2019-11-22 17:32:28",
     *               "updated_at": "2019-11-22 17:32:28"
     *           }
     *       ],
     *       "message": "Resource was succesfuly created"
     *   }

     * @apiError UnprocessableEntity Validation errors.
     *

     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *       "errors": {
     *           "message": [
     *               "The name field is required."
     *           ]
     *       }
     *   }
     */
    public function admin(CreateAdminRoomRequest $request)
    {
        try {

            $data = [];
            $data['message'] = $request->message;
            $data['user_id'] = $this->usersService->admin()->id;           

            $room = $this->roomsService->create($data);

            return $this->respond($room, self::RESOURCE_CREATED);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
