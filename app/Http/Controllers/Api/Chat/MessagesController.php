<?php

namespace App\Http\Controllers\Api\Chat;

use App\Room;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\CreateMessageRequest;

class MessagesController extends ApiController
{
        /**
     * @api {post} /chat/messages Send Message
     * @apiDescription Send message in room
     * @apiName SendMessage
     * @apiGroup Chat
     * @apiVersion 1.0.0
     *
  
     * @apiParam {Integer} room_id  Room where message will belong.
     * @apiParam {String} message  Message.


     *
     * @apiSuccess {Number} id Message ID
     * @apiSuccess {String} message The message content
     * @apiSuccess {Object} room Room where message belongs.
     * @apiSuccess {Object} user User who sent the message

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "room_id": "2",
     *           "message": "Lorem Ipsum",
     *           "user_id": 11,
     *           "updated_at": "2019-11-22 17:45:56",
     *           "created_at": "2019-11-22 17:45:56",
     *           "id": 6,
     *           "room": {
     *               "id": 2,
     *               "identifier": "613d3d5df83eba8845a8211a48fdd3dd",
     *               "user_id": 11,
     *               "message_id": 6,
     *               "title": "Rentout chat room",
     *               "slug": "8_11",
     *               "created_by": 11,
     *               "created_at": "2019-11-22 17:35:59",
     *               "updated_at": "2019-11-22 17:45:56"
     *           },
     *           "user": {
     *               "id": 11,
     *               "first_name": "John",
     *               "last_name": "Quigley",
     *               "email": "john@seller.com",
     *               "description": null,
     *               "username": "john.seller",
     *               "avatar": "",
     *               "email_verified_at": null,
     *               "api_token": "hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp",
     *               "created_at": "2019-11-17 15:02:18",
     *               "updated_at": "2019-11-17 15:02:18",
     *               "stripe_id": "cus_GCDiLOVgByfFQK",
     *               "card_brand": null,
     *               "card_last_four": null,
     *               "trial_ends_at": null,
     *               "gender": "",
     *               "date_of_birth": "",
     *               "phone": "",
     *               "show_location": 1
     *           }
     *       }
     *   }   

     * @apiError UnprocessableEntity Validation errors.
     *

     * 
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *     "errors": {
     *           "room_id": [
     *               "The room id field is required."
     *           ],
     *           "message": [
     *               "The message field is required."
     *           ],
     *       }
     *   }
     */
    
    public function store(CreateMessageRequest $request)
    {
        try {
            $data = $request->all();
            $data['user_id'] = \Auth::user()->id;

            $message = Message::create($data);

            $room = Room::find($request->room_id);
            $room->message_id = $message->id;
            $room->user_id = \Auth::user()->id;

            $room->save();

            return $this->respond($message->load('room', 'user'));
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
