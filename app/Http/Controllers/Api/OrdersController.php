<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\OrdersService;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\CreateOrderRequest;
use App\Http\Requests\Api\PreviewOrderRequest;

class OrdersController extends ApiController
{

    protected $ordersService;

    public function __construct(OrdersService $ordersService)
    {
        $this->ordersService = $ordersService;
    }

    /**
     * @api {get} /orders Show All
     * @apiDescription Show all order for current user
     * @apiName Show All Orders
     * @apiGroup Orders
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} id Order ID.
     * @apiSuccess {Number} amount Order amount in cents.
     * @apiSuccess {Number} deposit Order deposit in cents.
     * @apiSuccess {String} status Order status.
     * @apiSuccess {String} from_date Order Date from.
     * @apiSuccess {String} from_to Order Date to.
     * @apiSuccess {Object} user User object.
     * @apiSuccess {Object} product Product object.

     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *       {
     *           "id": 4,
     *           "amount": 1550000,
     *           "deposit": 9850,
     *           "status": "paid",
     *           "from_date": "2019-03-04",
     *           "to_date": "2019-05-05",
     *           "user": {
     *               "id": 6,
     *               "first_name": "MrsJaleel",
     *               "last_name": "Schuppe",
     *               "email": "Jaleel4@gmail.com"
     *           },
     *           "product": {
     *               "id": 2,
     *               "title": "Product 2",
     *               "description": "this is my sec product",
     *               "rating": null,
     *               "images": [
     *                   {
     *                   "id": 1,
     *                   "product_id": 2,
     *                   "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *                   },
     *                   {
     *                   "id": 2,
     *                   "product_id": 2,
     *                   "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *                   }
     *               ],
     *               "archived": null,
     *               "user": {
     *                   "id": 2,
     *                   "first_name": "Marija1",
     *                   "last_name": "Kovacevic",
     *                   "email": "user@example.com"
     *               }
     *           }
     *       }
     *       ],
     *           "links": {
     *               "first": "http://rentout.test/api/orders?page=1",
     *               "last": "http://rentout.test/api/orders?page=1",
     *               "prev": null,
     *               "next": null
     *           },
     *           "meta": {
     *               "current_page": 1,
     *               "from": 1,
     *               "last_page": 1,
     *              "path": "http://rentout.test/api/orders",
     *               "per_page": "10",
     *               "to": 1,
     *               "total": 1
     *          }
     *   }
     */
    public function index()
    {
        try {
            $params = request()->all() + ['current' => true];
            return OrderResource::collection($this->ordersService->search($params));

        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * @api {get} /orders/:id Show Order
     * @apiDescription Show order by id for current user
     * @apiName Show Order by ID
     * @apiGroup Orders
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} id Order ID.
     * @apiSuccess {Number} amount Order amount in cents.
     * @apiSuccess {Number} deposit Order deposit in cents.
     * @apiSuccess {String} status Order status.
     * @apiSuccess {String} from_date Order Date from.
     * @apiSuccess {String} from_to Order Date to.
     * @apiSuccess {Object} user User object.
     * @apiSuccess {Object} product Product object.

     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data":
     *           {
     *               "id": 4,
     *               "amount": 1550000,
     *               "deposit": 9850,
     *               "status": "paid",
     *               "from_date": "2019-03-04",
     *               "to_date": "2019-05-05",
     *               "user": {
     *                   "id": 6,
     *                   "first_name": "MrsJaleel",
     *                   "last_name": "Schuppe",
     *                   "email": "Jaleel4@gmail.com"
     *               },
     *               "product": {
     *                   "id": 2,
     *                   "title": "Product 2",
     *                   "description": "this is my sec product",
     *                   "rating": null,
     *                   "images": [
     *                       {
     *                       "id": 1,
     *                       "product_id": 2,
     *                       "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                       "created_at": "2019-11-08 20:21:01",
     *                       "updated_at": "2019-11-08 20:21:01"
     *                       },
     *                       {
     *                       "id": 2,
     *                       "product_id": 2,
     *                       "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                       "created_at": "2019-11-08 20:21:01",
     *                       "updated_at": "2019-11-08 20:21:01"
     *                       }
     *                   ],
     *                   "archived": null,
     *                   "user": {
     *                       "id": 2,
     *                       "first_name": "Marija1",
     *                       "last_name": "Kovacevic",
     *                       "email": "user@example.com"
     *                   }
     *               }
     *           }
     *
     *   }
     */
    public function show(Request $request)
    {
        try {
            return $this->respond(new OrderResource($this->ordersService->find($request->id)));

        } catch (\Throwable $th) {
            return $th;
        }
    }

    // search order
    public function search()
    {

    }

    /**
     * @api {post} /orders/preview Preview Order
     * @apiDescription Preview new Order
     * @apiName Preview Order
     * @apiGroup Orders
     * @apiVersion 1.0.0
     *

     * @apiParam {Number} product_id Product ID.
     * @apiParam {String} from_date Date from format YYYY-MM-DD
     * @apiParam {String} to_date Date to format YYYY-MM-DD
     *
     * @apiSuccess {Object} product Product object.
     * @apiSuccess {Number} product.id Product ID.
     * @apiSuccess {Number} product.user_id User ID.
     * @apiSuccess {String} product.title Product title.
     * @apiSuccess {String} product.description  Product description.
     * @apiSuccess {Number} product.category_id Category ID.
     * @apiSuccess {Number} product.is_rented Product is rented.
     * @apiSuccess {Number} product.avg_rating Product average rating.
     * @apiSuccess {Number} product.lat Product Latitude
     * @apiSuccess {Number} product.lng Product Lognitude
     * @apiSuccess {Number} product.price_per_day Product price per day in cents.
     * @apiSuccess {Number} product.deposit Product deposit in cents.
     * @apiSuccess {String} product.available_from Product Date available from.
     * @apiSuccess {String} product.available_to Product Date available to.
     * @apiSuccess {String} product.hour_from Product Hour from.
     * @apiSuccess {String} product.hour_to Product Hour to.
     * @apiSuccess {String} from_date Order Date from.
     * @apiSuccess {String} from_to Order Date to.
     * @apiSuccess {Number} number_of_days Order rent days.
     * @apiSuccess {Number} price_per_day Product price per day in cents.
     * @apiSuccess {Number} deposit Product deposit in cents.
     * @apiSuccess {Number} avg_rating Product average rating.
     * @apiSuccess {Number} fee Product fee in cents.
     * @apiSuccess {Number} total Order total price in cents.
     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *      "data": {
     *           "product": {
     *               "id": 2,
     *               "user_id": 2,
     *               "title": "Product 2",
     *               "description": "this is my sec product",
     *               "category_id": 1,
     *               "is_rented": 1,
     *               "avg_rating": null,
     *               "lat": -32.1476,
     *               "lng": 50.1178,
     *               "price_per_day": 25000,
     *               "deposit": 9850,
     *               "available_from": "2019-11-01",
     *               "available_to": "2019-11-07",
     *               "hour_from": "10:00",
     *               "hour_to": "16:00",
     *               "created_at": "2019-11-08 20:21:01",
     *               "updated_at": "2019-11-14 22:21:20",
     *               "deleted_at": null
     *          },
     *           "from_date": "2019-04-04",
     *           "to_date": "2019-04-11",
     *           "number_of_days": 7,
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "rating": null,
     *           "fee": 0,
     *           "total": 184850
     *       }
     *   }

     * @apiError UnprocessableEntity Validation errors.
     *

     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *          "errors": {
     *               "product_id": [
     *                   "The product id field is required."
     *               ],
     *               "from_date": [
     *                   "The from date field is required."
     *               ],
     *               "to_date": [
     *                   "The to date field is required."
     *               ],
     *           }
     *   }
     */
    public function preview(PreviewOrderRequest $request)
    {
        try {

            $order = $this->ordersService->preview($request->all());

            return $this->respond($order);

        } catch (\Throwable $th) {
            return $th;
        }
    }

        /**
     * @api {post} /orders Create
     * @apiDescription Create new Order
     * @apiName Create Order
     * @apiGroup Orders
     * @apiVersion 1.0.0
     *

     * @apiParam {Number} product_id Product ID.
     * @apiParam {String} from_date Date from format YYYY-MM-DD
     * @apiParam {String} to_date Date to format YYYY-MM-DD
     * @apiParam {String} payment_token Stripe Payment token.
     * @apiParam {String=creditcard, bancontact} payment_type Payment type.
     *
     * @apiSuccess {Object} product Product object.
     * @apiSuccess {Number} product.id Product ID.
     * @apiSuccess {Number} product.user_id User ID.
     * @apiSuccess {String} product.title Product title.
     * @apiSuccess {String} product.description  Product description.
     * @apiSuccess {Number} product.category_id Category ID.
     * @apiSuccess {Number} product.is_rented Product is rented.
     * @apiSuccess {Number} product.avg_rating Product average rating.
     * @apiSuccess {Number} product.lat Product Latitude
     * @apiSuccess {Number} product.lng Product Lognitude
     * @apiSuccess {Number} product.price_per_day Product price per day in cents.
     * @apiSuccess {Number} product.deposit Product deposit in cents.
     * @apiSuccess {String} product.available_from Product Date available from.
     * @apiSuccess {String} product.available_to Product Date available to.
     * @apiSuccess {String} product.hour_from Product Hour from.
     * @apiSuccess {String} product.hour_to Product Hour to.
     * @apiSuccess {String} from_date Order Date from.
     * @apiSuccess {String} from_to Order Date to.
     * @apiSuccess {Number} number_of_days Order rent days.
     * @apiSuccess {Number} price_per_day Product price per day in cents.
     * @apiSuccess {Number} deposit Product deposit in cents.
     * @apiSuccess {Number} avg_rating Product average rating.
     * @apiSuccess {Number} fee Product fee in cents.
     * @apiSuccess {Number} total Order total price in cents.
     * @apiSuccess {Object} user User who created the order.
     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "id": 4,
     *           "amount": 1550000,
     *           "deposit": 9850,
     *           "status": "paid",
     *           "from_date": "2019-03-04",
     *           "to_date": "2019-05-05",
     *           "user": {
     *               "id": 6,
     *               "first_name": "MrsJaleel",
     *               "last_name": "Schuppe",
     *               "email": "Jaleel4@gmail.com"
     *           },
     *           "product": {
     *               "id": 2,
     *               "title": "Product 2",
     *               "description": "this is my sec product",
     *               "rating": null,
     *               "images": [
     *                   {
     *                       "id": 1,
     *                       "product_id": 2,
     *                       "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                       "created_at": "2019-11-08 20:21:01",
     *                       "updated_at": "2019-11-08 20:21:01"
     *                   },
     *                   {
     *                       "id": 2,
     *                       "product_id": 2,
     *                       "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                       "created_at": "2019-11-08 20:21:01",
     *                       "updated_at": "2019-11-08 20:21:01"
     *                   }
     *               ],
     *               "archived": null,
     *               "user": {
     *                   "id": 2,
     *                   "first_name": "Hristijan",
     *                   "last_name": "Kovacevic",
     *                   "email": "user@example.com"
     *               }
     *           }
     *       },
     *       "message": "Resource was succesfuly created"
     *   }

     * @apiError UnprocessableEntity Validation errors.
     *

     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *
     *       "errors": {
     *           "product_id": [
     *               "The product id field is required."
     *           ],
     *           "from_date": [
     *               "The from date field is required."
     *           ],
     *           "to_date": [
     *               "The to date field is required."
     *           ],
     *           "payment_token": [
     *               "The payment token field is required."
     *           ],
     *           "payment_type": [
     *               "The payment type field is required."
     *           ],
     *       }
     *   }
     */
    public function store(CreateOrderRequest $request)
    {
        try {
            $order = $this->ordersService->create($request->all());

            return $this->respond(new OrderResource($order), self::RESOURCE_CREATED);

        } catch (\GeneralException $th) {
            return $th;
        }
    }

}
