<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public function __construct()
    {
        $locale = request()->header('Accept-Language');
        $allowed_locales = ['en', 'be', 'fr', 'de'];

        if (in_array($locale, $allowed_locales)) {
            App::setLocale($locale);
        } else {
            App::setLocale('en');
        }

        
    }

    const RESOURCE_CREATED = 'Resource was succesfuly created';
    const RESOURCE_UPDATED = 'Resource was succesfuly updated';
    const RESOURCE_DELETED = 'Resource was succesfuly deleted';
    const RESOURCE_NOT_EXIST = 'Resource does not exist';


    protected $status_code = 200;

    public function respond($data, $message = null)
    {   
        $response = ["data" => $data, 'message' => $message];
        if (is_null($message)) unset($response['message']);
        
        return response()
           ->json($response)
           ->setStatusCode($this->status_code);
    }

    public function setCode($status_code)
    {
       $this->status_code = $status_code;
       return $this;
    }

    public function respondWithError($errors) 
    {
        return response()
            ->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function notFound($message = null)
    {
        return response()
        ->json(['errors' => $message ?? self::RESOURCE_NOT_EXIST], JsonResponse::HTTP_NOT_FOUND);
    }
}
