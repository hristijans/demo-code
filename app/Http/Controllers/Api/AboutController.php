<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\AboutService;
use App\Http\Controllers\Controller;
use App\Http\Resources\AboutResource;
use App\Http\Controllers\Api\ApiController;



class AboutController extends ApiController
{
    protected $aboutService;

    public function __construct(AboutService $aboutService)
    {
        $this->aboutService = $aboutService;
    }

/**
     * @api {get} /about 
     * @apiDescription Show about texts
     * @apiName Show About
     * @apiGroup About
     * @apiVersion 1.0.0
     *
     *
     *
     * @apiSuccess {String} title Title.
     * @apiSuccess {String} content  Text.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "title": "About rentout",
     *           "content": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa, temporibus mollitia animi sit in voluptatum non dolore officia numquam quo pariatur reprehenderit fuga, repudiandae labore cum at ratione blanditiis.",
     *           "order": 1
     *       }
     * }
       
     */    
    public function index()
    {
        try {

            return $this->respond((new AboutResource($this->aboutService->get())));
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
