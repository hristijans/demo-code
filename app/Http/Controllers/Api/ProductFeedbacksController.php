<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\FeedbacksService;
use App\Http\Controllers\Controller;
use App\Http\Resources\FeedbacksResource;

class ProductFeedbacksController extends Controller
{
    protected $feedbacksService;

    public function __construct(FeedbacksService $feedbacksService)
    {
        $this->feedbacksService = $feedbacksService;
    }

         /**
     * @api {get} /products/:id/feedbacks Show All Feedbacks
     * @apiDescription Show all Product feedbacks
     * @apiName Show Feedbacks
     * @apiGroup Product
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} rating Rating for the product feedback.
     * @apiSuccess {String} comment Comment for the product.
     * @apiSuccess {Array} comments  List of comments for the feedback.
     * @apiSuccess {Object} user User Object.
     * @apiSuccess {Object} product Product Object.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": [
     *       {
     *       "rating": 5,
     *       "comment": "very good",
     *       "comments": [],
     *       "user": {
     *           "id": 6,
     *           "first_name": "MrsJaleel",
     *           "last_name": "Schuppe",
     *           "email": "Jaleel4@gmail.com",
     *           "description": null,
     *           "username": "Jaleel4@gmail.com",
     *           "date_of_birth": "",
     *           "phone": "",
     *           "gender": "",
     *           "avatar": "",
     *           "show_location": 1,
     *           "token": "S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr",
     *           "roles": [
     *               {
     *               "id": 2,
     *               "name": "user"
     *               }
     *           ],
     *       },
     *       "product": {
     *           "id": 2,
     *           "title": "Product 2",
     *           "description": "this is my sec product",
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "available_from": "2019-11-01",
     *           "available_to": "2019-11-07",
     *           "hour_from": "10:00",
     *           "hour_to": "16:00",
     *           "is_favorite": false,
     *           "distance": 5985.64,
     *           "rating": 5,
     *           "images": [
     *               {
     *                   "id": 1,
     *                   "product_id": 2,
     *                   "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               },
     *               {
     *                   "id": 2,
     *                   "product_id": 2,
     *                   "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               }
     *           ],
     *           "category": {
     *               "id": 1,
     *               "name": "Main Category"
     *           },
     *           "user": {
     *               "id": 2,
     *               "first_name": "Marija1",
     *               "last_name": "Kovacevic",
     *               "email": "user@example.com"
     *           }
     *           }
     *       }
     *       ],
     *       "links": {
     *           "first": "http://rentout.test/api/products/2/feedbacks?page=1",
     *           "last": "http://rentout.test/api/products/2/feedbacks?page=1",
     *           "prev": null,
     *           "next": null
     *       },
     *       "meta": {
     *           "current_page": 1,
     *           "from": 1,
     *           "last_page": 1,
     *           "path": "http://rentout.test/api/products/2/feedbacks",
     *           "per_page": 15,
     *           "to": 1,
     *           "total": 1
     *       }
     *   }
     *
     */
    public function index(Request $request)
    {
        try {
            $params['id'] =  $request->id;

            $feedbacks = $this->feedbacksService->search($params);

            return FeedbacksResource::collection($feedbacks);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
