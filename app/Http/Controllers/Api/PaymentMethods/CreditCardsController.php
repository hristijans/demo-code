<?php

namespace App\Http\Controllers\Api\PaymentMethods;

use Stripe\Stripe;
use App\CreditCard;
use App\PaymentMethod;
use Illuminate\Http\Request;
use App\Services\UsersService;
use App\Http\Controllers\Controller;
use App\Services\CreditCardsService;
use App\Http\Resources\CreditCardResource;
use App\Http\Requests\Api\CreateCreditCardRequest;
use App\Http\Controllers\Api\ApiController;


class CreditCardsController extends ApiController
{

    protected $userService;
    protected $creditCardsService;

    public function __construct(CreditCardsService $creditCardsService)
    {   
        $this->creditCardsService = $creditCardsService;
    }

    /**
     * @api {get} /payment-methods/cards Show
     * @apiDescription Show credit cards
     * @apiName Show CreditCards
     * @apiGroup CreditCards
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccess {Number} id Credit Card ID.
     * @apiSuccess {String} token Stripe Credit Card token.
     * @apiSuccess {String} type  Credit Card type.
     * @apiSuccess {String} brand  Credit Card brand.
     * @apiSuccess {Number} last4 Credit Card last 4 numbers.
     * @apiSuccess {String} exp_year  Credit Card expiration year.
     * @apiSuccess {String} exp_month  Credit Card expiration month.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *              {
     *               "id": 1,
     *                "token": "card_GChJ1mwFwZnN76",
     *               "type": "Visa",
     *               "brand": "Visa",
     *               "last4": "4242",
     *               "exp_year": "2020",
     *               "exp_month": "11"
     *              }
     *           ],
     *           "links": {
     *              "first": "http://rentout.test/api/payment-methods/cards?page=1",
     *              "last": "http://rentout.test/api/payment-methods/cards?page=1",
     *              "prev": null,
     *              "next": null
     *           },
     *           "meta": {
     *              "current_page": 1,
     *              "from": 1,
     *              "last_page": 1,
     *              "path": "http://rentout.test/api/payment-methods/cards",
     *              "per_page": "10",
     *              "to": 1,
     *              "total": 1
     *          }   
     *  }
   
     * @apiError Credit Cards was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *   {
     *       "errors": "Resource does not exist"
     *   }
     */
    public function index(Request $request)
    {
        try {            
           
            $cards = $this->creditCardsService->search($request->all());
        
            return CreditCardResource::collection($cards);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

     /**
     * @api {post} /payment-methods/cards Create
     * @apiDescription Create new credit card
     * @apiName CreateCreditCards
     * @apiGroup CreditCards
     * @apiVersion 1.0.0
     *
  
     * @apiParam {String} token stripe token
     *
     * @apiSuccess {Number} id Credit Card ID.
     * @apiSuccess {String} token Stripe Credit Card token.
     * @apiSuccess {String} type  Credit Card type.
     * @apiSuccess {String} brand  Credit Card brand.
     * @apiSuccess {Number} last4 Credit Card last 4 numbers.
     * @apiSuccess {String} exp_year  Credit Card expiration year.
     * @apiSuccess {String} exp_month  Credit Card expiration month.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *      "data": {
     *         "id": 1,
     *         "token": "card_GChJ1mwFwZnN76",
     *         "type": "Visa",
     *          "brand": "Visa",
     *          "last4": "4242",
     *          "exp_year": 2020,
     *          "exp_month": 11
     *      }
     *       "message": "Resource was succesfuly updated"
     *   }   

     * @apiError UnprocessableEntity Validation errors.
     *

     * 
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *       "errors": {
     *          "token": [
     *              "The token field is required."
     *          ],
     *      }
     *   }
     */
    public function store(CreateCreditCardRequest $request)
    {
        try {            
           
            $card = $this->creditCardsService->create($request->all());

            return $this->respond(new CreditCardResource($card));

        } catch (\Throwable $th) {
            throw $th;
        }
    }


     /**
     * @api {delete} /payment-methods/cards/:id Delete
     * @apiDescription Delete credit card
     * @apiName Delete Credit Card
     * @apiGroup CreditCards
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "message": "Resource deleted"
     *       }
     *   }
   
     * @apiError CreditCardNotFound The id of the Card was not found.
   
     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 404 Not Found
     *   {
     *       "errors": "Resource does not exist"
     *   }
     * 
     */    
    public function destroy(Request $request)
    {
        try {
            
          
            $this->creditCardsService->delete($request->id);

            return $this->respond(['message' => __('general.resource_deleted')]);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
