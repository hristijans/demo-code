<?php

namespace App\Http\Controllers\Api\PaymentMethods;

use Stripe\Source;
use Stripe\Stripe;
use Stripe\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BanContactController extends Controller
{
    public function store()
    {

        $customer = \Auth::user()->stripe_id;
        $email =  \Auth::user()->email;

        $source = Source::create([
            "type" => "bancontact",
            "currency" => "eur",
            "amount" => 80,
            "owner" => [
                "email" => $email,
                "name" =>
            ],

            "redirect" => [
                "return_url" => "http://tokyo.local"
            ]
        ]);



        $card = Customer::createSource(
            $customer,
            [
                'source' => $source->id,
            ]
        );


        print_r($card);
    }

}
