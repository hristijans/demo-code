<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\FavoritesService;
use App\Http\Controllers\Controller;
use App\Http\Resources\FavoritesResource;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\CreateFavoriteRequest;

class FavoritesController extends ApiController
{
    protected $favoritesService;

    public function __construct(FavoritesService $favoritesService)
    {
        $this->favoritesService = $favoritesService;
    }

    /**
     * @api {get} /favorites Show
     * @apiDescription Show user's favorite prducts
     * @apiName Show 
     * @apiGroup Favorites
     * @apiVersion 1.0.0
     *
     *
     *
     * @apiSuccess {Number} id Favorite ID.
     * @apiSuccess {Object} product Product info
     * @apiSuccess {Number} product.id Product ID.
     * @apiSuccess {String} product.title Product title
  
     * @apiSuccess {Object} category Product category info.
     * @apiSuccess {Number} category.id Category ID.
     * @apiSuccess {String} category.category Category name.
  
     * @apiSuccess {Object} user Product owner info.
     * @apiSuccess {Number} user.id User id.
     * @apiSuccess {String} user.first_name User name.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     * "data": [
     *               {
     *               "id": 11,
     *               "product": {
     *               "id": 6,
     *               "title": "Burmashine",
     *               "description": "za budalene komsiite",
     *               "price_per_day": 654,
     *               "deposit": 500,
     *               "available_from": "2019-08-21",
     *               "available_to": "2019-08-23",
     *               "hour_from": "10:00",
     *               "hour_to": "16:00",
     *               "images": [],
     *               "category": {
     *               "id": 13,
     *               "name": "Submain11"
     *               },
     *               "user": {
     *               "id": 6,
     *               "first_name": "Tijana",
     *               "last_name": "Bogicevis",
     *               "email": "user2@rentout.com"
     *               }
     *               }
     *               },
     *               {
     *               "id": 10,
     *               "product": {
     *               "id": 5,
     *               "title": "Burmashine",
     *               "description": "za budalene komsiite",
     *               "price_per_day": 3200,
     *               "deposit": 500,
     *               "available_from": "2019-08-21",
     *               "available_to": "2019-08-23",
     *               "hour_from": "10:00",
     *               "hour_to": "16:00",
     *               "images": [],
     *               "category": {
     *               "id": 13,
     *               "name": "Submain11"
     *               },
     *               "user": {
     *               "id": 6,
     *               "first_name": "Tijana",
     *               "last_name": "Bogicevis",
     *               "email": "user2@rentout.com"
     *               }
     *               }
     *               }
     *               ],
     *               "links": {
     *               "first": "http://tokyo.test/api/favorites?page=1",
     *               "last": "http://tokyo.test/api/favorites?page=1",
     *               "prev": null,
     *               "next": null
     *               },
     *               "meta": {
     *               "current_page": 1,
     *               "from": 1,
     *               "last_page": 1,
     *               "path": "http://tokyo.test/api/favorites",
     *               "per_page": "100",
     *               "to": 2,
     *               "total": 2
     *               }
     * }
       
     */

    public function index(Request $request)
    {   
        try { 
            return FavoritesResource::collection($this->favoritesService->get($request->all()));
        } catch (\Throwable $th) {
            throw $th;
        }
    }

        /**
     * @api {post} /favorites Create
     * @apiDescription Add product to favorites
     * @apiName Create 
     * @apiGroup Favorites
     * @apiVersion 1.0.0
     *
     * @apiParam {Number} id Product id
     *
     * @apiSuccess {Number} id Favorite ID.
     * @apiSuccess {Object} product Product info
     * @apiSuccess {Number} product.id Product ID.
     * @apiSuccess {String} product.title Product title
  
     * @apiSuccess {Object} category Product category info.
     * @apiSuccess {Number} category.id Category ID.
     * @apiSuccess {String} category.category Category name.
  
     * @apiSuccess {Object} user Product owner info.
     * @apiSuccess {Number} user.id User id.
     * @apiSuccess {String} user.first_name User name.

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *           "data": {
     *           "id": 11,
     *           "product": {
     *              "id": 6,
     *              "title": "Burmashine",
     *              "description": "za budalene komsiite",
     *              "price_per_day": 654,
     *              "deposit": 500,
     *              "available_from": "2019-08-21",
     *              "available_to": "2019-08-23",
     *              "hour_from": "10:00",
     *              "hour_to": "16:00",
     *              "images": [],
     *           "category": {
     *               "id": 13,
     *               "name": "Submain11"
     *           },
     *           "user": {
     *               "id": 6,
     *               "first_name": "Tijana",
     *               "last_name": "Bogicevis",
     *               "email": "user2@rentout.com"
     *               }
     *           }
     *       }
     * }
       
     * @apiError UnprocessableEntity Validation errors.
    
     * 
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *   {
     *       "errors": {
     *           "product_id": [
     *               "The selected product id is invalid."
     *           ],
     *           
     *       }
     *   }
     */
    public function store(CreateFavoriteRequest $request)
    {
        try {
            
            $favorite = $this->favoritesService->create($request->product_id);
            
            return $this->respond((new FavoritesResource($favorite)));

        } catch (\Throwable $th) {
            throw $th;
        }
    }


     /**
     * @api {delete} /favorites/:product_id Delete
     * @apiDescription Remove product from favorites
     * @apiName Delete
     * @apiGroup Favorites
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "message": "Resource deleted"
     *       }
     *   }
   
     * @apiError FavoriteNotFound The id of the Favorite was not found.
   
     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 404 Not Found
     *   {
     *       "errors": "Resource does not exist"
     *   }
     * 
     */    
    public function destroy(Request $request)
    {
        try {
            
            $favorite = $this->favoritesService->delete($request->product_id);

            return $this->respond(['message' => __('general.resource_deleted')]);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
