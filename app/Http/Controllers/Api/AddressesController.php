<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\UsersService;

use App\Services\AddressesService;
use App\Http\Controllers\Controller;
use App\Http\Resources\AddressResource;
use App\Http\Resources\CategoryResource;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\UpdateAddressRequest;
use App\Http\Requests\Api\UpdateAddressPositionRequest;

class AddressesController extends ApiController
{
    private $addressesService;
    private $usersService;

    public function __construct(AddressesService $addressesService, UsersService $usersService)
    {
        $this->addressesService = $addressesService;
        $this->usersService     = $usersService; 
    }

    /**
     * @api {get} /addresses Show
     * @apiDescription Show user addresses
     * @apiName Show Category
     * @apiGroup Address
     * @apiVersion 1.0.0
     *
     *
     *
     * @apiSuccess {Number} id Address ID.
     * @apiSuccess {String} address_1  Address 1.
     * @apiSuccess {String} address_2  Address 2.
     * @apiSuccess {String} city    City name
     * @apiSuccess {String} zip    ZIP
     * @apiSuccess {Object} country Active status.
     * @apiSuccess {Number} country.id Country ID.
     * @apiSuccess {String} country.name Country name.
     * @apiSuccess {String} country.code Country code.
     * @apiSuccess {String} type Type.
     * @apiSuccess {Number} lat Latitude
     * @apiSuccess {Number} lng Lognitude

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     * "data": [
     *     {
     *        "id": 8,
     *       "address_1": "Dimitrov 103",
     *        "address_2": null,
     *        "city": "Prilep",
     *        "zip": "7500",
     *        "country": {
     *            "id": 1,
     *            "name": "Belgium",
     *            "code": "BE"
     *        },
     *        "type": "regular",
     *        "lat": "41.3285454",
     *        "lng": "21.5499996"
     *    },
     *    {
     *        "id": 9,
     *        "address_1": "Dimitrov 103",
     *        "address_2": null,
     *        "city": "Prilep",
     *        "zip": "7500",
     *        "country": {
     *            "id": 1,
     *            "name": "Belgium",
     *            "code": "BE"
     *        },
     *        "type": "meeting"
     *        "lat": "41.3285454",
     *        "lng": "21.5499996"
     *    }
     * ]
     * }
       
     */
    public function index()
    {
        try {
            return $this->respond(AddressResource::collection($this->addressesService->get()));
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    /**
     * @api {put} /addresses/:id Update
     * @apiDescription Update address
     * @apiName UpdateAddress
     * @apiGroup Address
     * @apiVersion 1.0.0
     *
  
     * @apiParam {String} address_1 Address 1
     * @apiParam {String} city City name
     * @apiParam {Number} country_id Counttry ID.
     * @apiParam {String} zip ZIP.
     * @apiParam {Number} lat Latitude
     * @apiParam {Number} lng Lognitude
     *
     * @apiSuccess {Number} id Address ID.
     * @apiSuccess {String} address_1  Address 1.
     * @apiSuccess {String} address_2  Address 2.
     * @apiSuccess {String} city    City name
     * @apiSuccess {String} zip    ZIP
     * @apiSuccess {Object} country Active status.
     * @apiSuccess {Number} country.id Country ID.
     * @apiSuccess {String} country.name Country name.
     * @apiSuccess {String} country.code Country code.
     * @apiSuccess {String} type Type.
     * @apiSuccess {Number} lat Latitude
     * @apiSuccess {Number} lng Lognitude

     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *          "id": 8,
     *          "address_1": "Dimitrov 103",
     *          "address_2": null,
     *          "city": "Prilep",
     *          "zip": "7500",
     *          "country": {
     *            "id": 1,
     *            "name": "Belgium",
     *            "code": "BE"
     *          },
     *          "type": "regular",
     *          "lat": "41.3285454",
     *          "lng": "21.5499996"
     *          },
     *          "message": "Resource was succesfuly updated"
     *      }
     *   }
   
     * @apiError CategoryNotFound The id of the Category was not found.
     * @apiError UnprocessableEntity Validation errors.
     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 404 Not Found
     *   {
     *       "errors": "Resource does not exist"
     *   }
     * 
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *   {
     *       "errors": {
     *           "address_1": [
     *               "The address 1 field is required."
     *           ],
     *           "city": [
     *               "The city field is required."
     *           ],
     *           "country_id": [
     *               "The country id field is required."
     *           ],
     *           "zip": [
     *               "The zip field is required."
     *           ],
     *           "lat": [
     *               "The lat field is required."
     *           ],
     *           "lng": [
     *               "The lng field is required."
     *           ]
     *       }
     *   }
     */

    public function update(UpdateAddressRequest $request)
    {
        try {
            
            $address = $this->addressesService->find($request->id);
      
            if (is_null($address)) {
              return  $this->notFound();
            } 

            $address = $this->addressesService->update($address->id, $request->all());
           
            return $this->respond(new AddressResource($address), self::RESOURCE_UPDATED);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * @api {put} /addresses/position Update Location
     * @apiDescription Update location
     * @apiName UpdateLocation
     * @apiGroup Address
     * @apiVersion 1.0.0
     *
  
     * @apiParam {Integer=1,2} show_location Show location flag



     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {    "data": {
     *           "show_location": "1"
     *       },
     *       "message": "Resource was succesfuly updated"
     *   }
   
 
     * @apiError UnprocessableEntity Validation errors.
     *
     * 
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *   {
     *  "errors": {
     *           "show_location": [
     *               "The selected show location is invalid."
     *           ]
     *       }
     *   }
     */


    public function position(UpdateAddressPositionRequest $request)
    {
        try {
               
            $position = $this->usersService->updatePosition($request->all());

            return $this->respond(["show_location" => $request->show_location], self::RESOURCE_UPDATED);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
