<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\ProductsService;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\CreateProductImageRequest;
use App\Http\Requests\Api\UpdateProductImageRequest;

class ProductImagesController extends ApiController
{

    protected $productsService;

    public function __construct(ProductsService $productsService)
    {   
        $this->productsService = $productsService;
    }

    /**
     * @api {post} /products/:id/images Add image
     * @apiDescription Add image
     * @apiName Add image
     * @apiGroup Product
     * @apiVersion 1.0.0
     *
  
     
     * @apiParam {String} image  Image in base64 format.
   
     *
     * @apiSuccess {Object} image The image object.
     * @apiSuccess {Number} image.id  Image id.
     * @apiSuccess {Number} image.product_id  Product ID.
     * @apiSuccess {String} image.image Image path.


     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *  "data": {
     *       "image": {
     *           "product_id": "3",
     *           "image": "/images/de8f721ba2e11710dc12fbf8c65092d2.jpeg",
     *           "updated_at": "2019-10-17 17:22:17",
     *           "created_at": "2019-10-17 17:22:17",
     *           "id": 8
     *           }
     *       }
     *   }   

     * @apiError UnprocessableEntity Validation errors.
     *

     * 
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 422 Unprocessable Entity
     *    {
     *           "errors": {
     *               "image": [
     *                   "The image field is required."
     *               ]                   
     *          }
     *   }
     */    
    public function store(CreateProductImageRequest $request)
    {
        try {
     
            $image = $this->productsService->addImage($request->all());

            return $this->respond(['image' => $image]);

        } catch (\Throwable $th) {
            throw $th;
        }
    }


     /**
     * @api {delete} /products/:product_id/images/:id Delete image
     * @apiDescription Delete image  by product id and image id
     * @apiName Delete Image
     * @apiGroup Product
     * @apiVersion 1.0.0
     *
     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [],
     *       "message": "Resource was succesfuly deleted"
     *   }
   
     * @apiError ProductNotFound The id of the Product was not found.
   
     *
     * @apiErrorExample Error-Response:
     *   HTTP/1.1 404 Not Found
     *   {
     *       "errors": "Resource does not exist"
     *   }
     * 
     */     

    public function destroy(UpdateProductImageRequest $request)
    {
        try {
            
            $image = $this->productsService->removeImage($request->all());

            return $this->respond([], self::RESOURCE_DELETED);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
