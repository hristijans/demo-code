<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\UsersService;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Api\UpdateUserRequest;
use App\Http\Requests\Api\UpdateAvatarRequest;
use App\Http\Requests\Api\UpdatePasswordRequest;
use App\Http\Requests\Api\UpdatePhoneRequest;

class UserController extends ApiController
{
    protected $usersService;

    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

        /**
     * @api {get} /user User
     * @apiDescription Get user info
     * @apiName UserInfo
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * 
     *
     * @apiSuccess {Number} id User ID.
     * @apiSuccess {String} first_name  First name.
     * @apiSuccess {String} last_name  Last name.
     * @apiSuccess {String} email  Email.
     * @apiSuccess {String} description  User info.
     * @apiSuccess {String} username  Username.
     * @apiSuccess {String} token  Authentication token.
     * @apiSuccess {Object} roles  User roles information.
     * @apiSuccess {Number} roles.id  Role ID.
     * @apiSuccess {String} roles.name  Role name.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "data": {
     *               "id": 12,
     *               "first_name": "John",
     *               "last_name": "Snow",
     *               "email": "johnsnow@winterfell.com",
     *               "description" : "dsadsa",
     *               "username": "john.snow",
     *               "token": "OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg",
     *               "roles": [
     *                   {
     *                       "id": 2,
     *                       "name": "user"
     *                   }
     *               ]
     *           }
     *       }    

     */
    public function index()
    {
        try {           
            return $this->respond(new UserResource(\Auth::user()));                
        } catch (\Throwable $th) {
            return $th;
        }
    }
    /**
     * @api {put} /user Edit
     * @apiDescription Edit user info
     * @apiName EditUser
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} first_name First name.
     * @apiParam {String} last_name Last name.
     * @apiParam {String} description User description.
     * @apiParam {String} gender User gender.
     * @apiParam {String} date_of_birth Date of birth.
     * @apiParam {String} phone User phone.
     *
     * @apiSuccess {Number} id User ID.
     * @apiSuccess {String} first_name  First name.
     * @apiSuccess {String} last_name  Last name.
     * @apiSuccess {String} email  Email.
     * @apiSuccess {String} username  Username.
     * @apiSuccess {String} api_token  Authentication token.
     * @apiSuccess {Object} roles  User roles information.
     * @apiSuccess {Number} roles.id  Role ID.
     * @apiSuccess {String} roles.name  Role name.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "data": {
     *               "id": 12,
     *               "first_name": "Aegon",
     *               "last_name": "Targaryen",
     *               "email": "johnsnow@winterfell.com",
     *               "username": "john.snow",
     *               "token": "OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg",
     *               "roles": [
     *                   {
     *                       "id": 2,
     *                       "name": "user"
     *                   }
     *               ]
     *           }
     *       }
     *
     * @apiError UnprocessableEntity Validation errors.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *   {
     *       "errors": {
     *           "first_name": [
     *               "The first name field is required."
     *           ],
     *           "last_name": [
     *               "The last name field is required."
     *           ],
     *           "gender": [
     *               "The gender field is required."
     *           ],
     *           "date_of_birth": [
     *               "The date of birth field is required."
     *           ],
     *           "phone": [
     *               "The phone field is required."
     *           ]
     *       }
     *   }
     */
    public function update(UpdateUserRequest $request)
    {
        try {

            $user = $this->usersService->update($request->all());
            
            return $this->respond(new UserResource($user)); 

        } catch (\Throwable $th) {
            return $th;
        }
    }


    /**
     * @api {put} /user/password Change password
     * @apiDescription Change password as logged user
     * @apiName ChangePassword
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} old_password Current password
     * @apiParam {String} new_password New password.

     *
     * @apiSuccess {String} message  Success message.
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "data": {
     *               "message": "Please check your email"              
     *           }
     *       }
     *
     * @apiError UnprocessableEntity Validation errors.
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *       {
     *           "errors": {
     *               "old_password": [
     *                   "The old password field is required."
     *               ],
     *               "new_password": [
     *                   "The new password field is required."
     *               ]                  
     *           }
     *       }
     */
    public function password(UpdatePasswordRequest $request)
    {
        try {

            $changed = $this->usersService->updatePassword($request->all());
            
            if ($changed['changed']) {
                return $this->respond(["message" => "Password was changed"]);
            }

            return response()->json([
                'errors' => ['password' => $changed['message']]
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);

        } catch (\Throwable $th) {
            throw $th;
        }
    }


    /**
     * @api {put} /user/avatar Change avatar
     * @apiDescription Change avatar as logged user
     * @apiName ChangeAvatar
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} avatar base64 encoded image     

     *
     * @apiSuccess {String} message  Success message.
     *
     * 
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "data": {
     *               "avatar": "/images/8213f121fc70398104c14d9be17a9c6e.jpeg"              
     *           }
     *       }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *       {
     *           "errors": {
     *               "avatar": [
     *                   "The avatar field is required."
     *               ]                            
     *           }
     *       }
     */
    public function avatar(UpdateAvatarRequest $request)
    {
        try {
            $avatar = $this->usersService->avatar($request->avatar);
            return $this->respond(['avatar' => $avatar]);

        } catch (\Throwable $th) {
            throw $th;
        }
        
    }

        /**
     * @api {put} /user/phone Change phone
     * @apiDescription Change phone as logged user
     * @apiName ChangePhone
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} phone     

     *
     * @apiSuccess {String} message  Success message.
     *
     * 
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "data": {
     *               "phone": "23545235"           
     *           }
     *       }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *       {
     *           "errors": {
     *               "phone": [
     *                   "The phone field is required."
     *               ]                            
     *           }
     *       }
     */
    public function phone(UpdatePhoneRequest $request)
    {
        try {
            $phone = $this->usersService->updatePhone($request->phone);
            return $this->respond(['phone' => $phone]);

        } catch (\Throwable $th) {
            throw $th;
        }
        
    }

        /**
     * @api {put} /user/all All users
     * @apiDescription Get All users
     * @apiName AllYsers
     * @apiGroup User
     * @apiVersion 1.0.0
     *
     *    

     *
     * @apiSuccess {Number} id  User ID.
     * @apiSuccess {String} first_name  First name.
     * @apiSuccess {String} last_name Last name.
     * @apiSuccess {String} email Email
     * 
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *          "data": [
     *           {
     *               "id": 12,
     *               "first_name": "Ruthie",
     *               "last_name": "Schumm",
     *               "email": "ruthie@buyer.com",
     *               "rating": {
     *                   "rating": 0,
     *                   "num_ratings": 0
     *               }
     *           },
     *           {
     *               "id": 11,
     *               "first_name": "John",
     *               "last_name": "Quigley",
     *               "email": "john@seller.com",
     *               "rating": {
     *                   "rating": 5,
     *                   "num_ratings": 3
     *               }
     *           },
     *           {
     *               "id": 10,
     *               "first_name": "Hristijan",
     *               "last_name": "Stojanoski",
     *               "email": "hs@dev.com",
     *               "rating": {
     *                   "rating": 0,
     *                   "num_ratings": 0
     *               }
     *           },
     *           ],
     *       }
     *
     */    
    public function all()
    {
        try {
            $users = $this->usersService->all();
            return $this->respond($users);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
