<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\HelpService;
use App\Http\Controllers\Controller;
use App\Http\Resources\HelpResource;
use App\Http\Controllers\Api\ApiController;

class HelpController extends ApiController
{

    protected $helpService;

    public function __construct(HelpService $helpService)
    {
        $this->helpService = $helpService;
    }
    /**
     * @api {get} /help Show
     * @apiDescription Show help menu texts
     * @apiName Show Help 
     * @apiGroup Help
     * @apiVersion 1.0.0
     *
 

     *
     * @apiSuccess {String} title Title.
     * @apiSuccess {String} content HTML text
     * @apiSuccess {Number} order Order by.


     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": [
     *           {
     *           {
     *               "title": "Haley",
     *           {
     *               "content": "<html><head><title>Voluptates et error et.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">ut</label><input type=\"text\" id=\"username\"><label for=\"password\">eius</label><input type=\"password\" id=\"password\"></form>Optio delectus ea porro quia.<p>Est non non animi deserunt blanditiis enim sapiente impedit numquam est quo ipsum.</p></body></html>\n",
     *               "order": 3
     *           },
     *           {
     *               "title": "Okon",
     *               "content": "<html><head><title>Et voluptatem est.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">optio</label><input type=\"text\" id=\"username\"><label for=\"password\">impedit</label><input type=\"password\" id=\"password\"></form><i>Quod cumque perferendis qui in ut.</i><h3>Officia rerum officiis neque ut blanditiis quod ut.</h3>Molestiae magnam deleniti ab officiis tenetur unde dignissimos.</body></html>\n",
     *               "order": 4
     *           },
     *           {
     *               "title": "Johnston",
     *               "content": "<html><head><title>Quia et.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">reiciendis</label><input type=\"text\" id=\"username\"><label for=\"password\">voluptas</label><input type=\"password\" id=\"password\"></form><i>Voluptatum aut odio explicabo modi sed eius nisi qui sapiente sed.</i><h3>Exercitationem quaerat repellendus culpa eius dolorem quo et iure corporis.</h3></body></html>\n",
     *               "order": 4
     *           },
     *           {
     *               "title": "Nader",
     *               "content": "<html><head><title>Et.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">placeat</label><input type=\"text\" id=\"username\"><label for=\"password\">tempora</label><input type=\"password\" id=\"password\"></form><ul><li>Non.</li><li>Repellat et architecto.</li><li>Nihil repudiandae.</li><li>Dolor rem inventore magnam.</li><li>Odio omnis.</li><li>Est.</li><li>Quibusdam eos quasi.</li><li>Aperiam voluptas dignissimos consequuntur.</li></ul></body></html>\n",
     *               "order": 6
     *           },
     *           {
     *               "title": "Marks",
     *               "content": "<html><head><title>Nihil officiis.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">sit</label><input type=\"text\" id=\"username\"><label for=\"password\">quis</label><input type=\"password\" id=\"password\"></form><h1>Doloremque sint dignissimos accusantium.</h1>Modi facilis consequatur ipsam et aliquid at qui sint qui voluptatem harum nam.Explicabo qui magni et enim dolor deserunt porro.</body></html>\n",
     *               "order": 6
     *           },
     *           {
     *               "title": "Hand",
     *               "content": "<html><head><title>Sunt necessitatibus exercitationem tempore neque voluptatem.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">rerum</label><input type=\"text\" id=\"username\"><label for=\"password\">commodi</label><input type=\"password\" id=\"password\"></form><h3>Adipisci vel nisi vitae explicabo.</h3><p>Neque corporis qui ducimus excepturi reiciendis sint odio.</p>Ut rerum quis maiores.</body></html>\n",
     *               "order": 6
     *           },
     *           {
     *               "title": "Upton",
     *               "content": "<html><head><title>Maxime dignissimos illum inventore rerum cupiditate ea provident sint.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">inventore</label><input type=\"text\" id=\"username\"><label for=\"password\">praesentium</label><input type=\"password\" id=\"password\"></form>Rerum autem atque similique aut consequatur delectus quaerat officiis.Provident ex harum voluptas aut nihil velit eum.</body></html>\n",
     *               "order": 7
     *           },
     *           {
     *               "title": "Johnston",
     *               "content": "<html><head><title>Explicabo.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">reprehenderit</label><input type=\"text\" id=\"username\"><label for=\"password\">voluptatibus</label><input type=\"password\" id=\"password\"></form><span>Reiciendis corporis assumenda illum corrupti beatae qui dignissimos qui illo sunt.</span><h2>Tempora ea iure blanditiis voluptate non rerum.</h2></body></html>\n",
     *               "order": 8
     *           }
     *       ]
     *}
   
     */
    public function index()
    {
        try {
         
            return $this->respond(HelpResource::collection($this->helpService->get()));

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * @api {get} /search?term=:string
     * @apiDescription Search help texts
     * @apiName Search Help
     * @apiGroup Help
     * @apiVersion 1.0.0
     *
  
     * @apiParam {String} term Term to search.


     *
     * @apiSuccess {String} title Title.
     * @apiSuccess {String} content HTML text.
     * @apiSuccess {Number} order Order by.


     *
     * @apiSuccessExample Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *   "data": [
     *            {
     *               "title": "Haley",
     *               "content": "<html><head><title>Voluptates et error et.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">ut</label><input type=\"text\" id=\"username\"><label for=\"password\">eius</label><input type=\"password\" id=\"password\"></form>Optio delectus ea porro quia.<p>Est non non animi deserunt blanditiis enim sapiente impedit numquam est quo ipsum.</p></body></html>\n",
     *               "order": 3
     *           }
     *   ]
     *}
     */    

    public function search(Request $request)
    {
        try {
         
            return $this->respond(HelpResource::collection($this->helpService->search($request->term)));

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
