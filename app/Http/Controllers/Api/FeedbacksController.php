<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateFeedbackRequest;
use App\Http\Resources\FeedbacksResource;
use App\Services\FeedbacksService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbacksController extends ApiController
{
    protected $feedbacksService;

    public function __construct(FeedbacksService $feedbacksService)
    {
        $this->feedbacksService = $feedbacksService;
    }

    
     /**
     * @api {get} /feedbacks Show All
     * @apiDescription Show all feedbacks
     * @apiName Show 
     * @apiGroup Feedbacks
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} rating Rating for the product.
     * @apiSuccess {String} comment Comment for the product.
     * @apiSuccess {Array} comments  List of comments for the feedback.
     * @apiSuccess {Object} user User Object.
     * @apiSuccess {Object} product Product Object.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": [
     *       {
     *       "rating": 5,
     *       "comment": "very good",
     *       "comments": [],
     *       "user": {
     *           "id": 6,
     *           "first_name": "MrsJaleel",
     *           "last_name": "Schuppe",
     *           "email": "Jaleel4@gmail.com",
     *           "description": null,
     *           "username": "Jaleel4@gmail.com",
     *           "date_of_birth": "",
     *           "phone": "",
     *           "gender": "",
     *           "avatar": "",
     *           "show_location": 1,
     *           "token": "S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr",
     *           "roles": [
     *               {
     *               "id": 2,
     *               "name": "user"
     *               }
     *           ],
     *       },
     *       "product": {
     *           "id": 2,
     *           "title": "Product 2",
     *           "description": "this is my sec product",
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "available_from": "2019-11-01",
     *           "available_to": "2019-11-07",
     *           "hour_from": "10:00",
     *           "hour_to": "16:00",
     *           "is_favorite": false,
     *           "distance": 5985.64,
     *           "rating": 5,
     *           "images": [
     *               {
     *                   "id": 1,
     *                   "product_id": 2,
     *                   "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               },
     *               {
     *                   "id": 2,
     *                   "product_id": 2,
     *                   "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               }
     *           ],
     *           "category": {
     *               "id": 1,
     *               "name": "Main Category"
     *           },
     *           "user": {
     *               "id": 2,
     *               "first_name": "Marija1",
     *               "last_name": "Kovacevic",
     *               "email": "user@example.com"
     *           }
     *           }
     *       }
     *       ],
     *       "links": {
     *           "first": "http://rentout.test/api/feedbacks?page=1",
     *           "last": "http://rentout.test/api/feedbacks?page=1",
     *           "prev": null,
     *           "next": null
     *       },
     *       "meta": {
     *           "current_page": 1,
     *           "from": 1,
     *           "last_page": 1,
     *           "path": "http://rentout.test/api/feedbacks",
     *           "per_page": 15,
     *           "to": 1,
     *           "total": 1
     *       }
     *   }
     *
     */
    public function index()
    {
        try {
            $feedback = $this->feedbacksService->get();
            return FeedbacksResource::collection($feedback);
        } catch (\Throwable $th) {
            throw $th;
        }
    }


        
     /**
     * @api {get} /feedbacks/:id Show Feedback
     * @apiDescription Show feedback by id
     * @apiName ShowFeedback
     * @apiGroup Feedbacks
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} rating Rating for the product.
     * @apiSuccess {String} comment Comment for the product.
     * @apiSuccess {Array} comments  List of comments for the feedback.
     * @apiSuccess {Object} user User Object.
     * @apiSuccess {Object} product Product Object.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": 
     *       {
     *       "rating": 5,
     *       "comment": "very good",
     *       "comments": [],
     *       "user": {
     *           "id": 6,
     *           "first_name": "MrsJaleel",
     *           "last_name": "Schuppe",
     *           "email": "Jaleel4@gmail.com",
     *           "description": null,
     *           "username": "Jaleel4@gmail.com",
     *           "date_of_birth": "",
     *           "phone": "",
     *           "gender": "",
     *           "avatar": "",
     *           "show_location": 1,
     *           "token": "S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr",
     *           "roles": [
     *               {
     *               "id": 2,
     *               "name": "user"
     *               }
     *           ],
     *       },
     *       "product": {
     *           "id": 2,
     *           "title": "Product 2",
     *           "description": "this is my sec product",
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "available_from": "2019-11-01",
     *           "available_to": "2019-11-07",
     *           "hour_from": "10:00",
     *           "hour_to": "16:00",
     *           "is_favorite": false,
     *           "distance": 5985.64,
     *           "rating": 5,
     *           "images": [
     *               {
     *                   "id": 1,
     *                   "product_id": 2,
     *                   "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               },
     *               {
     *                   "id": 2,
     *                   "product_id": 2,
     *                   "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               }
     *           ],
     *           "category": {
     *               "id": 1,
     *               "name": "Main Category"
     *           },
     *           "user": {
     *               "id": 2,
     *               "first_name": "Marija1",
     *               "last_name": "Kovacevic",
     *               "email": "user@example.com"
     *           }
     *           }
     *       }
     *       
     *   }
     *
     */
    public function show()
    {
        try {

            $feedback = $this->feedbacksService->find(request()->id);
            return (new FeedbacksResource($feedback));
        } catch (\Throwable $th) {
            throw $th;
        }
    }

        /**
     * @api {post} /feedbacks Create
     * @apiDescription Create new feedback
     * @apiName Create Feedback
     * @apiGroup Feedbacks
     * @apiVersion 1.0.0
     *
     * @apiParam {Number} product_id Product ID.
     * @apiParam {Number} rating Rating for the product.
     * @apiParam {String} comment Comment for the product.
     *
  
     
     * @apiSuccess {Number} rating Rating for the product.
     * @apiSuccess {String} comment Comment for the product.
     * @apiSuccess {Array} comments  List of comments for the feedback.
     * @apiSuccess {Object} user User Object.
     * @apiSuccess {Object} product Product Object.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *   "data": {
     *       "user": {
     *           "id": 6,
     *           "first_name": "MrsJaleel",
     *           "last_name": "Schuppe",
     *           "email": "Jaleel4@gmail.com",
     *           "description": null,
     *           "username": "Jaleel4@gmail.com",
     *           "date_of_birth": "",
     *           "phone": "",
     *           "gender": "",
     *           "avatar": "",
     *           "show_location": 1,
     *           "token": "S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr",
     *           "roles": [
     *               {
     *               "id": 2,
     *               "name": "user"
     *               }
     *           ],
     *       },
     *       "product": {
     *           "id": 2,
     *           "title": "Product 2",
     *           "description": "this is my sec product",
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "available_from": "2019-11-01",
     *           "available_to": "2019-11-07",
     *           "hour_from": "10:00",
     *           "hour_to": "16:00",
     *           "is_favorite": false,
     *           "distance": 5985.64,
     *           "rating": 5,
     *           "images": [
     *               {
     *               "id": 1,
     *               "product_id": 2,
     *               "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *               "created_at": "2019-11-08 20:21:01",
     *               "updated_at": "2019-11-08 20:21:01"
     *               },
     *               {
     *               "id": 2,
     *               "product_id": 2,
     *               "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *               "created_at": "2019-11-08 20:21:01",
     *               "updated_at": "2019-11-08 20:21:01"
     *               }
     *           ],
     *           "category": {
     *               "id": 1,
     *               "name": "Main Category"
     *           },
     *           "user": {
     *               "id": 2,
     *               "first_name": "Marija",
     *               "last_name": "Kovacevic",
     *               "email": "user@example.com"
     *           }
     *       },
     *       "rating": "5",
     *       "comment": "very good",
     *       "comments": [],
     *   }
     * }
     *
     * @apiError UnprocessableEntity Validation errors.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *       {
     *           "errors": {
     *               "product_id": [
     *                   "The product id field is required."
     *               ],
     *               "rating": [
     *                   "The rating field is required."
     *               ],
     *               "comment": [
     *                   "The comment field is required."
     *               ],
     *           }
     *       }
     */
    public function store(CreateFeedbackRequest $request)
    {
        try {
            $feedback = $this->feedbacksService->create($request->all());

            return $this->respond(new FeedbacksResource($feedback));
        } catch (\Throwable $th) {
            throw $th;
        }
    }


     /**
     * @api {get} /feedbacks/tenant Tenant feedbacks
     * @apiDescription Show all feedbacks created from the tenant
     * @apiName ShowTenantFeedbacks
     * @apiGroup Feedbacks
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} rating Rating for the product.
     * @apiSuccess {String} comment Comment for the product.
     * @apiSuccess {Array} comments  List of comments for the feedback.
     * @apiSuccess {Object} user User Object.
     * @apiSuccess {Object} product Product Object.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": [
     *       {
     *       "rating": 5,
     *       "comment": "very good",
     *       "comments": [],
     *       "user": {
     *           "id": 6,
     *           "first_name": "MrsJaleel",
     *           "last_name": "Schuppe",
     *           "email": "Jaleel4@gmail.com",
     *           "description": null,
     *           "username": "Jaleel4@gmail.com",
     *           "date_of_birth": "",
     *           "phone": "",
     *           "gender": "",
     *           "avatar": "",
     *           "show_location": 1,
     *           "token": "S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr",
     *           "roles": [
     *               {
     *               "id": 2,
     *               "name": "user"
     *               }
     *           ],
     *       },
     *       "product": {
     *           "id": 2,
     *           "title": "Product 2",
     *           "description": "this is my sec product",
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "available_from": "2019-11-01",
     *           "available_to": "2019-11-07",
     *           "hour_from": "10:00",
     *           "hour_to": "16:00",
     *           "is_favorite": false,
     *           "distance": 5985.64,
     *           "rating": 5,
     *           "images": [
     *               {
     *                   "id": 1,
     *                   "product_id": 2,
     *                   "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               },
     *               {
     *                   "id": 2,
     *                   "product_id": 2,
     *                   "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               }
     *           ],
     *           "category": {
     *               "id": 1,
     *               "name": "Main Category"
     *           },
     *           "user": {
     *               "id": 2,
     *               "first_name": "Marija1",
     *               "last_name": "Kovacevic",
     *               "email": "user@example.com"
     *           }
     *           }
     *       }
     *       ],
     *       "links": {
     *           "first": "http://rentout.test/api/feedbacks?page=1",
     *           "last": "http://rentout.test/api/feedbacks?page=1",
     *           "prev": null,
     *           "next": null
     *       },
     *       "meta": {
     *           "current_page": 1,
     *           "from": 1,
     *           "last_page": 1,
     *           "path": "http://rentout.test/api/feedbacks",
     *           "per_page": 15,
     *           "to": 1,
     *           "total": 1
     *       }
     *   }
     *
     */

    public function tenant()
    {
        try {
            $feedback = $this->feedbacksService->tenant();


            return  FeedbacksResource::collection($feedback);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

     /**
     * @api {get} /feedbacks/owner Owner feedbacks
     * @apiDescription Show all feedbacks created for the owner
     * @apiName ShowOwnerFeedbacks    
     * @apiGroup Feedbacks
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Number} rating Rating for the product.
     * @apiSuccess {String} comment Comment for the product.
     * @apiSuccess {Array} comments  List of comments for the feedback.
     * @apiSuccess {Object} user User Object.
     * @apiSuccess {Object} product Product Object.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": [
     *       {
     *       "rating": 5,
     *       "comment": "very good",
     *       "comments": [],
     *       "user": {
     *           "id": 6,
     *           "first_name": "MrsJaleel",
     *           "last_name": "Schuppe",
     *           "email": "Jaleel4@gmail.com",
     *           "description": null,
     *           "username": "Jaleel4@gmail.com",
     *           "date_of_birth": "",
     *           "phone": "",
     *           "gender": "",
     *           "avatar": "",
     *           "show_location": 1,
     *           "token": "S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr",
     *           "roles": [
     *               {
     *               "id": 2,
     *               "name": "user"
     *               }
     *           ],
     *       },
     *       "product": {
     *           "id": 2,
     *           "title": "Product 2",
     *           "description": "this is my sec product",
     *           "price_per_day": 25000,
     *           "deposit": 9850,
     *           "available_from": "2019-11-01",
     *           "available_to": "2019-11-07",
     *           "hour_from": "10:00",
     *           "hour_to": "16:00",
     *           "is_favorite": false,
     *           "distance": 5985.64,
     *           "rating": 5,
     *           "images": [
     *               {
     *                   "id": 1,
     *                   "product_id": 2,
     *                   "image": "/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               },
     *               {
     *                   "id": 2,
     *                   "product_id": 2,
     *                   "image": "/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg",
     *                   "created_at": "2019-11-08 20:21:01",
     *                   "updated_at": "2019-11-08 20:21:01"
     *               }
     *           ],
     *           "category": {
     *               "id": 1,
     *               "name": "Main Category"
     *           },
     *           "user": {
     *               "id": 2,
     *               "first_name": "Marija1",
     *               "last_name": "Kovacevic",
     *               "email": "user@example.com"
     *           }
     *           }
     *       }
     *       ],
     *       "links": {
     *           "first": "http://rentout.test/api/feedbacks?page=1",
     *           "last": "http://rentout.test/api/feedbacks?page=1",
     *           "prev": null,
     *           "next": null
     *       },
     *       "meta": {
     *           "current_page": 1,
     *           "from": 1,
     *           "last_page": 1,
     *           "path": "http://rentout.test/api/feedbacks",
     *           "per_page": 15,
     *           "to": 1,
     *           "total": 1
     *       }
     *   }
     *
     */

    public function owner()
    {
        try {
            $feedback = $this->feedbacksService->owner();

            return  FeedbacksResource::collection($feedback);
        } catch (\Throwable $th) {
            throw $th;
        }
    }    
}
