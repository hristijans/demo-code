<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    //use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(Request $request)
    {
        $token = $request->token;
        
        return view('password.reset')->with(['token' => $token]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'token' => 'required|exists:password_resets',
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        ]);
        

        $user_token = \DB::table('password_resets')->where(['token' => $request->token])->get()->first();

        $user = User::where(['email' => $user_token->email])->get()->first();

        $user->password = \Hash::make($request->password);

        $user->save();

        \DB::table('password_resets')->where(['email' => $user->email])->delete();
        
       return view('password.changed');
    }
}
