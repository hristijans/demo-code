<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\FormRequest;

class SearchCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sort'      => 'in:asc,desc',
            'order_by'  => 'in:id,name',
            'per_page'  => 'numeric'
        ];
    }
}
