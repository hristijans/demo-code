<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\FormRequest;

class PreviewOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'from_date' => 'required|date_format:Y-m-d|before_or_equal:to_date',
            'to_date'   => 'required|date_format:Y-m-d|after_or_equal:from_date',
        ];
    }
}
