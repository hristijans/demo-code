<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique:category_translations',
            'description'   => 'required',
            'icon'          =>  'required',
            'parent_id'     => 'required'
        ];
    }
}
