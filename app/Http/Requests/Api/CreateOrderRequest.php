<?php

namespace App\Http\Requests\Api;

use App\Rules\AfterToday;
use App\Http\Requests\Api\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [          
            'product_id' => 'required',
            'from_date' => 'required|date_format:Y-m-d|before:to_date',
            'from_date' => [
                'required',
                'date_format:Y-m-d',
                'before:to_date',
                new AfterToday
            ],
            'to_date'   => 'required|date_format:Y-m-d|after:from_date',
            'payment_token' => 'required',
            'payment_type'  => 'required|in:creditcard,bancontact',
            'message'   => 'required',
        ];
    }
}
