<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\FormRequest;


class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            // 'lat' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            // 'lng' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
    
            'price_per_day' => 'required|numeric',
            'deposit' => 'required|numeric',
            'available_from' => 'required|date_format:Y-m-d|before_or_equal:available_to',
            'available_to' => 'required|date_format:Y-m-d|after_or_equal:available_from',
            //'hour_from' => 'required|regex:/^(?:([01]?\d|2[0-3]):([0-5]?\d))$/',
            //'hour_to' => 'required|regex:/^(?:([01]?\d|2[0-3]):([0-5]?\d))$/'
            'hour_from' => 'required',
            'hour_to' => 'required',
           // 'images' => 'required'
        ];
    }
}
