<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\FormRequest;

class UpdateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        return array_replace_recursive(
            parent::all(),
            $this->route()->parameters()
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|exists:addresses,id',
            'address_1'     => 'required',
            //'address_2'     => 'required',
            'city'          => 'required',
            'country_id'    => 'required',
            'zip'           => 'required',
            'lat'           => 'required',
            'lng'           => 'required'
        ];
    }
}
