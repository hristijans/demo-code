<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\RolesResource;
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'email'         => $this->email,
            'description'   => $this->description,
            'username'      => $this->username,
            'date_of_birth' => $this->date_of_birth,
            'phone'         => $this->phone,
            'gender'        => $this->gender,
            'avatar'        => $this->avatar,
            'rating'        => $this->rating,
            'show_location' => $this->show_location,
            'token'         => $this->api_token,
            'roles'         => RolesResource::collection($this->roles)
        ];
    }
}
