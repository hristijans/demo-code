<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CreditCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'token' => $this->token,
            'type'  => $this->type,
            'brand' => $this->brand,
            'last4' => $this->last4,
            'exp_year' => (string)$this->exp_year,
            'exp_month' => (string)$this->exp_month,
        ];
    }
}
