<?php

namespace App\Http\Resources;

use App\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'description' => $this->description,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'rating' => $this->rating,
            'products' => ProductResource::collection($this->products),
            'address' => [
                'address_1' => $this->address->first()->address_1,
                'city'  => $this->address->first()->city,
                'country' => $this->address->first()->country->name,
                'zip' => $this->address->first()->zip
            ]
        ];
    }
}
