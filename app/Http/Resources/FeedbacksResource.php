<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class FeedbacksResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'rating'    => $this->rating,
            'comment'   => $this->comment,
            'comments'  => $this->comments->load('user'),
            'is_mine'   => $this->is_mine,
            'created_at'    => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),
            'user'      => (new UserResource($this->user)),
            'product'   => (new ProductResource($this->product)),
        ];
    }
}
