<?php

namespace App\Http\Resources;

use App\Http\Resources\CountryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'address_1'     => $this->address_1,
            'address_2'     => $this->address_2,
            'city'          => $this->city,
            'zip'           => $this->zip,
            'country'       => new CountryResource($this->country),
            'type'          => $this->type ,
            'lat'           => $this->lat,
            'lng'           => $this->lng
        ];
    }
}
