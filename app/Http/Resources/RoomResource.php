<?php

namespace App\Http\Resources;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RoomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'last_user'     => $this->user_id != 0 ? $this->user->full_name : null,
            'last_message'  => $this->message_id != 0 ? $this->message->message : null ,
            'messages'      => $this->messages->load('user'),
            'participants'  => $this->users,
        ];
    }
}
