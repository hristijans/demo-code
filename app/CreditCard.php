<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'user_id',
        'token',
        'brand',
        'type',
        'last4', 
        'exp_year', 
        'exp_month', 
    ];
}
