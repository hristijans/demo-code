<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// apidoc

Route::post('/register', 'Api\RegisterController@store')->name('register.create');
Route::post('/login',  'Api\LoginController@login')->name('login.login');
Route::post('/forgotpassword', 'Api\ForgotpasswordController@forgotpassword');
Route::post('/forgotpassword/reset', 'Api\ForgotpasswordController@reset');

Route::post('/social/login', 'Api\SocialLogin\LoginController@store');

Route::middleware('auth:api')->get('/user', 'Api\UserController@index');

// apidoc
Route::get('/categories', 'Api\CategoriesController@index');
Route::get('/categories/search', 'Api\CategoriesController@search');
Route::get('/categories/{id}', 'Api\CategoriesController@index');


Route::get('/admin/statistics', 'Api\AdminStatisticsController@index');

Route::get('products/last', 'Api\ProductsController@last');


Route::middleware('auth:api')->group(function () {


    Route::post('/categories', 'Api\CategoriesController@store');
    Route::put('/categories/{id}', 'Api\CategoriesController@update');
    Route::delete('/categories/{id}', 'Api\CategoriesController@destroy');



    Route::get('/user/{id}/details', 'Api\UserDetailsController@index');
    Route::put('/user', 'Api\UserController@update');
    Route::put('/user/password', 'Api\UserController@password');
    Route::put('/user/avatar', 'Api\UserController@avatar');
    Route::put('/user/phone', 'Api\UserController@phone');
    Route::get('/user/all', 'Api\UserController@all');

    Route::put('/user/report', 'Api\UserReportsController@update');


    Route::get('/owner/home', 'Api\OwnerHomeController@index');

    Route::get('/products/{id}/busy', 'Api\ProductsController@busy');
    Route::get('/products', 'Api\ProductsController@index');
    Route::get('/products/search', 'Api\ProductsController@search');
    Route::get('/products/{id}', 'Api\ProductsController@show');


    Route::post('/products', 'Api\ProductsController@store')->name('products.create');
    Route::put('/products/{id}/status', 'Api\ProductsController@status');
    Route::put('/products/{id}', 'Api\ProductsController@update');

    Route::delete('/products/{id}', 'Api\ProductsController@destroy');
    Route::get('/products/{id}/feedbacks', 'Api\ProductFeedbacksController@index');

    // product images
    Route::post('/products/{product_id}/images', 'Api\ProductImagesController@store');
    Route::delete('/products/{product_id}/images/{id}', 'Api\ProductImagesController@destroy');


    Route::get('/about', 'Api\AboutController@index');

    Route::get('/help', 'Api\HelpController@index');
    Route::get('/help/search', 'Api\HelpController@search');

    // favorites
    Route::get('/favorites', 'Api\FavoritesController@index');
    Route::post('/favorites', 'Api\FavoritesController@store');
    Route::delete('/favorites/{product_id}', 'Api\FavoritesController@destroy');

    //addresses - apidoc
    Route::get('/addresses', 'Api\AddressesController@index');
    Route::put ('/addresses/position', 'Api\AddressesController@position');
    Route::put('/addresses/{id}', 'Api\AddressesController@update');


    // feedbacks
    Route::get('/feedbacks', 'Api\FeedbacksController@index');
    Route::get('/feedbacks/tenant', 'Api\FeedbacksController@tenant');
    Route::get('/feedbacks/owner', 'Api\FeedbacksController@owner');
    Route::get('/feedbacks/{id}', 'Api\FeedbacksController@show');
    Route::post('/feedbacks', 'Api\FeedbacksController@store');

    Route::get('/feedbacks/{id}/comments', 'Api\FeedbackCommentsController@index');
    Route::post('/feedbacks/{id}/comments', 'Api\FeedbackCommentsController@store');


    /**Payment methods */
    Route::get('payment-methods/cards/', 'Api\PaymentMethods\CreditCardsController@index');
    Route::post('payment-methods/cards/', 'Api\PaymentMethods\CreditCardsController@store');
    Route::delete('payment-methods/cards/{id}', 'Api\PaymentMethods\CreditCardsController@destroy');

    Route::post('payment-methods/bancontact/', 'Api\PaymentMethods\BanContactController@store');


    /** Orders */
    Route::get('orders/tenant', 'Api\OrdersBuyerController@index');
    Route::get('orders/owner', 'Api\OrdersSellerController@index');

    Route::post('orders', 'Api\OrdersController@store');
    Route::post('orders/preview', 'Api\OrdersController@preview');
    Route::get('orders', 'Api\OrdersController@index');
    Route::get('orders/{id}', 'Api\OrdersController@show');
    Route::get('orders/search', 'Api\OrdersController@search');


    Route::get('/chat/rooms', 'Api\Chat\RoomsController@index');
    Route::get('/chat/rooms/{id}', 'Api\Chat\RoomsController@show');
    Route::post('/chat/rooms', 'Api\Chat\RoomsController@store');
    Route::post('/chat/rooms/bulk', 'Api\Chat\RoomsController@bulk');
    Route::post('/chat/rooms/admin', 'Api\Chat\RoomsController@admin');
    Route::delete('/chat/rooms/{id}', 'Api\Chat\RoomsController@destroy');

    Route::post('/chat/messages', 'Api\Chat\MessagesController@store');



    Route::post('/app/feedbacks', 'Api\AppFeedbacksController@store');
    Route::get('/app/feedbacks', 'Api\AppFeedbacksController@index');
    Route::get('/app/feedbacks/user', 'Api\AppFeedbacksController@user');
    Route::get('/app/feedbacks/{id}', 'Api\AppFeedbacksController@show');


    Route::post('/app/feedbacks/{id}/comments', 'Api\AppFeedbackCommentsController@store');


});

Route::get('/tools', 'Api\ToolsController@index');
Route::post('/tools', 'Api\ToolsController@store');
Route::put('/tools/{id}', 'Api\ToolsController@update');
Route::delete('/tools/{id}', 'Api\ToolsController@destroy');

Route::get('/countries', 'Api\CountriesController@index');
