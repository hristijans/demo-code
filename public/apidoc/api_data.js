define({ "api": [
  {
    "type": "get",
    "url": "/about",
    "title": "",
    "description": "<p>Show about texts</p>",
    "name": "Show_About",
    "group": "About",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Text.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"title\": \"About rentout\",\n          \"content\": \"Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa, temporibus mollitia animi sit in voluptatum non dolore officia numquam quo pariatur reprehenderit fuga, repudiandae labore cum at ratione blanditiis.\",\n          \"order\": 1\n      }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AboutController.php",
    "groupTitle": "About"
  },
  {
    "type": "get",
    "url": "/addresses",
    "title": "Show",
    "description": "<p>Show user addresses</p>",
    "name": "Show_Category",
    "group": "Address",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Address ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>Address 1.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address_2",
            "description": "<p>Address 2.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zip",
            "description": "<p>ZIP</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "country",
            "description": "<p>Active status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "country.id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country.name",
            "description": "<p>Country name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country.code",
            "description": "<p>Country code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Lognitude</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n\"data\": [\n    {\n       \"id\": 8,\n      \"address_1\": \"Dimitrov 103\",\n       \"address_2\": null,\n       \"city\": \"Prilep\",\n       \"zip\": \"7500\",\n       \"country\": {\n           \"id\": 1,\n           \"name\": \"Belgium\",\n           \"code\": \"BE\"\n       },\n       \"type\": \"regular\",\n       \"lat\": \"41.3285454\",\n       \"lng\": \"21.5499996\"\n   },\n   {\n       \"id\": 9,\n       \"address_1\": \"Dimitrov 103\",\n       \"address_2\": null,\n       \"city\": \"Prilep\",\n       \"zip\": \"7500\",\n       \"country\": {\n           \"id\": 1,\n           \"name\": \"Belgium\",\n           \"code\": \"BE\"\n       },\n       \"type\": \"meeting\"\n       \"lat\": \"41.3285454\",\n       \"lng\": \"21.5499996\"\n   }\n]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressesController.php",
    "groupTitle": "Address"
  },
  {
    "type": "put",
    "url": "/addresses/:id",
    "title": "Update",
    "description": "<p>Update address</p>",
    "name": "UpdateAddress",
    "group": "Address",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>Address 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>Counttry ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zip",
            "description": "<p>ZIP.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Lognitude</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Address ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>Address 1.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address_2",
            "description": "<p>Address 2.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zip",
            "description": "<p>ZIP</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "country",
            "description": "<p>Active status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "country.id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country.name",
            "description": "<p>Country name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country.code",
            "description": "<p>Country code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Lognitude</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n       \"id\": 8,\n       \"address_1\": \"Dimitrov 103\",\n       \"address_2\": null,\n       \"city\": \"Prilep\",\n       \"zip\": \"7500\",\n       \"country\": {\n         \"id\": 1,\n         \"name\": \"Belgium\",\n         \"code\": \"BE\"\n       },\n       \"type\": \"regular\",\n       \"lat\": \"41.3285454\",\n       \"lng\": \"21.5499996\"\n       },\n       \"message\": \"Resource was succesfuly updated\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n    \"errors\": {\n        \"address_1\": [\n            \"The address 1 field is required.\"\n        ],\n        \"city\": [\n            \"The city field is required.\"\n        ],\n        \"country_id\": [\n            \"The country id field is required.\"\n        ],\n        \"zip\": [\n            \"The zip field is required.\"\n        ],\n        \"lat\": [\n            \"The lat field is required.\"\n        ],\n        \"lng\": [\n            \"The lng field is required.\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressesController.php",
    "groupTitle": "Address"
  },
  {
    "type": "put",
    "url": "/addresses/position",
    "title": "Update Location",
    "description": "<p>Update location</p>",
    "name": "UpdateLocation",
    "group": "Address",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "allowedValues": [
              "1",
              "2"
            ],
            "optional": false,
            "field": "show_location",
            "description": "<p>Show location flag</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{    \"data\": {\n        \"show_location\": \"1\"\n    },\n    \"message\": \"Resource was succesfuly updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 422 Unprocessable Entity\n {\n\"errors\": {\n         \"show_location\": [\n             \"The selected show location is invalid.\"\n         ]\n     }\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressesController.php",
    "groupTitle": "Address"
  },
  {
    "type": "get",
    "url": "/admin/statistics",
    "title": "Statistics",
    "description": "<p>Show admin statistics</p>",
    "name": "AdminStats",
    "group": "Admin",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "registered_users",
            "description": "<p>Number of registered users</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "registered_users_today",
            "description": "<p>Number of registerd users today</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "completed_transactions",
            "description": "<p>Number of completed transactions</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rented_tools",
            "description": "<p>Number of rented tools.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "categories",
            "description": "<p>List of popular categories.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "reviews",
            "description": "<p>Number of  total reviews</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "app_rating",
            "description": "<p>App rating</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": {\n      \"registered_users\": 10,\n      \"registered_users_today\": 0,\n      \"completed_transactions\": 16,\n      \"rented_tools\": 6,\n      \"categories\": [\n          {\n              \"id\": 18,\n              \"name\": \"Makedonija\",\n              \"description\": \"dsa\",\n              \"icon\": \"empty\",\n              \"status\": 1,\n              \"sub_categories\": [],\n          },\n          {\n              \"id\": 12,\n              \"name\": \"Main3\",\n              \"description\": \"updaed desc\",\n              \"icon\": \"icon-cat\",\n              \"status\": 1,\n              \"sub_categories\": [],\n          },\n          {\n              \"id\": 11,\n              \"name\": \"Main2\",\n              \"description\": \"updaed desc\",\n              \"icon\": \"icon-cat\",\n              \"status\": 1,\n              \"sub_categories\": [],\n          },\n          {\n              \"id\": 10,\n              \"name\": \"Main1\",\n              \"description\": \"updaed desc\",\n              \"icon\": \"icon-cat\",\n              \"status\": 1,\n              \"sub_categories\": [],\n          }\n      ],\n      \"reviews\": 43242,\n      \"app_rating\": 4\n      }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AdminStatisticsController.php",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/app/feedbacks",
    "title": "Create",
    "description": "<p>Create new App feedback</p>",
    "name": "Create_App_Feedback",
    "group": "AppFeedbacks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n  {\n  \"data\": {\n      \"user\": {\n          \"id\": 6,\n          \"first_name\": \"MrsJaleel\",\n          \"last_name\": \"Schuppe\",\n          \"email\": \"Jaleel4@gmail.com\",\n          \"description\": null,\n          \"username\": \"Jaleel4@gmail.com\",\n          \"date_of_birth\": \"\",\n          \"phone\": \"\",\n          \"gender\": \"\",\n          \"avatar\": \"\",\n          \"show_location\": 1,\n          \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n          \"roles\": [\n              {\n              \"id\": 2,\n              \"name\": \"user\"\n              }\n          ],\n      },\n      \"rating\": \"5\",\n      \"comment\": \"very good\",\n      \"comments\": [],\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"rating\": [\n              \"The rating field is required.\"\n          ],\n          \"comment\": [\n              \"The comment field is required.\"\n          ],\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AppFeedbacksController.php",
    "groupTitle": "AppFeedbacks"
  },
  {
    "type": "post",
    "url": "/app/feedbacks/:id/comments",
    "title": "Create comment",
    "description": "<p>Create new feedback comment</p>",
    "name": "Create_Comment_for_feedback",
    "group": "AppFeedbacks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the feedback.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Feedback Comment ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "feedback",
            "description": "<p>Feedback Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the feedback.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 7,\n          \"feedback\": {\n              \"id\": 2,\n              \"product_id\": 1,\n              \"user_id\": 2,\n              \"rating\": 4,\n              \"comment\": \"test\",\n              \"created_at\": \"2019-11-11 19:23:17\",\n              \"updated_at\": \"2019-11-11 19:23:17\"\n          },\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"MrsJaleel\",\n              \"last_name\": \"Schuppe\",\n              \"email\": \"Jaleel4@gmail.com\",\n              \"description\": null,\n              \"username\": \"Jaleel4@gmail.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n              \"api_token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n              \"created_at\": \"2019-11-18 21:30:59\",\n              \"updated_at\": \"2019-11-18 21:30:59\",\n              \"stripe_id\": \"cus_GChCPAW3wuiCKv\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1\n          },\n          \"comment\": \"Not so good\"\n      }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"comment\": [\n              \"The comment field is required.\"\n          ],\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AppFeedbackCommentsController.php",
    "groupTitle": "AppFeedbacks"
  },
  {
    "type": "get",
    "url": "/app/feedbacks/:id",
    "title": "Show App Feedback",
    "description": "<p>Show feedback by id</p>",
    "name": "ShowAppFeedback",
    "group": "AppFeedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n  \"data\": {\n      \"id\": 2,\n      \"rating\": 5,\n      \"comment\": \"i like this app\",\n      \"comments\": [\n      {\n          \"id\": 1,\n          \"feedback_id\": 2,\n          \"user_id\": 6,\n          \"comment\": \"smthing is wrong\",\n          \"created_at\": \"2019-12-26 16:28:38\",\n          \"updated_at\": \"2019-12-26 16:28:38\",\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Seller\",\n              \"last_name\": \"Byd\",\n              \"email\": \"seller@tst.com\",\n              \"description\": null,\n              \"username\": \"seller@tst.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n              \"api_token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n              \"created_at\": \"2019-12-26 16:16:48\",\n              \"updated_at\": \"2019-12-26 16:16:48\",\n              \"stripe_id\": \"cus_GQqidXmebVoD1Z\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1,\n              \"rating\": {\n                  \"rating\": 0,\n                  \"num_ratings\": 0\n              }\n          }\n      },\n      {\n          \"id\": 2,\n          \"feedback_id\": 2,\n          \"user_id\": 6,\n          \"comment\": \"smthing is wrong\",\n          \"created_at\": \"2019-12-26 16:30:40\",\n          \"updated_at\": \"2019-12-26 16:30:40\",\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Seller\",\n              \"last_name\": \"Byd\",\n              \"email\": \"seller@tst.com\",\n              \"description\": null,\n              \"username\": \"seller@tst.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n              \"api_token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n              \"created_at\": \"2019-12-26 16:16:48\",\n              \"updated_at\": \"2019-12-26 16:16:48\",\n              \"stripe_id\": \"cus_GQqidXmebVoD1Z\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1,\n              \"rating\": {\n                  \"rating\": 0,\n                  \"num_ratings\": 0\n              }\n           }\n       }],\n           \"user\": {\n               \"id\": 6,\n               \"first_name\": \"Seller\",\n               \"last_name\": \"Byd\",\n               \"email\": \"seller@tst.com\",\n               \"description\": null,\n               \"username\": \"seller@tst.com\",\n               \"date_of_birth\": \"\",\n               \"phone\": \"\",\n               \"gender\": \"\",\n               \"avatar\": \"\",\n               \"rating\": {\n               \"rating\": 0,\n               \"num_ratings\": 0\n               },\n               \"show_location\": 1,\n               \"token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n               \"roles\": [\n                   {\n                   \"id\": 2,\n                   \"name\": \"user\"\n                   }\n               ],\n       }\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AppFeedbacksController.php",
    "groupTitle": "AppFeedbacks"
  },
  {
    "type": "get",
    "url": "/app/feedbacks",
    "title": "Show All",
    "description": "<p>Show all App feedbacks</p>",
    "name": "ShowAppFeedbacks",
    "group": "AppFeedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n {\n      \"data\": [\n      {\n          \"id\": 2,\n          \"rating\": 5,\n          \"comment\": \"i like this app\",\n          \"comments\": [\n          {\n              \"id\": 1,\n              \"feedback_id\": 2,\n              \"user_id\": 6,\n              \"comment\": \"smthing is wrong\",\n              \"created_at\": \"2019-12-26 16:28:38\",\n              \"updated_at\": \"2019-12-26 16:28:38\",\n              \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Seller\",\n              \"last_name\": \"Byd\",\n              \"email\": \"seller@tst.com\",\n              \"description\": null,\n              \"username\": \"seller@tst.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n*               \"api_token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n              \"created_at\": \"2019-12-26 16:16:48\",\n              \"updated_at\": \"2019-12-26 16:16:48\",\n              \"stripe_id\": \"cus_GQqidXmebVoD1Z\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1,\n              \"rating\": {\n              \"rating\": 0,\n              \"num_ratings\": 0\n              }\n          }\n      },\n      {\n          \"id\": 2,\n          \"feedback_id\": 2,\n          \"user_id\": 6,\n          \"comment\": \"smthing is wrong\",\n          \"created_at\": \"2019-12-26 16:30:40\",\n          \"updated_at\": \"2019-12-26 16:30:40\",\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Seller\",\n              \"last_name\": \"Byd\",\n              \"email\": \"seller@tst.com\",\n              \"description\": null,\n              \"username\": \"seller@tst.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n              \"api_token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n              \"created_at\": \"2019-12-26 16:16:48\",\n              \"updated_at\": \"2019-12-26 16:16:48\",\n              \"stripe_id\": \"cus_GQqidXmebVoD1Z\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1,\n              \"rating\": {\n                  \"rating\": 0,\n                  \"num_ratings\": 0\n              }\n          }\n      }\n      ],\n      \"user\": {\n      \"id\": 6,\n      \"first_name\": \"Seller\",\n      \"last_name\": \"Byd\",\n      \"email\": \"seller@tst.com\",\n      \"description\": null,\n      \"username\": \"seller@tst.com\",\n      \"date_of_birth\": \"\",\n      \"phone\": \"\",\n      \"gender\": \"\",\n      \"avatar\": \"\",\n      \"rating\": {\n      \"rating\": 0,\n      \"num_ratings\": 0\n      },\n      \"show_location\": 1,\n      \"token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n      \"roles\": [\n      {\n      \"id\": 2,\n      \"name\": \"user\"\n      }\n      ],\n      }\n      },\n      {\n      \"id\": 1,\n      \"rating\": 3,\n      \"comment\": \"very great app\",\n      \"comments\": [],\n      \"user\": {\n      \"id\": 7,\n      \"first_name\": \"Buyer\",\n      \"last_name\": \"Smth\",\n      \"email\": \"buyer@tst.com\",\n      \"description\": null,\n      \"username\": \"buyer@tst.com\",\n      \"date_of_birth\": \"\",\n      \"phone\": \"\",\n      \"gender\": \"\",\n      \"avatar\": \"\",\n      \"rating\": {\n      \"rating\": 0,\n      \"num_ratings\": 0\n      },\n      \"show_location\": 1,\n      \"token\": \"XXxaGCk8MGXinF1OjAgoUqRAM7I5gz5po24ZEbVKK8pKwYEvq9qdSLHOjdhF\",\n      \"roles\": [\n      {\n      \"id\": 2,\n      \"name\": \"user\"\n      }\n      ],\n      }\n      }\n      ],\n      \"links\": {\n      \"first\": \"http://tokyo.test/api/app/feedbacks?page=1\",\n      \"last\": \"http://tokyo.test/api/app/feedbacks?page=1\",\n      \"prev\": null,\n      \"next\": null\n      },\n      \"meta\": {\n      \"current_page\": 1,\n      \"from\": 1,\n      \"last_page\": 1,\n      \"path\": \"http://tokyo.test/api/app/feedbacks\",\n      \"per_page\": 20,\n      \"to\": 2,\n      \"total\": 2\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AppFeedbacksController.php",
    "groupTitle": "AppFeedbacks"
  },
  {
    "type": "get",
    "url": "/app/feedbacks/user",
    "title": "Show All User Feedbacks",
    "description": "<p>Show all User App feedbacks</p>",
    "name": "ShowUserAppFeedbacks",
    "group": "AppFeedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n {\n      \"data\": [\n      {\n          \"id\": 2,\n          \"rating\": 5,\n          \"comment\": \"i like this app\",\n          \"comments\": [\n          {\n              \"id\": 1,\n              \"feedback_id\": 2,\n              \"user_id\": 6,\n              \"comment\": \"smthing is wrong\",\n              \"created_at\": \"2019-12-26 16:28:38\",\n              \"updated_at\": \"2019-12-26 16:28:38\",\n              \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Seller\",\n              \"last_name\": \"Byd\",\n              \"email\": \"seller@tst.com\",\n              \"description\": null,\n              \"username\": \"seller@tst.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n*               \"api_token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n              \"created_at\": \"2019-12-26 16:16:48\",\n              \"updated_at\": \"2019-12-26 16:16:48\",\n              \"stripe_id\": \"cus_GQqidXmebVoD1Z\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1,\n              \"rating\": {\n              \"rating\": 0,\n              \"num_ratings\": 0\n              }\n          }\n      },\n      {\n          \"id\": 2,\n          \"feedback_id\": 2,\n          \"user_id\": 6,\n          \"comment\": \"smthing is wrong\",\n          \"created_at\": \"2019-12-26 16:30:40\",\n          \"updated_at\": \"2019-12-26 16:30:40\",\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Seller\",\n              \"last_name\": \"Byd\",\n              \"email\": \"seller@tst.com\",\n              \"description\": null,\n              \"username\": \"seller@tst.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n              \"api_token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n              \"created_at\": \"2019-12-26 16:16:48\",\n              \"updated_at\": \"2019-12-26 16:16:48\",\n              \"stripe_id\": \"cus_GQqidXmebVoD1Z\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1,\n              \"rating\": {\n                  \"rating\": 0,\n                  \"num_ratings\": 0\n              }\n          }\n      }\n      ],\n      \"user\": {\n      \"id\": 6,\n      \"first_name\": \"Seller\",\n      \"last_name\": \"Byd\",\n      \"email\": \"seller@tst.com\",\n      \"description\": null,\n      \"username\": \"seller@tst.com\",\n      \"date_of_birth\": \"\",\n      \"phone\": \"\",\n      \"gender\": \"\",\n      \"avatar\": \"\",\n      \"rating\": {\n      \"rating\": 0,\n      \"num_ratings\": 0\n      },\n      \"show_location\": 1,\n      \"token\": \"oTqoftGGsKUXDHWWhbSD1Ny5zq6xk9bFQzUEmWe55NzKS0enUeHVXQnMutDR\",\n      \"roles\": [\n      {\n      \"id\": 2,\n      \"name\": \"user\"\n      }\n      ],\n      }\n      },\n      {\n      \"id\": 1,\n      \"rating\": 3,\n      \"comment\": \"very great app\",\n      \"comments\": [],\n      \"user\": {\n      \"id\": 7,\n      \"first_name\": \"Buyer\",\n      \"last_name\": \"Smth\",\n      \"email\": \"buyer@tst.com\",\n      \"description\": null,\n      \"username\": \"buyer@tst.com\",\n      \"date_of_birth\": \"\",\n      \"phone\": \"\",\n      \"gender\": \"\",\n      \"avatar\": \"\",\n      \"rating\": {\n      \"rating\": 0,\n      \"num_ratings\": 0\n      },\n      \"show_location\": 1,\n      \"token\": \"XXxaGCk8MGXinF1OjAgoUqRAM7I5gz5po24ZEbVKK8pKwYEvq9qdSLHOjdhF\",\n      \"roles\": [\n      {\n      \"id\": 2,\n      \"name\": \"user\"\n      }\n      ],\n      }\n      }\n      ],\n      \"links\": {\n      \"first\": \"http://tokyo.test/api/app/feedbacks?page=1\",\n      \"last\": \"http://tokyo.test/api/app/feedbacks?page=1\",\n      \"prev\": null,\n      \"next\": null\n      },\n      \"meta\": {\n      \"current_page\": 1,\n      \"from\": 1,\n      \"last_page\": 1,\n      \"path\": \"http://tokyo.test/api/app/feedbacks\",\n      \"per_page\": 20,\n      \"to\": 2,\n      \"total\": 2\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AppFeedbacksController.php",
    "groupTitle": "AppFeedbacks"
  },
  {
    "type": "post",
    "url": "/categories",
    "title": "Create",
    "description": "<p>Create new category</p>",
    "name": "CreateCategory",
    "group": "Category",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Category Icon.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "parent_id",
            "description": "<p>Parent category ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Icon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n        \"id\": 9,\n        \"name\": \"Saw\",\n        \"description\": \"Saw machines\",\n        \"icon\": \"icon-cat\",\n        \"status\": 1\n    },\n    \"message\": \"Resource was succesfuly updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n    \"errors\": {\n        \"name\": [\n            \"The name field is required.\"\n        ],\n        \"description\": [\n            \"The description field is required.\"\n        ],\n        \"status\": [\n            \"The status field is required.\"\n        ],\n        \"icon\": [\n            \"The icon field is required.\"\n        ],\n        \"parent_id\": [\n            \"The parent id field is required.\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CategoriesController.php",
    "groupTitle": "Category"
  },
  {
    "type": "delete",
    "url": "/categories/:id",
    "title": "Delete",
    "description": "<p>Delete category</p>",
    "name": "DeleteCategory",
    "group": "Category",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [],\n    \"message\": \"Resource was succesfuly deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CategoriesController.php",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/categories?name=Main&order_by=id&sort=desc",
    "title": "Show",
    "description": "<p>Show category by ID</p>",
    "name": "Show_Category",
    "group": "Category",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Icon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": {\n      \"id\": 10,\n      \"name\": \"Main1\",\n      \"description\": \"updaed desc\",\n      \"icon\": \"icon-cat\",\n      \"status\": 1,\n      \"sub_categories\": [\n          {\n              \"id\": 13,\n              \"name\": \"Submain11\",\n              \"description\": \"updaed desc\",\n              \"icon\": \"icon-cat\",\n              \"status\": 1,\n              \"sub_categories\": [\n                  {\n                      \"id\": 17,\n                      \"name\": \"ThirdLevel\",\n                      \"description\": \"updaed desc\",\n                      \"icon\": \"icon-cat\",\n                      \"status\": 1,\n                      \"sub_categories\": []\n                  }\n              ]\n          },\n          {\n              \"id\": 14,\n              \"name\": \"Submain12\",\n              \"description\": \"updaed desc\",\n              \"icon\": \"icon-cat\",\n              \"status\": 1,\n              \"sub_categories\": []\n          }\n      ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CategoriesController.php",
    "groupTitle": "Category"
  },
  {
    "type": "put",
    "url": "/categories/:id",
    "title": "Update",
    "description": "<p>Update category</p>",
    "name": "UpdateCategory",
    "group": "Category",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Category Icon.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "1",
              "0"
            ],
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Icon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n        \"id\": 9,\n        \"name\": \"Tools\",\n        \"description\": \"new desc\",\n        \"icon\": \"icon-cat\",\n        \"status\": 1\n    },\n    \"message\": \"Resource was succesfuly updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n    \"errors\": {\n        \"name\": [\n            \"The name field is required.\"\n        ],\n        \"description\": [\n            \"The description field is required.\"\n        ],\n        \"status\": [\n            \"The status field is required.\"\n        ],\n        \"icon\": [\n            \"The icon field is required.\"\n        ],\n        \"parent_id\": [\n            \"The parent id field is required.\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CategoriesController.php",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "/chat/rooms/admin",
    "title": "Create Admin Room",
    "description": "<p>Create chat with the admin</p>",
    "name": "CreateAdminChatRoom",
    "group": "Chat",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Icon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            \"id\": 1,\n            \"identifier\": \"1a46910f3d6e04d7cd45c7caff38c63b\",\n            \"user_id\": 11,\n            \"message_id\": 1,\n            \"title\": \"Rentout chat room\",\n            \"slug\": \"4_11\",\n            \"created_by\": 11,\n            \"created_at\": \"2019-11-22 17:32:28\",\n            \"updated_at\": \"2019-11-22 17:32:28\"\n        }\n    ],\n    \"message\": \"Resource was succesfuly created\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n    \"errors\": {\n        \"message\": [\n            \"The name field is required.\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/RoomsController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/chat/rooms/bulk",
    "title": "Create bulk rooms",
    "description": "<p>Create multiple cchat rooms in one request</p>",
    "name": "CreateBulkChatRoom",
    "group": "Chat",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "users",
            "description": "<p>Array with user ids to start chat with.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Icon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n           \"status\":\"succes\"\n        }\n    ],\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/RoomsController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/chat/rooms",
    "title": "Create Room",
    "description": "<p>Create chat room</p>",
    "name": "CreateChatRoom",
    "group": "Chat",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>User to chat with.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Icon.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Active status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            \"id\": 1,\n            \"identifier\": \"1a46910f3d6e04d7cd45c7caff38c63b\",\n            \"user_id\": 11,\n            \"message_id\": 1,\n            \"title\": \"Rentout chat room\",\n            \"slug\": \"4_11\",\n            \"created_by\": 11,\n            \"created_at\": \"2019-11-22 17:32:28\",\n            \"updated_at\": \"2019-11-22 17:32:28\"\n        }\n    ],\n    \"message\": \"Resource was succesfuly created\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n    \"errors\": {\n        \"user_id\": [\n            \"The name field is required.\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/RoomsController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "delete",
    "url": "/chat/rooms/:id",
    "title": "Delete Room",
    "description": "<p>Delete chat room</p>",
    "name": "DeleteChatRoom",
    "group": "Chat",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [],\n    \"message\": \"Resource was succesfuly deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/RoomsController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "post",
    "url": "/chat/messages",
    "title": "Send Message",
    "description": "<p>Send message in room</p>",
    "name": "SendMessage",
    "group": "Chat",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "room_id",
            "description": "<p>Room where message will belong.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Message ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The message content</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "room",
            "description": "<p>Room where message belongs.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User who sent the message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n        \"room_id\": \"2\",\n        \"message\": \"Lorem Ipsum\",\n        \"user_id\": 11,\n        \"updated_at\": \"2019-11-22 17:45:56\",\n        \"created_at\": \"2019-11-22 17:45:56\",\n        \"id\": 6,\n        \"room\": {\n            \"id\": 2,\n            \"identifier\": \"613d3d5df83eba8845a8211a48fdd3dd\",\n            \"user_id\": 11,\n            \"message_id\": 6,\n            \"title\": \"Rentout chat room\",\n            \"slug\": \"8_11\",\n            \"created_by\": 11,\n            \"created_at\": \"2019-11-22 17:35:59\",\n            \"updated_at\": \"2019-11-22 17:45:56\"\n        },\n        \"user\": {\n            \"id\": 11,\n            \"first_name\": \"John\",\n            \"last_name\": \"Quigley\",\n            \"email\": \"john@seller.com\",\n            \"description\": null,\n            \"username\": \"john.seller\",\n            \"avatar\": \"\",\n            \"email_verified_at\": null,\n            \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n            \"created_at\": \"2019-11-17 15:02:18\",\n            \"updated_at\": \"2019-11-17 15:02:18\",\n            \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n            \"card_brand\": null,\n            \"card_last_four\": null,\n            \"trial_ends_at\": null,\n            \"gender\": \"\",\n            \"date_of_birth\": \"\",\n            \"phone\": \"\",\n            \"show_location\": 1\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n  \"errors\": {\n        \"room_id\": [\n            \"The room id field is required.\"\n        ],\n        \"message\": [\n            \"The message field is required.\"\n        ],\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/MessagesController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/chat/rooms",
    "title": "All rooms",
    "description": "<p>Show all rooms for the user</p>",
    "name": "ShowAllRoom",
    "group": "Chat",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>room ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_user",
            "description": "<p>Last user who send message</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_message",
            "description": "<p>Lsat message in the room.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "messages",
            "description": "<p>List of messages for this room.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n \"data\": [\n      {\n          \"id\": 1,\n          \"last_user\": \"John Quigley\",\n          \"last_message\": \"Hello\",\n          \"messages\": [\n          {\n              \"id\": 1,\n              \"user_id\": 11,\n              \"room_id\": 1,\n              \"message\": \"Hello\",\n              \"created_at\": \"2019-11-22 17:32:28\",\n              \"updated_at\": \"2019-11-22 17:32:28\"\n          }\n          ],\n      },\n      {\n          \"id\": 2,\n          \"last_user\": \"John Quigley\",\n          \"last_message\": \"Hello\",\n          \"messages\": [\n          {\n              \"id\": 2,\n              \"user_id\": 11,\n              \"room_id\": 2,\n              \"message\": \"Hello\",\n              \"created_at\": \"2019-11-22 17:35:59\",\n              \"updated_at\": \"2019-11-22 17:35:59\"\n          }\n          ],\n      }\n  ],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/RoomsController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/chat/rooms/{id}",
    "title": "Show Room",
    "description": "<p>Show room by id</p>",
    "name": "ShowRoom",
    "group": "Chat",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>room ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_user",
            "description": "<p>Last user who send message</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_message",
            "description": "<p>Lsat message in the room.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "messages",
            "description": "<p>List of messages for this room.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": {\n      \"id\": 2,\n      \"last_user\": \"John Quigley\",\n      \"last_message\": \"Hello\",\n      \"messages\": [\n          {\n              \"id\": 2,\n              \"user_id\": 11,\n              \"room_id\": 2,\n              \"message\": \"Hello\",\n              \"created_at\": \"2019-11-22 17:35:59\",\n              \"updated_at\": \"2019-11-22 17:35:59\"\n          }\n      ],\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/Chat/RoomsController.php",
    "groupTitle": "Chat"
  },
  {
    "type": "get",
    "url": "/countries?name=Germany&order_by=name&sort=desc",
    "title": "List",
    "description": "<p>List all available countries</p>",
    "name": "ListCountries",
    "group": "Countries",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n   \"data\": [\n      {\n          \"id\": 1,\n          \"name\": \"Belgium\",\n          \"code\": \"BE\"\n      },\n      {\n          \"id\": 2,\n          \"name\": \"France\",\n          \"code\": \"FR\"\n      },\n      {\n          \"id\": 3,\n          \"name\": \"Germany\",\n          \"code\": \"DE\"\n      }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CountriesController.php",
    "groupTitle": "Countries"
  },
  {
    "type": "post",
    "url": "/payment-methods/cards",
    "title": "Create",
    "description": "<p>Create new credit card</p>",
    "name": "CreateCreditCards",
    "group": "CreditCards",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>stripe token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Credit Card ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Stripe Credit Card token.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Credit Card type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "brand",
            "description": "<p>Credit Card brand.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "last4",
            "description": "<p>Credit Card last 4 numbers.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "exp_year",
            "description": "<p>Credit Card expiration year.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "exp_month",
            "description": "<p>Credit Card expiration month.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"data\": {\n      \"id\": 1,\n      \"token\": \"card_GChJ1mwFwZnN76\",\n      \"type\": \"Visa\",\n       \"brand\": \"Visa\",\n       \"last4\": \"4242\",\n       \"exp_year\": 2020,\n       \"exp_month\": 11\n   }\n    \"message\": \"Resource was succesfuly updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n    \"errors\": {\n       \"token\": [\n           \"The token field is required.\"\n       ],\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/PaymentMethods/CreditCardsController.php",
    "groupTitle": "CreditCards"
  },
  {
    "type": "delete",
    "url": "/payment-methods/cards/:id",
    "title": "Delete",
    "description": "<p>Delete credit card</p>",
    "name": "Delete_Credit_Card",
    "group": "CreditCards",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n        \"message\": \"Resource deleted\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CreditCardNotFound",
            "description": "<p>The id of the Card was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/PaymentMethods/CreditCardsController.php",
    "groupTitle": "CreditCards"
  },
  {
    "type": "get",
    "url": "/payment-methods/cards",
    "title": "Show",
    "description": "<p>Show credit cards</p>",
    "name": "Show_CreditCards",
    "group": "CreditCards",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Credit Card ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Stripe Credit Card token.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Credit Card type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "brand",
            "description": "<p>Credit Card brand.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "last4",
            "description": "<p>Credit Card last 4 numbers.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "exp_year",
            "description": "<p>Credit Card expiration year.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "exp_month",
            "description": "<p>Credit Card expiration month.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n     \"data\": [\n            {\n             \"id\": 1,\n              \"token\": \"card_GChJ1mwFwZnN76\",\n             \"type\": \"Visa\",\n             \"brand\": \"Visa\",\n             \"last4\": \"4242\",\n             \"exp_year\": \"2020\",\n             \"exp_month\": \"11\"\n            }\n         ],\n         \"links\": {\n            \"first\": \"http://rentout.test/api/payment-methods/cards?page=1\",\n            \"last\": \"http://rentout.test/api/payment-methods/cards?page=1\",\n            \"prev\": null,\n            \"next\": null\n         },\n         \"meta\": {\n            \"current_page\": 1,\n            \"from\": 1,\n            \"last_page\": 1,\n            \"path\": \"http://rentout.test/api/payment-methods/cards\",\n            \"per_page\": \"10\",\n            \"to\": 1,\n            \"total\": 1\n        }   \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Credit",
            "description": "<p>Cards was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/PaymentMethods/CreditCardsController.php",
    "groupTitle": "CreditCards"
  },
  {
    "type": "post",
    "url": "/favorites",
    "title": "Create",
    "description": "<p>Add product to favorites</p>",
    "name": "Create",
    "group": "Favorites",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Favorite ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product info</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.title",
            "description": "<p>Product title</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.category",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n          \"data\": {\n          \"id\": 11,\n          \"product\": {\n             \"id\": 6,\n             \"title\": \"Burmashine\",\n             \"description\": \"za budalene komsiite\",\n             \"price_per_day\": 654,\n             \"deposit\": 500,\n             \"available_from\": \"2019-08-21\",\n             \"available_to\": \"2019-08-23\",\n             \"hour_from\": \"10:00\",\n             \"hour_to\": \"16:00\",\n             \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Tijana\",\n              \"last_name\": \"Bogicevis\",\n              \"email\": \"user2@rentout.com\"\n              }\n          }\n      }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n    \"errors\": {\n        \"product_id\": [\n            \"The selected product id is invalid.\"\n        ],\n        \n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FavoritesController.php",
    "groupTitle": "Favorites"
  },
  {
    "type": "delete",
    "url": "/favorites/:product_id",
    "title": "Delete",
    "description": "<p>Remove product from favorites</p>",
    "name": "Delete",
    "group": "Favorites",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n        \"message\": \"Resource deleted\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FavoriteNotFound",
            "description": "<p>The id of the Favorite was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FavoritesController.php",
    "groupTitle": "Favorites"
  },
  {
    "type": "get",
    "url": "/favorites",
    "title": "Show",
    "description": "<p>Show user's favorite prducts</p>",
    "name": "Show",
    "group": "Favorites",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Favorite ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product info</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.title",
            "description": "<p>Product title</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.category",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n\"data\": [\n              {\n              \"id\": 11,\n              \"product\": {\n              \"id\": 6,\n              \"title\": \"Burmashine\",\n              \"description\": \"za budalene komsiite\",\n              \"price_per_day\": 654,\n              \"deposit\": 500,\n              \"available_from\": \"2019-08-21\",\n              \"available_to\": \"2019-08-23\",\n              \"hour_from\": \"10:00\",\n              \"hour_to\": \"16:00\",\n              \"images\": [],\n              \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n              },\n              \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Tijana\",\n              \"last_name\": \"Bogicevis\",\n              \"email\": \"user2@rentout.com\"\n              }\n              }\n              },\n              {\n              \"id\": 10,\n              \"product\": {\n              \"id\": 5,\n              \"title\": \"Burmashine\",\n              \"description\": \"za budalene komsiite\",\n              \"price_per_day\": 3200,\n              \"deposit\": 500,\n              \"available_from\": \"2019-08-21\",\n              \"available_to\": \"2019-08-23\",\n              \"hour_from\": \"10:00\",\n              \"hour_to\": \"16:00\",\n              \"images\": [],\n              \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n              },\n              \"user\": {\n              \"id\": 6,\n              \"first_name\": \"Tijana\",\n              \"last_name\": \"Bogicevis\",\n              \"email\": \"user2@rentout.com\"\n              }\n              }\n              }\n              ],\n              \"links\": {\n              \"first\": \"http://tokyo.test/api/favorites?page=1\",\n              \"last\": \"http://tokyo.test/api/favorites?page=1\",\n              \"prev\": null,\n              \"next\": null\n              },\n              \"meta\": {\n              \"current_page\": 1,\n              \"from\": 1,\n              \"last_page\": 1,\n              \"path\": \"http://tokyo.test/api/favorites\",\n              \"per_page\": \"100\",\n              \"to\": 2,\n              \"total\": 2\n              }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FavoritesController.php",
    "groupTitle": "Favorites"
  },
  {
    "type": "post",
    "url": "/feedbacks/:id/comments",
    "title": "Create comment",
    "description": "<p>Create new feedback comment</p>",
    "name": "Create_Comment_for_feedback",
    "group": "Feedbacks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the feedback.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Feedback Comment ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "feedback",
            "description": "<p>Feedback Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the feedback.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 7,\n          \"feedback\": {\n              \"id\": 2,\n              \"product_id\": 1,\n              \"user_id\": 2,\n              \"rating\": 4,\n              \"comment\": \"test\",\n              \"created_at\": \"2019-11-11 19:23:17\",\n              \"updated_at\": \"2019-11-11 19:23:17\"\n          },\n          \"user\": {\n              \"id\": 6,\n              \"first_name\": \"MrsJaleel\",\n              \"last_name\": \"Schuppe\",\n              \"email\": \"Jaleel4@gmail.com\",\n              \"description\": null,\n              \"username\": \"Jaleel4@gmail.com\",\n              \"avatar\": \"\",\n              \"email_verified_at\": null,\n              \"api_token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n              \"created_at\": \"2019-11-18 21:30:59\",\n              \"updated_at\": \"2019-11-18 21:30:59\",\n              \"stripe_id\": \"cus_GChCPAW3wuiCKv\",\n              \"card_brand\": null,\n              \"card_last_four\": null,\n              \"trial_ends_at\": null,\n              \"gender\": \"\",\n              \"date_of_birth\": \"\",\n              \"phone\": \"\",\n              \"show_location\": 1\n          },\n          \"comment\": \"Not so good\"\n      }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"comment\": [\n              \"The comment field is required.\"\n          ],\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbackCommentsController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "post",
    "url": "/feedbacks",
    "title": "Create",
    "description": "<p>Create new feedback</p>",
    "name": "Create_Feedback",
    "group": "Feedbacks",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n  {\n  \"data\": {\n      \"user\": {\n          \"id\": 6,\n          \"first_name\": \"MrsJaleel\",\n          \"last_name\": \"Schuppe\",\n          \"email\": \"Jaleel4@gmail.com\",\n          \"description\": null,\n          \"username\": \"Jaleel4@gmail.com\",\n          \"date_of_birth\": \"\",\n          \"phone\": \"\",\n          \"gender\": \"\",\n          \"avatar\": \"\",\n          \"show_location\": 1,\n          \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n          \"roles\": [\n              {\n              \"id\": 2,\n              \"name\": \"user\"\n              }\n          ],\n      },\n      \"product\": {\n          \"id\": 2,\n          \"title\": \"Product 2\",\n          \"description\": \"this is my sec product\",\n          \"price_per_day\": 25000,\n          \"deposit\": 9850,\n          \"available_from\": \"2019-11-01\",\n          \"available_to\": \"2019-11-07\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"is_favorite\": false,\n          \"distance\": 5985.64,\n          \"rating\": 5,\n          \"images\": [\n              {\n              \"id\": 1,\n              \"product_id\": 2,\n              \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n              \"created_at\": \"2019-11-08 20:21:01\",\n              \"updated_at\": \"2019-11-08 20:21:01\"\n              },\n              {\n              \"id\": 2,\n              \"product_id\": 2,\n              \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n              \"created_at\": \"2019-11-08 20:21:01\",\n              \"updated_at\": \"2019-11-08 20:21:01\"\n              }\n          ],\n          \"category\": {\n              \"id\": 1,\n              \"name\": \"Main Category\"\n          },\n          \"user\": {\n              \"id\": 2,\n              \"first_name\": \"Marija\",\n              \"last_name\": \"Kovacevic\",\n              \"email\": \"user@example.com\"\n          }\n      },\n      \"rating\": \"5\",\n      \"comment\": \"very good\",\n      \"comments\": [],\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"product_id\": [\n              \"The product id field is required.\"\n          ],\n          \"rating\": [\n              \"The rating field is required.\"\n          ],\n          \"comment\": [\n              \"The comment field is required.\"\n          ],\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbacksController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "get",
    "url": "/feedbacks",
    "title": "Show All",
    "description": "<p>Show all feedbacks</p>",
    "name": "Show",
    "group": "Feedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"data\": [\n    {\n    \"rating\": 5,\n    \"comment\": \"very good\",\n    \"comments\": [],\n    \"user\": {\n        \"id\": 6,\n        \"first_name\": \"MrsJaleel\",\n        \"last_name\": \"Schuppe\",\n        \"email\": \"Jaleel4@gmail.com\",\n        \"description\": null,\n        \"username\": \"Jaleel4@gmail.com\",\n        \"date_of_birth\": \"\",\n        \"phone\": \"\",\n        \"gender\": \"\",\n        \"avatar\": \"\",\n        \"show_location\": 1,\n        \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n        \"roles\": [\n            {\n            \"id\": 2,\n            \"name\": \"user\"\n            }\n        ],\n    },\n    \"product\": {\n        \"id\": 2,\n        \"title\": \"Product 2\",\n        \"description\": \"this is my sec product\",\n        \"price_per_day\": 25000,\n        \"deposit\": 9850,\n        \"available_from\": \"2019-11-01\",\n        \"available_to\": \"2019-11-07\",\n        \"hour_from\": \"10:00\",\n        \"hour_to\": \"16:00\",\n        \"is_favorite\": false,\n        \"distance\": 5985.64,\n        \"rating\": 5,\n        \"images\": [\n            {\n                \"id\": 1,\n                \"product_id\": 2,\n                \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            },\n            {\n                \"id\": 2,\n                \"product_id\": 2,\n                \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            }\n        ],\n        \"category\": {\n            \"id\": 1,\n            \"name\": \"Main Category\"\n        },\n        \"user\": {\n            \"id\": 2,\n            \"first_name\": \"Marija1\",\n            \"last_name\": \"Kovacevic\",\n            \"email\": \"user@example.com\"\n        }\n        }\n    }\n    ],\n    \"links\": {\n        \"first\": \"http://rentout.test/api/feedbacks?page=1\",\n        \"last\": \"http://rentout.test/api/feedbacks?page=1\",\n        \"prev\": null,\n        \"next\": null\n    },\n    \"meta\": {\n        \"current_page\": 1,\n        \"from\": 1,\n        \"last_page\": 1,\n        \"path\": \"http://rentout.test/api/feedbacks\",\n        \"per_page\": 15,\n        \"to\": 1,\n        \"total\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbacksController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "get",
    "url": "/feedbacks/:id",
    "title": "Show Feedback",
    "description": "<p>Show feedback by id</p>",
    "name": "ShowFeedback",
    "group": "Feedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"data\": \n    {\n    \"rating\": 5,\n    \"comment\": \"very good\",\n    \"comments\": [],\n    \"user\": {\n        \"id\": 6,\n        \"first_name\": \"MrsJaleel\",\n        \"last_name\": \"Schuppe\",\n        \"email\": \"Jaleel4@gmail.com\",\n        \"description\": null,\n        \"username\": \"Jaleel4@gmail.com\",\n        \"date_of_birth\": \"\",\n        \"phone\": \"\",\n        \"gender\": \"\",\n        \"avatar\": \"\",\n        \"show_location\": 1,\n        \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n        \"roles\": [\n            {\n            \"id\": 2,\n            \"name\": \"user\"\n            }\n        ],\n    },\n    \"product\": {\n        \"id\": 2,\n        \"title\": \"Product 2\",\n        \"description\": \"this is my sec product\",\n        \"price_per_day\": 25000,\n        \"deposit\": 9850,\n        \"available_from\": \"2019-11-01\",\n        \"available_to\": \"2019-11-07\",\n        \"hour_from\": \"10:00\",\n        \"hour_to\": \"16:00\",\n        \"is_favorite\": false,\n        \"distance\": 5985.64,\n        \"rating\": 5,\n        \"images\": [\n            {\n                \"id\": 1,\n                \"product_id\": 2,\n                \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            },\n            {\n                \"id\": 2,\n                \"product_id\": 2,\n                \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            }\n        ],\n        \"category\": {\n            \"id\": 1,\n            \"name\": \"Main Category\"\n        },\n        \"user\": {\n            \"id\": 2,\n            \"first_name\": \"Marija1\",\n            \"last_name\": \"Kovacevic\",\n            \"email\": \"user@example.com\"\n        }\n        }\n    }\n    \n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbacksController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "get",
    "url": "/feedbacks/owner",
    "title": "Owner feedbacks",
    "description": "<p>Show all feedbacks created for the owner</p>",
    "name": "ShowOwnerFeedbacks",
    "group": "Feedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"data\": [\n    {\n    \"rating\": 5,\n    \"comment\": \"very good\",\n    \"comments\": [],\n    \"user\": {\n        \"id\": 6,\n        \"first_name\": \"MrsJaleel\",\n        \"last_name\": \"Schuppe\",\n        \"email\": \"Jaleel4@gmail.com\",\n        \"description\": null,\n        \"username\": \"Jaleel4@gmail.com\",\n        \"date_of_birth\": \"\",\n        \"phone\": \"\",\n        \"gender\": \"\",\n        \"avatar\": \"\",\n        \"show_location\": 1,\n        \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n        \"roles\": [\n            {\n            \"id\": 2,\n            \"name\": \"user\"\n            }\n        ],\n    },\n    \"product\": {\n        \"id\": 2,\n        \"title\": \"Product 2\",\n        \"description\": \"this is my sec product\",\n        \"price_per_day\": 25000,\n        \"deposit\": 9850,\n        \"available_from\": \"2019-11-01\",\n        \"available_to\": \"2019-11-07\",\n        \"hour_from\": \"10:00\",\n        \"hour_to\": \"16:00\",\n        \"is_favorite\": false,\n        \"distance\": 5985.64,\n        \"rating\": 5,\n        \"images\": [\n            {\n                \"id\": 1,\n                \"product_id\": 2,\n                \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            },\n            {\n                \"id\": 2,\n                \"product_id\": 2,\n                \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            }\n        ],\n        \"category\": {\n            \"id\": 1,\n            \"name\": \"Main Category\"\n        },\n        \"user\": {\n            \"id\": 2,\n            \"first_name\": \"Marija1\",\n            \"last_name\": \"Kovacevic\",\n            \"email\": \"user@example.com\"\n        }\n        }\n    }\n    ],\n    \"links\": {\n        \"first\": \"http://rentout.test/api/feedbacks?page=1\",\n        \"last\": \"http://rentout.test/api/feedbacks?page=1\",\n        \"prev\": null,\n        \"next\": null\n    },\n    \"meta\": {\n        \"current_page\": 1,\n        \"from\": 1,\n        \"last_page\": 1,\n        \"path\": \"http://rentout.test/api/feedbacks\",\n        \"per_page\": 15,\n        \"to\": 1,\n        \"total\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbacksController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "get",
    "url": "/feedbacks/tenant",
    "title": "Tenant feedbacks",
    "description": "<p>Show all feedbacks created from the tenant</p>",
    "name": "ShowTenantFeedbacks",
    "group": "Feedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"data\": [\n    {\n    \"rating\": 5,\n    \"comment\": \"very good\",\n    \"comments\": [],\n    \"user\": {\n        \"id\": 6,\n        \"first_name\": \"MrsJaleel\",\n        \"last_name\": \"Schuppe\",\n        \"email\": \"Jaleel4@gmail.com\",\n        \"description\": null,\n        \"username\": \"Jaleel4@gmail.com\",\n        \"date_of_birth\": \"\",\n        \"phone\": \"\",\n        \"gender\": \"\",\n        \"avatar\": \"\",\n        \"show_location\": 1,\n        \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n        \"roles\": [\n            {\n            \"id\": 2,\n            \"name\": \"user\"\n            }\n        ],\n    },\n    \"product\": {\n        \"id\": 2,\n        \"title\": \"Product 2\",\n        \"description\": \"this is my sec product\",\n        \"price_per_day\": 25000,\n        \"deposit\": 9850,\n        \"available_from\": \"2019-11-01\",\n        \"available_to\": \"2019-11-07\",\n        \"hour_from\": \"10:00\",\n        \"hour_to\": \"16:00\",\n        \"is_favorite\": false,\n        \"distance\": 5985.64,\n        \"rating\": 5,\n        \"images\": [\n            {\n                \"id\": 1,\n                \"product_id\": 2,\n                \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            },\n            {\n                \"id\": 2,\n                \"product_id\": 2,\n                \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            }\n        ],\n        \"category\": {\n            \"id\": 1,\n            \"name\": \"Main Category\"\n        },\n        \"user\": {\n            \"id\": 2,\n            \"first_name\": \"Marija1\",\n            \"last_name\": \"Kovacevic\",\n            \"email\": \"user@example.com\"\n        }\n        }\n    }\n    ],\n    \"links\": {\n        \"first\": \"http://rentout.test/api/feedbacks?page=1\",\n        \"last\": \"http://rentout.test/api/feedbacks?page=1\",\n        \"prev\": null,\n        \"next\": null\n    },\n    \"meta\": {\n        \"current_page\": 1,\n        \"from\": 1,\n        \"last_page\": 1,\n        \"path\": \"http://rentout.test/api/feedbacks\",\n        \"per_page\": 15,\n        \"to\": 1,\n        \"total\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbacksController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "get",
    "url": "/feedbacks/:id/comments",
    "title": "Show All Comments",
    "description": "<p>Show all feedbacks comments</p>",
    "name": "Show_Comments",
    "group": "Feedbacks",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Feedback Comment ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "feedback",
            "description": "<p>Feedback Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the feedback.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            \"id\": 7,\n            \"feedback\": {\n                \"id\": 2,\n                \"product_id\": 1,\n                \"user_id\": 2,\n                \"rating\": 4,\n                \"comment\": \"test\",\n                \"created_at\": \"2019-11-11 19:23:17\",\n                \"updated_at\": \"2019-11-11 19:23:17\"\n            },\n            \"user\": {\n                \"id\": 6,\n                \"first_name\": \"MrsJaleel\",\n                \"last_name\": \"Schuppe\",\n                \"email\": \"Jaleel4@gmail.com\",\n                \"description\": null,\n                \"username\": \"Jaleel4@gmail.com\",\n                \"avatar\": \"\",\n                \"email_verified_at\": null,\n                \"api_token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n                \"created_at\": \"2019-11-18 21:30:59\",\n                \"updated_at\": \"2019-11-18 21:30:59\",\n                \"stripe_id\": \"cus_GChCPAW3wuiCKv\",\n                \"card_brand\": null,\n                \"card_last_four\": null,\n                \"trial_ends_at\": null,\n                \"gender\": \"\",\n                \"date_of_birth\": \"\",\n                \"phone\": \"\",\n                \"show_location\": 1\n            },\n            \"comment\": \"Not so good\"\n        },\n        {\n            \"id\": 8,\n            \"feedback\": {\n                \"id\": 2,\n                \"product_id\": 1,\n                \"user_id\": 2,\n                \"rating\": 4,\n                \"comment\": \"test\",\n                \"created_at\": \"2019-11-11 19:23:17\",\n                \"updated_at\": \"2019-11-11 19:23:17\"\n            },\n            \"user\": {\n                \"id\": 6,\n                \"first_name\": \"MrsJaleel\",\n                \"last_name\": \"Schuppe\",\n                \"email\": \"Jaleel4@gmail.com\",\n                \"description\": null,\n                \"username\": \"Jaleel4@gmail.com\",\n                \"avatar\": \"\",\n                \"email_verified_at\": null,\n                \"api_token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n                \"created_at\": \"2019-11-18 21:30:59\",\n                \"updated_at\": \"2019-11-18 21:30:59\",\n                \"stripe_id\": \"cus_GChCPAW3wuiCKv\",\n                \"card_brand\": null,\n                \"card_last_four\": null,\n                \"trial_ends_at\": null,\n                \"gender\": \"\",\n                \"date_of_birth\": \"\",\n                \"phone\": \"\",\n                \"show_location\": 1\n            },\n            \"comment\": \"Very good\"\n        }\n    ],\n    \"links\": {\n        \"first\": \"http://rentout.test/api/feedbacks/2/comments?page=1\",\n        \"last\": \"http://rentout.test/api/feedbacks/2/comments?page=1\",\n        \"prev\": null,\n        \"next\": null\n    },\n    \"meta\": {\n        \"current_page\": 1,\n        \"from\": 1,\n        \"last_page\": 1,\n        \"path\": \"http://rentout.test/api/feedbacks/2/comments\",\n        \"per_page\": 15,\n        \"to\": 2,\n        \"total\": 2\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/FeedbackCommentsController.php",
    "groupTitle": "Feedbacks"
  },
  {
    "type": "get",
    "url": "/search?term=:string",
    "title": "",
    "description": "<p>Search help texts</p>",
    "name": "Search_Help",
    "group": "Help",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "term",
            "description": "<p>Term to search.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>HTML text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Order by.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": [\n           {\n              \"title\": \"Haley\",\n              \"content\": \"<html><head><title>Voluptates et error et.</title></head><body><form action=\\\"example.com\\\" method=\\\"POST\\\"><label for=\\\"username\\\">ut</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">eius</label><input type=\\\"password\\\" id=\\\"password\\\"></form>Optio delectus ea porro quia.<p>Est non non animi deserunt blanditiis enim sapiente impedit numquam est quo ipsum.</p></body></html>\\n\",\n              \"order\": 3\n          }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/HelpController.php",
    "groupTitle": "Help"
  },
  {
    "type": "get",
    "url": "/help",
    "title": "Show",
    "description": "<p>Show help menu texts</p>",
    "name": "Show_Help",
    "group": "Help",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>HTML text</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>Order by.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n      \"data\": [\n          {\n          {\n              \"title\": \"Haley\",\n          {\n              \"content\": \"<html><head><title>Voluptates et error et.</title></head><body><form action=\\\"example.com\\\" method=\\\"POST\\\"><label for=\\\"username\\\">ut</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">eius</label><input type=\\\"password\\\" id=\\\"password\\\"></form>Optio delectus ea porro quia.<p>Est non non animi deserunt blanditiis enim sapiente impedit numquam est quo ipsum.</p></body></html>\\n\",\n              \"order\": 3\n          },\n          {\n              \"title\": \"Okon\",\n              \"content\": \"<html><head><title>Et voluptatem est.</title></head><body><form action=\\\"example.net\\\" method=\\\"POST\\\"><label for=\\\"username\\\">optio</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">impedit</label><input type=\\\"password\\\" id=\\\"password\\\"></form><i>Quod cumque perferendis qui in ut.</i><h3>Officia rerum officiis neque ut blanditiis quod ut.</h3>Molestiae magnam deleniti ab officiis tenetur unde dignissimos.</body></html>\\n\",\n              \"order\": 4\n          },\n          {\n              \"title\": \"Johnston\",\n              \"content\": \"<html><head><title>Quia et.</title></head><body><form action=\\\"example.net\\\" method=\\\"POST\\\"><label for=\\\"username\\\">reiciendis</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">voluptas</label><input type=\\\"password\\\" id=\\\"password\\\"></form><i>Voluptatum aut odio explicabo modi sed eius nisi qui sapiente sed.</i><h3>Exercitationem quaerat repellendus culpa eius dolorem quo et iure corporis.</h3></body></html>\\n\",\n              \"order\": 4\n          },\n          {\n              \"title\": \"Nader\",\n              \"content\": \"<html><head><title>Et.</title></head><body><form action=\\\"example.net\\\" method=\\\"POST\\\"><label for=\\\"username\\\">placeat</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">tempora</label><input type=\\\"password\\\" id=\\\"password\\\"></form><ul><li>Non.</li><li>Repellat et architecto.</li><li>Nihil repudiandae.</li><li>Dolor rem inventore magnam.</li><li>Odio omnis.</li><li>Est.</li><li>Quibusdam eos quasi.</li><li>Aperiam voluptas dignissimos consequuntur.</li></ul></body></html>\\n\",\n              \"order\": 6\n          },\n          {\n              \"title\": \"Marks\",\n              \"content\": \"<html><head><title>Nihil officiis.</title></head><body><form action=\\\"example.net\\\" method=\\\"POST\\\"><label for=\\\"username\\\">sit</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">quis</label><input type=\\\"password\\\" id=\\\"password\\\"></form><h1>Doloremque sint dignissimos accusantium.</h1>Modi facilis consequatur ipsam et aliquid at qui sint qui voluptatem harum nam.Explicabo qui magni et enim dolor deserunt porro.</body></html>\\n\",\n              \"order\": 6\n          },\n          {\n              \"title\": \"Hand\",\n              \"content\": \"<html><head><title>Sunt necessitatibus exercitationem tempore neque voluptatem.</title></head><body><form action=\\\"example.net\\\" method=\\\"POST\\\"><label for=\\\"username\\\">rerum</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">commodi</label><input type=\\\"password\\\" id=\\\"password\\\"></form><h3>Adipisci vel nisi vitae explicabo.</h3><p>Neque corporis qui ducimus excepturi reiciendis sint odio.</p>Ut rerum quis maiores.</body></html>\\n\",\n              \"order\": 6\n          },\n          {\n              \"title\": \"Upton\",\n              \"content\": \"<html><head><title>Maxime dignissimos illum inventore rerum cupiditate ea provident sint.</title></head><body><form action=\\\"example.org\\\" method=\\\"POST\\\"><label for=\\\"username\\\">inventore</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">praesentium</label><input type=\\\"password\\\" id=\\\"password\\\"></form>Rerum autem atque similique aut consequatur delectus quaerat officiis.Provident ex harum voluptas aut nihil velit eum.</body></html>\\n\",\n              \"order\": 7\n          },\n          {\n              \"title\": \"Johnston\",\n              \"content\": \"<html><head><title>Explicabo.</title></head><body><form action=\\\"example.net\\\" method=\\\"POST\\\"><label for=\\\"username\\\">reprehenderit</label><input type=\\\"text\\\" id=\\\"username\\\"><label for=\\\"password\\\">voluptatibus</label><input type=\\\"password\\\" id=\\\"password\\\"></form><span>Reiciendis corporis assumenda illum corrupti beatae qui dignissimos qui illo sunt.</span><h2>Tempora ea iure blanditiis voluptate non rerum.</h2></body></html>\\n\",\n              \"order\": 8\n          }\n      ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/HelpController.php",
    "groupTitle": "Help"
  },
  {
    "type": "post",
    "url": "/orders",
    "title": "Create",
    "description": "<p>Create new Order</p>",
    "name": "Create_Order",
    "group": "Orders",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Date from format YYYY-MM-DD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "to_date",
            "description": "<p>Date to format YYYY-MM-DD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "payment_token",
            "description": "<p>Stripe Payment token.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "creditcard",
              "bancontact"
            ],
            "optional": false,
            "field": "payment_type",
            "description": "<p>Payment type.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.user_id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.title",
            "description": "<p>Product title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.category_id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.is_rented",
            "description": "<p>Product is rented.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.avg_rating",
            "description": "<p>Product average rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.lat",
            "description": "<p>Product Latitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.lng",
            "description": "<p>Product Lognitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.price_per_day",
            "description": "<p>Product price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.deposit",
            "description": "<p>Product deposit in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.available_from",
            "description": "<p>Product Date available from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.available_to",
            "description": "<p>Product Date available to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.hour_from",
            "description": "<p>Product Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.hour_to",
            "description": "<p>Product Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Order Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_to",
            "description": "<p>Order Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number_of_days",
            "description": "<p>Order rent days.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Product price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Product deposit in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "avg_rating",
            "description": "<p>Product average rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "fee",
            "description": "<p>Product fee in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": "<p>Order total price in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User who created the order.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": {\n        \"id\": 4,\n        \"amount\": 1550000,\n        \"deposit\": 9850,\n        \"status\": \"paid\",\n        \"from_date\": \"2019-03-04\",\n        \"to_date\": \"2019-05-05\",\n        \"user\": {\n            \"id\": 6,\n            \"first_name\": \"MrsJaleel\",\n            \"last_name\": \"Schuppe\",\n            \"email\": \"Jaleel4@gmail.com\"\n        },\n        \"product\": {\n            \"id\": 2,\n            \"title\": \"Product 2\",\n            \"description\": \"this is my sec product\",\n            \"rating\": null,\n            \"images\": [\n                {\n                    \"id\": 1,\n                    \"product_id\": 2,\n                    \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                    \"created_at\": \"2019-11-08 20:21:01\",\n                    \"updated_at\": \"2019-11-08 20:21:01\"\n                },\n                {\n                    \"id\": 2,\n                    \"product_id\": 2,\n                    \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                    \"created_at\": \"2019-11-08 20:21:01\",\n                    \"updated_at\": \"2019-11-08 20:21:01\"\n                }\n            ],\n            \"archived\": null,\n            \"user\": {\n                \"id\": 2,\n                \"first_name\": \"Hristijan\",\n                \"last_name\": \"Kovacevic\",\n                \"email\": \"user@example.com\"\n            }\n        }\n    },\n    \"message\": \"Resource was succesfuly created\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n\n    \"errors\": {\n        \"product_id\": [\n            \"The product id field is required.\"\n        ],\n        \"from_date\": [\n            \"The from date field is required.\"\n        ],\n        \"to_date\": [\n            \"The to date field is required.\"\n        ],\n        \"payment_token\": [\n            \"The payment token field is required.\"\n        ],\n        \"payment_type\": [\n            \"The payment type field is required.\"\n        ],\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrdersController.php",
    "groupTitle": "Orders"
  },
  {
    "type": "post",
    "url": "/orders/preview",
    "title": "Preview Order",
    "description": "<p>Preview new Order</p>",
    "name": "Preview_Order",
    "group": "Orders",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Date from format YYYY-MM-DD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "to_date",
            "description": "<p>Date to format YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.user_id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.title",
            "description": "<p>Product title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.category_id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.is_rented",
            "description": "<p>Product is rented.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.avg_rating",
            "description": "<p>Product average rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.lat",
            "description": "<p>Product Latitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.lng",
            "description": "<p>Product Lognitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.price_per_day",
            "description": "<p>Product price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.deposit",
            "description": "<p>Product deposit in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.available_from",
            "description": "<p>Product Date available from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.available_to",
            "description": "<p>Product Date available to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.hour_from",
            "description": "<p>Product Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.hour_to",
            "description": "<p>Product Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Order Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_to",
            "description": "<p>Order Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number_of_days",
            "description": "<p>Order rent days.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Product price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Product deposit in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "avg_rating",
            "description": "<p>Product average rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "fee",
            "description": "<p>Product fee in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": "<p>Order total price in cents.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"data\": {\n        \"product\": {\n            \"id\": 2,\n            \"user_id\": 2,\n            \"title\": \"Product 2\",\n            \"description\": \"this is my sec product\",\n            \"category_id\": 1,\n            \"is_rented\": 1,\n            \"avg_rating\": null,\n            \"lat\": -32.1476,\n            \"lng\": 50.1178,\n            \"price_per_day\": 25000,\n            \"deposit\": 9850,\n            \"available_from\": \"2019-11-01\",\n            \"available_to\": \"2019-11-07\",\n            \"hour_from\": \"10:00\",\n            \"hour_to\": \"16:00\",\n            \"created_at\": \"2019-11-08 20:21:01\",\n            \"updated_at\": \"2019-11-14 22:21:20\",\n            \"deleted_at\": null\n       },\n        \"from_date\": \"2019-04-04\",\n        \"to_date\": \"2019-04-11\",\n        \"number_of_days\": 7,\n        \"price_per_day\": 25000,\n        \"deposit\": 9850,\n        \"rating\": null,\n        \"fee\": 0,\n        \"total\": 184850\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n       \"errors\": {\n            \"product_id\": [\n                \"The product id field is required.\"\n            ],\n            \"from_date\": [\n                \"The from date field is required.\"\n            ],\n            \"to_date\": [\n                \"The to date field is required.\"\n            ],\n        }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrdersController.php",
    "groupTitle": "Orders"
  },
  {
    "type": "get",
    "url": "/orders/owner",
    "title": "Owner Orders",
    "description": "<p>Show owner orders</p>",
    "name": "ShowOwnerOrders",
    "group": "Orders",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "upcoming",
            "description": "<p>List with upcoming orders.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "upcoming.id",
            "description": "<p>Order id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "upcoming.from_date",
            "description": "<p>Order from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "upcoming.to_date",
            "description": "<p>Order to date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "upcoming.product",
            "description": "<p>Order product object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "ongoing",
            "description": "<p>List with ongoing orders.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ongoing.id",
            "description": "<p>Order id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ongoing.from_date",
            "description": "<p>Order from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ongoing.to_date",
            "description": "<p>Order to date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "ongoing.product",
            "description": "<p>Order product object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "finished",
            "description": "<p>List with finished orders.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "finished.id",
            "description": "<p>Order id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "finished.from_date",
            "description": "<p>Order from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "finished.to_date",
            "description": "<p>Order to date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "finished.product",
            "description": "<p>Order product object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n\n      \"data\": {\n          \"upcoming\": [\n              {\n                  \"id\": 25,\n                  \"product_id\": 33,\n                  \"user_id\": 12,\n                  \"charge_id\": 13,\n                  \"from_date\": \"2019-12-10\",\n                  \"to_date\": \"2019-12-12\",\n                  \"amount\": 3200,\n                  \"deposit\": 4000,\n                  \"fee\": 0,\n                  \"status\": \"paid\",\n                  \"charged\": 0,\n                  \"created_at\": \"2019-11-17 16:09:25\",\n                  \"updated_at\": \"2019-11-17 16:09:27\",\n                  \"product\": {\n                      \"id\": 33,\n                      \"user_id\": 11,\n                      \"title\": \"Toothbrush for Teeth\",\n                      \"description\": \"Voluptas quam odio provident iste qui sed fuga optio. Ea est adipisci dignissimos eius nihil. Eos dolor recusandae aperiam saepe. Voluptas voluptates debitis aut fuga doloremque tenetur quo laborum error. Officiis sapiente at distinctio itaque voluptatem. Aut qui quae mollitia facilis et mollitia. Dolor doloremque vero voluptas aspernatur eum corporis ipsam vel. Qui eos enim dolor magni voluptas accusantium in quibusdam. Qui et vitae voluptatum tenetur est qui non. Dolores sit quia dolores voluptatem. Quia voluptas qui quis dolorum dolorem ad ut ea ut. Enim mollitia est accusantium velit.\",\n                      \"category_id\": 10,\n                      \"is_rented\": 1,\n                      \"avg_rating\": null,\n                      \"price_per_day\": 1600,\n                      \"deposit\": 4000,\n                      \"lng\": 23.6715,\n                      \"lat\": -46.6732,\n                      \"available_from\": \"2019-12-10\",\n                      \"available_to\": \"2019-12-15\",\n                      \"hour_from\": \"10:00\",\n                      \"hour_to\": \"16:00\",\n                      \"created_at\": \"2019-11-17 15:27:45\",\n                      \"updated_at\": \"2019-11-17 16:09:27\",\n                      \"deleted_at\": null,\n                      \"images\": [],\n                      \"user\": {\n                          \"id\": 11,\n                          \"first_name\": \"John\",\n                          \"last_name\": \"Quigley\",\n                          \"email\": \"john@seller.com\",\n                          \"description\": null,\n                          \"username\": \"john.seller\",\n                          \"avatar\": \"\",\n                          \"email_verified_at\": null,\n                          \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n                          \"created_at\": \"2019-11-17 15:02:18\",\n                          \"updated_at\": \"2019-11-17 15:02:18\",\n                          \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n                          \"card_brand\": null,\n                          \"card_last_four\": null,\n                          \"trial_ends_at\": null,\n                          \"gender\": \"\",\n                          \"date_of_birth\": \"\",\n                          \"phone\": \"\",\n                          \"show_location\": 1\n                      }\n                  }\n              }\n          ],\n          \"ongoing\": [\n              {\n                  \"id\": 24,\n                  \"product_id\": 34,\n                  \"user_id\": 12,\n                  \"charge_id\": 12,\n                  \"from_date\": \"2019-11-07\",\n                  \"to_date\": \"2019-11-22\",\n                  \"amount\": 3200,\n                  \"deposit\": 4000,\n                  \"fee\": 0,\n                  \"status\": \"paid\",\n                  \"charged\": 0,\n                  \"created_at\": \"2019-11-17 15:40:58\",\n                  \"updated_at\": \"2019-11-17 15:40:59\",\n                  \"product\": {\n                      \"id\": 34,\n                      \"user_id\": 11,\n                      \"title\": \"Wrench 156\",\n                      \"description\": \"Voluptas quam odio provident iste qui sed fuga optio. Ea est adipisci dignissimos eius nihil. Eos dolor recusandae aperiam saepe. Voluptas voluptates debitis aut fuga doloremque tenetur quo laborum error. Officiis sapiente at distinctio itaque voluptatem. Aut qui quae mollitia facilis et mollitia. Dolor doloremque vero voluptas aspernatur eum corporis ipsam vel. Qui eos enim dolor magni voluptas accusantium in quibusdam. Qui et vitae voluptatum tenetur est qui non. Dolores sit quia dolores voluptatem. Quia voluptas qui quis dolorum dolorem ad ut ea ut. Enim mollitia est accusantium velit.\",\n                      \"category_id\": 10,\n                      \"is_rented\": 1,\n                      \"avg_rating\": null,\n                      \"price_per_day\": 1600,\n                      \"deposit\": 4000,\n                      \"lng\": 23.6715,\n                      \"lat\": -46.6732,\n                      \"available_from\": \"2019-12-10\",\n                      \"available_to\": \"2019-12-15\",\n                      \"hour_from\": \"10:00\",\n                      \"hour_to\": \"16:00\",\n                      \"created_at\": \"2019-11-17 15:28:03\",\n                      \"updated_at\": \"2019-11-17 15:57:15\",\n                      \"deleted_at\": \"2019-11-17 15:57:15\",\n                      \"images\": [],\n                      \"user\": {\n                          \"id\": 11,\n                          \"first_name\": \"John\",\n                          \"last_name\": \"Quigley\",\n                          \"email\": \"john@seller.com\",\n                          \"description\": null,\n                          \"username\": \"john.seller\",\n                          \"avatar\": \"\",\n                          \"email_verified_at\": null,\n                          \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n                          \"created_at\": \"2019-11-17 15:02:18\",\n                          \"updated_at\": \"2019-11-17 15:02:18\",\n                          \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n                          \"card_brand\": null,\n                          \"card_last_four\": null,\n                          \"trial_ends_at\": null,\n                          \"gender\": \"\",\n                          \"date_of_birth\": \"\",\n                          \"phone\": \"\",\n                          \"show_location\": 1\n                      }\n                  }\n              }\n          ],\n          \"finished\": [\n              {\n                  \"id\": 23,\n                  \"product_id\": 34,\n                  \"user_id\": 12,\n                  \"charge_id\": null,\n                  \"from_date\": \"2019-10-10\",\n                  \"to_date\": \"2019-10-12\",\n                  \"amount\": 3200,\n                  \"deposit\": 4000,\n                  \"fee\": 0,\n                  \"status\": \"in_progress\",\n                  \"charged\": 0,\n                  \"created_at\": \"2019-11-17 15:38:54\",\n                  \"updated_at\": \"2019-11-17 15:38:54\",\n                  \"product\": {\n                      \"id\": 34,\n                      \"user_id\": 11,\n                      \"title\": \"Wrench 156\",\n                      \"description\": \"Voluptas quam odio provident iste qui sed fuga optio. Ea est adipisci dignissimos eius nihil. Eos dolor recusandae aperiam saepe. Voluptas voluptates debitis aut fuga doloremque tenetur quo laborum error. Officiis sapiente at distinctio itaque voluptatem. Aut qui quae mollitia facilis et mollitia. Dolor doloremque vero voluptas aspernatur eum corporis ipsam vel. Qui eos enim dolor magni voluptas accusantium in quibusdam. Qui et vitae voluptatum tenetur est qui non. Dolores sit quia dolores voluptatem. Quia voluptas qui quis dolorum dolorem ad ut ea ut. Enim mollitia est accusantium velit.\",\n                      \"category_id\": 10,\n                      \"is_rented\": 1,\n                      \"avg_rating\": null,\n                      \"price_per_day\": 1600,\n                      \"deposit\": 4000,\n                      \"lng\": 23.6715,\n                      \"lat\": -46.6732,\n                      \"available_from\": \"2019-12-10\",\n                      \"available_to\": \"2019-12-15\",\n                      \"hour_from\": \"10:00\",\n                      \"hour_to\": \"16:00\",\n                      \"created_at\": \"2019-11-17 15:28:03\",\n                      \"updated_at\": \"2019-11-17 15:57:15\",\n                      \"deleted_at\": \"2019-11-17 15:57:15\",\n                      \"images\": [],\n                      \"user\": {\n                          \"id\": 11,\n                          \"first_name\": \"John\",\n                          \"last_name\": \"Quigley\",\n                          \"email\": \"john@seller.com\",\n                          \"description\": null,\n                          \"username\": \"john.seller\",\n                          \"avatar\": \"\",\n                          \"email_verified_at\": null,\n                          \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n                          \"created_at\": \"2019-11-17 15:02:18\",\n                          \"updated_at\": \"2019-11-17 15:02:18\",\n                          \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n                          \"card_brand\": null,\n                          \"card_last_four\": null,\n                          \"trial_ends_at\": null,\n                          \"gender\": \"\",\n                          \"date_of_birth\": \"\",\n                          \"phone\": \"\",\n                          \"show_location\": 1\n                      }\n                  }\n              }\n          ],\n      }   \n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrdersSellerController.php",
    "groupTitle": "Orders"
  },
  {
    "type": "get",
    "url": "/orders/tenant",
    "title": "Tenant Orders",
    "description": "<p>Show tenant orders</p>",
    "name": "ShowTenantOrders",
    "group": "Orders",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "upcoming",
            "description": "<p>List with upcoming orders.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "upcoming.id",
            "description": "<p>Order id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "upcoming.from_date",
            "description": "<p>Order from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "upcoming.to_date",
            "description": "<p>Order to date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "upcoming.product",
            "description": "<p>Order product object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "ongoing",
            "description": "<p>List with ongoing orders.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ongoing.id",
            "description": "<p>Order id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ongoing.from_date",
            "description": "<p>Order from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ongoing.to_date",
            "description": "<p>Order to date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "ongoing.product",
            "description": "<p>Order product object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "finished",
            "description": "<p>List with finished orders.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "finished.id",
            "description": "<p>Order id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "finished.from_date",
            "description": "<p>Order from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "finished.to_date",
            "description": "<p>Order to date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "finished.product",
            "description": "<p>Order product object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n\n      \"data\": {\n          \"upcoming\": [\n              {\n                  \"id\": 25,\n                  \"product_id\": 33,\n                  \"user_id\": 12,\n                  \"charge_id\": 13,\n                  \"from_date\": \"2019-12-10\",\n                  \"to_date\": \"2019-12-12\",\n                  \"amount\": 3200,\n                  \"deposit\": 4000,\n                  \"fee\": 0,\n                  \"status\": \"paid\",\n                  \"charged\": 0,\n                  \"created_at\": \"2019-11-17 16:09:25\",\n                  \"updated_at\": \"2019-11-17 16:09:27\",\n                  \"product\": {\n                      \"id\": 33,\n                      \"user_id\": 11,\n                      \"title\": \"Toothbrush for Teeth\",\n                      \"description\": \"Voluptas quam odio provident iste qui sed fuga optio. Ea est adipisci dignissimos eius nihil. Eos dolor recusandae aperiam saepe. Voluptas voluptates debitis aut fuga doloremque tenetur quo laborum error. Officiis sapiente at distinctio itaque voluptatem. Aut qui quae mollitia facilis et mollitia. Dolor doloremque vero voluptas aspernatur eum corporis ipsam vel. Qui eos enim dolor magni voluptas accusantium in quibusdam. Qui et vitae voluptatum tenetur est qui non. Dolores sit quia dolores voluptatem. Quia voluptas qui quis dolorum dolorem ad ut ea ut. Enim mollitia est accusantium velit.\",\n                      \"category_id\": 10,\n                      \"is_rented\": 1,\n                      \"avg_rating\": null,\n                      \"price_per_day\": 1600,\n                      \"deposit\": 4000,\n                      \"lng\": 23.6715,\n                      \"lat\": -46.6732,\n                      \"available_from\": \"2019-12-10\",\n                      \"available_to\": \"2019-12-15\",\n                      \"hour_from\": \"10:00\",\n                      \"hour_to\": \"16:00\",\n                      \"created_at\": \"2019-11-17 15:27:45\",\n                      \"updated_at\": \"2019-11-17 16:09:27\",\n                      \"deleted_at\": null,\n                      \"images\": [],\n                      \"user\": {\n                          \"id\": 11,\n                          \"first_name\": \"John\",\n                          \"last_name\": \"Quigley\",\n                          \"email\": \"john@seller.com\",\n                          \"description\": null,\n                          \"username\": \"john.seller\",\n                          \"avatar\": \"\",\n                          \"email_verified_at\": null,\n                          \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n                          \"created_at\": \"2019-11-17 15:02:18\",\n                          \"updated_at\": \"2019-11-17 15:02:18\",\n                          \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n                          \"card_brand\": null,\n                          \"card_last_four\": null,\n                          \"trial_ends_at\": null,\n                          \"gender\": \"\",\n                          \"date_of_birth\": \"\",\n                          \"phone\": \"\",\n                          \"show_location\": 1\n                      }\n                  }\n              }\n          ],\n          \"ongoing\": [\n              {\n                  \"id\": 24,\n                  \"product_id\": 34,\n                  \"user_id\": 12,\n                  \"charge_id\": 12,\n                  \"from_date\": \"2019-11-07\",\n                  \"to_date\": \"2019-11-22\",\n                  \"amount\": 3200,\n                  \"deposit\": 4000,\n                  \"fee\": 0,\n                  \"status\": \"paid\",\n                  \"charged\": 0,\n                  \"created_at\": \"2019-11-17 15:40:58\",\n                  \"updated_at\": \"2019-11-17 15:40:59\",\n                  \"product\": {\n                      \"id\": 34,\n                      \"user_id\": 11,\n                      \"title\": \"Wrench 156\",\n                      \"description\": \"Voluptas quam odio provident iste qui sed fuga optio. Ea est adipisci dignissimos eius nihil. Eos dolor recusandae aperiam saepe. Voluptas voluptates debitis aut fuga doloremque tenetur quo laborum error. Officiis sapiente at distinctio itaque voluptatem. Aut qui quae mollitia facilis et mollitia. Dolor doloremque vero voluptas aspernatur eum corporis ipsam vel. Qui eos enim dolor magni voluptas accusantium in quibusdam. Qui et vitae voluptatum tenetur est qui non. Dolores sit quia dolores voluptatem. Quia voluptas qui quis dolorum dolorem ad ut ea ut. Enim mollitia est accusantium velit.\",\n                      \"category_id\": 10,\n                      \"is_rented\": 1,\n                      \"avg_rating\": null,\n                      \"price_per_day\": 1600,\n                      \"deposit\": 4000,\n                      \"lng\": 23.6715,\n                      \"lat\": -46.6732,\n                      \"available_from\": \"2019-12-10\",\n                      \"available_to\": \"2019-12-15\",\n                      \"hour_from\": \"10:00\",\n                      \"hour_to\": \"16:00\",\n                      \"created_at\": \"2019-11-17 15:28:03\",\n                      \"updated_at\": \"2019-11-17 15:57:15\",\n                      \"deleted_at\": \"2019-11-17 15:57:15\",\n                      \"images\": [],\n                      \"user\": {\n                          \"id\": 11,\n                          \"first_name\": \"John\",\n                          \"last_name\": \"Quigley\",\n                          \"email\": \"john@seller.com\",\n                          \"description\": null,\n                          \"username\": \"john.seller\",\n                          \"avatar\": \"\",\n                          \"email_verified_at\": null,\n                          \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n                          \"created_at\": \"2019-11-17 15:02:18\",\n                          \"updated_at\": \"2019-11-17 15:02:18\",\n                          \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n                          \"card_brand\": null,\n                          \"card_last_four\": null,\n                          \"trial_ends_at\": null,\n                          \"gender\": \"\",\n                          \"date_of_birth\": \"\",\n                          \"phone\": \"\",\n                          \"show_location\": 1\n                      }\n                  }\n              }\n          ],\n          \"finished\": [\n              {\n                  \"id\": 23,\n                  \"product_id\": 34,\n                  \"user_id\": 12,\n                  \"charge_id\": null,\n                  \"from_date\": \"2019-10-10\",\n                  \"to_date\": \"2019-10-12\",\n                  \"amount\": 3200,\n                  \"deposit\": 4000,\n                  \"fee\": 0,\n                  \"status\": \"in_progress\",\n                  \"charged\": 0,\n                  \"created_at\": \"2019-11-17 15:38:54\",\n                  \"updated_at\": \"2019-11-17 15:38:54\",\n                  \"product\": {\n                      \"id\": 34,\n                      \"user_id\": 11,\n                      \"title\": \"Wrench 156\",\n                      \"description\": \"Voluptas quam odio provident iste qui sed fuga optio. Ea est adipisci dignissimos eius nihil. Eos dolor recusandae aperiam saepe. Voluptas voluptates debitis aut fuga doloremque tenetur quo laborum error. Officiis sapiente at distinctio itaque voluptatem. Aut qui quae mollitia facilis et mollitia. Dolor doloremque vero voluptas aspernatur eum corporis ipsam vel. Qui eos enim dolor magni voluptas accusantium in quibusdam. Qui et vitae voluptatum tenetur est qui non. Dolores sit quia dolores voluptatem. Quia voluptas qui quis dolorum dolorem ad ut ea ut. Enim mollitia est accusantium velit.\",\n                      \"category_id\": 10,\n                      \"is_rented\": 1,\n                      \"avg_rating\": null,\n                      \"price_per_day\": 1600,\n                      \"deposit\": 4000,\n                      \"lng\": 23.6715,\n                      \"lat\": -46.6732,\n                      \"available_from\": \"2019-12-10\",\n                      \"available_to\": \"2019-12-15\",\n                      \"hour_from\": \"10:00\",\n                      \"hour_to\": \"16:00\",\n                      \"created_at\": \"2019-11-17 15:28:03\",\n                      \"updated_at\": \"2019-11-17 15:57:15\",\n                      \"deleted_at\": \"2019-11-17 15:57:15\",\n                      \"images\": [],\n                      \"user\": {\n                          \"id\": 11,\n                          \"first_name\": \"John\",\n                          \"last_name\": \"Quigley\",\n                          \"email\": \"john@seller.com\",\n                          \"description\": null,\n                          \"username\": \"john.seller\",\n                          \"avatar\": \"\",\n                          \"email_verified_at\": null,\n                          \"api_token\": \"hrZVVVo0GwGYMbCLLcQFdJiUmzQqxcnXlZ7yFyeq2niVjmJRjyCwqDBRSmKp\",\n                          \"created_at\": \"2019-11-17 15:02:18\",\n                          \"updated_at\": \"2019-11-17 15:02:18\",\n                          \"stripe_id\": \"cus_GCDiLOVgByfFQK\",\n                          \"card_brand\": null,\n                          \"card_last_four\": null,\n                          \"trial_ends_at\": null,\n                          \"gender\": \"\",\n                          \"date_of_birth\": \"\",\n                          \"phone\": \"\",\n                          \"show_location\": 1\n                      }\n                  }\n              }\n          ],\n      }   \n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrdersBuyerController.php",
    "groupTitle": "Orders"
  },
  {
    "type": "get",
    "url": "/orders",
    "title": "Show All",
    "description": "<p>Show all order for current user</p>",
    "name": "Show_All_Orders",
    "group": "Orders",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Order ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Order amount in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Order deposit in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Order status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Order Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_to",
            "description": "<p>Order Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n    {\n        \"id\": 4,\n        \"amount\": 1550000,\n        \"deposit\": 9850,\n        \"status\": \"paid\",\n        \"from_date\": \"2019-03-04\",\n        \"to_date\": \"2019-05-05\",\n        \"user\": {\n            \"id\": 6,\n            \"first_name\": \"MrsJaleel\",\n            \"last_name\": \"Schuppe\",\n            \"email\": \"Jaleel4@gmail.com\"\n        },\n        \"product\": {\n            \"id\": 2,\n            \"title\": \"Product 2\",\n            \"description\": \"this is my sec product\",\n            \"rating\": null,\n            \"images\": [\n                {\n                \"id\": 1,\n                \"product_id\": 2,\n                \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n                },\n                {\n                \"id\": 2,\n                \"product_id\": 2,\n                \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n                }\n            ],\n            \"archived\": null,\n            \"user\": {\n                \"id\": 2,\n                \"first_name\": \"Marija1\",\n                \"last_name\": \"Kovacevic\",\n                \"email\": \"user@example.com\"\n            }\n        }\n    }\n    ],\n        \"links\": {\n            \"first\": \"http://rentout.test/api/orders?page=1\",\n            \"last\": \"http://rentout.test/api/orders?page=1\",\n            \"prev\": null,\n            \"next\": null\n        },\n        \"meta\": {\n            \"current_page\": 1,\n            \"from\": 1,\n            \"last_page\": 1,\n           \"path\": \"http://rentout.test/api/orders\",\n            \"per_page\": \"10\",\n            \"to\": 1,\n            \"total\": 1\n       }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrdersController.php",
    "groupTitle": "Orders"
  },
  {
    "type": "get",
    "url": "/orders/:id",
    "title": "Show Order",
    "description": "<p>Show order by id for current user</p>",
    "name": "Show_Order_by_ID",
    "group": "Orders",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Order ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Order amount in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Order deposit in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Order status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_date",
            "description": "<p>Order Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from_to",
            "description": "<p>Order Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": \n        {\n            \"id\": 4,\n            \"amount\": 1550000,\n            \"deposit\": 9850,\n            \"status\": \"paid\",\n            \"from_date\": \"2019-03-04\",\n            \"to_date\": \"2019-05-05\",\n            \"user\": {\n                \"id\": 6,\n                \"first_name\": \"MrsJaleel\",\n                \"last_name\": \"Schuppe\",\n                \"email\": \"Jaleel4@gmail.com\"\n            },\n            \"product\": {\n                \"id\": 2,\n                \"title\": \"Product 2\",\n                \"description\": \"this is my sec product\",\n                \"rating\": null,\n                \"images\": [\n                    {\n                    \"id\": 1,\n                    \"product_id\": 2,\n                    \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                    \"created_at\": \"2019-11-08 20:21:01\",\n                    \"updated_at\": \"2019-11-08 20:21:01\"\n                    },\n                    {\n                    \"id\": 2,\n                    \"product_id\": 2,\n                    \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                    \"created_at\": \"2019-11-08 20:21:01\",\n                    \"updated_at\": \"2019-11-08 20:21:01\"\n                    }\n                ],\n                \"archived\": null,\n                \"user\": {\n                    \"id\": 2,\n                    \"first_name\": \"Marija1\",\n                    \"last_name\": \"Kovacevic\",\n                    \"email\": \"user@example.com\"\n                }\n            }\n        }\n    \n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrdersController.php",
    "groupTitle": "Orders"
  },
  {
    "type": "post",
    "url": "/products/:id/images",
    "title": "Add image",
    "description": "<p>Add image</p>",
    "name": "Add_image",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>Image in base64 format.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "image",
            "description": "<p>The image object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "image.id",
            "description": "<p>Image id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "image.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image.image",
            "description": "<p>Image path.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n\"data\": {\n     \"image\": {\n         \"product_id\": \"3\",\n         \"image\": \"/images/de8f721ba2e11710dc12fbf8c65092d2.jpeg\",\n         \"updated_at\": \"2019-10-17 17:22:17\",\n         \"created_at\": \"2019-10-17 17:22:17\",\n         \"id\": 8\n         }\n     }\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n        \"errors\": {\n            \"image\": [\n                \"The image field is required.\"\n            ]                   \n       }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductImagesController.php",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products/:id/busy",
    "title": "Busy Dates",
    "description": "<p>Get busy dates for given product</p>",
    "name": "Busy_dates",
    "group": "Product",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>data with dates.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "from",
            "description": "<p>from date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "to",
            "description": "<p>to date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [\n        {\n            \"from\": \"2019-12-10\",\n            \"to\": \"2019-12-12\"\n        },\n        {\n            \"from\": \"2019-11-24\",\n            \"to\": \"2019-11-26\"\n        }\n    ],\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/products",
    "title": "Create",
    "description": "<p>Create new product</p>",
    "name": "Create",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "images.0",
            "description": "<p>Base64 encoded image</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "images.1",
            "description": "<p>Base64 encoded image</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"data\": {\n    \"id\": 15,\n    \"title\": \"Black & Decker 500W Triming Machine\",\n    \"description\": \"For outdoor use only this machine is very lound and strong. Be careful!\",\n    \"price_per_day\": \"12000\",\n    \"deposit\": \"2000\",\n    \"available_from\": \"2019-10-10\",\n    \"available_to\": \"2019-10-15\",\n    \"hour_from\": \"10:00\",\n    \"hour_to\": \"16:00\",\n    \"is_favorite\": false,\n    \"distance\": 152.88,\n    \"images\": [\n        {\n            \"id\": 5,\n            \"product_id\": 15,\n            \"image\": \"/images/5de4b85eb314c69e25f54d5d40091397.png\",\n            \"created_at\": \"2019-10-11 14:54:03\",\n            \"updated_at\": \"2019-10-11 14:54:03\"\n        }\n    ],\n    \"category\": {\n        \"id\": 10,\n        \"name\": \"Main1\"\n    },\n    \"user\": {\n        \"id\": 1,\n        \"first_name\": \"John\",\n        \"last_name\": \"Snow\",\n        \"email\": \"johnsnow@winterfell.com\",\n        \"username\": \"john.snow\",\n        \"show_location\": 0,\n        \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n        \"roles\": []\n    }\n},\n\"message\": \"Resource was succesfuly created\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n        \"errors\": {\n            \"title\": [\n                \"The title field is required.\"\n            ],\n            \"description\": [\n                \"The description field is required.\"\n            ],\n            \"category_id\": [\n                \"The category id field is required.\"\n            ],\n            \"price_per_day\": [\n                \"The price per day field is required.\"\n            ],\n            \"deposit\": [\n                \"The deposit field is required.\"\n            ],\n            \"available_from\": [\n                \"The available from field is required.\"\n            ],\n            \"available_to\": [\n                \"The available to field is required.\"\n            ],\n            \"hour_from\": [\n                \"The hour from field is required.\"\n            ],\n            \"hour_to\": [\n                \"The hour to field is required.\"\n            ],\n            \"images\": [\n                \"The images field is required.\"\n            ]\n       }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "delete",
    "url": "/products/:product_id/images/:id",
    "title": "Delete image",
    "description": "<p>Delete image  by product id and image id</p>",
    "name": "Delete_Image",
    "group": "Product",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [],\n    \"message\": \"Resource was succesfuly deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProductNotFound",
            "description": "<p>The id of the Product was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductImagesController.php",
    "groupTitle": "Product"
  },
  {
    "type": "delete",
    "url": "/products/:id",
    "title": "Delete product",
    "description": "<p>Delete product by id</p>",
    "name": "Delete_product",
    "group": "Product",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"data\": [],\n    \"message\": \"Resource was succesfuly deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProductNotFound",
            "description": "<p>The id of the Product was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products/last",
    "title": "Last Products",
    "description": "<p>Get last products without login</p>",
    "name": "LastProducts",
    "group": "Product",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>Is product added in user's favorite products.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "distance",
            "description": "<p>Product distance from user's location.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": [\n      {\n          \"id\": 14,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"is_favorite\": false,\n          \"distance\": 152.88,\n          \"images\": [\n              {\n                  \"id\": 1,\n                  \"product_id\": 14,\n                  \"image\": \"/images/9a61f579504ea5bc589546ecb3dadff8.jpeg\",\n                  \"created_at\": \"2019-08-21 20:31:10\",\n                  \"updated_at\": \"2019-08-21 20:31:10\"\n              },\n              {\n                  \"id\": 2,\n                  \"product_id\": 14,\n                  \"image\": \"/images/cde4a713c55edd651e921a3dbe7a53f6.jpeg\",\n                  \"created_at\": \"2019-08-21 20:31:10\",\n                  \"updated_at\": \"2019-08-21 20:31:10\"\n              },\n              {\n                  \"id\": 3,\n                  \"product_id\": 14,\n                  \"image\": \"/images/fa0999082f53ae12e7c65d26b63d9c61.jpeg\",\n                  \"created_at\": \"2019-08-28 23:04:24\",\n                  \"updated_at\": \"2019-08-28 23:04:24\"\n              },\n              {\n                  \"id\": 4,\n                  \"product_id\": 14,\n                  \"image\": \"/images/ecab2622d712d000026cf1f88db76621.jpeg\",\n                  \"created_at\": \"2019-08-28 23:09:35\",\n                  \"updated_at\": \"2019-08-28 23:09:35\"\n              }\n          ],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n              \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      },\n      {\n          \"id\": 13,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n             \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      },\n      {\n          \"id\": 12,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"is_favorite\": false,\n          \"distance\": 152.88,\n          \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n              \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      }\n     \n    \n  ],\n  \"links\": {\n      \"first\": \"http://tokyo.test/api/products?page=1\",\n      \"last\": \"http://tokyo.test/api/products?page=1\",\n      \"prev\": null,\n      \"next\": null\n  },\n  \"meta\": {\n      \"current_page\": 1,\n      \"from\": 1,\n      \"last_page\": 1,\n      \"path\": \"http://tokyo.test/api/products\",\n      \"per_page\": 15,\n      \"to\": 14,\n      \"total\": 14\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products/search?per_page=10&order_by=id&sort=desc&distance=10",
    "title": "Search",
    "description": "<p>Show all products but not for current user</p>",
    "name": "Search",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "per_page",
            "description": "<p>[per_page=10] Number of results per page.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "id",
              "title",
              "price",
              "recent",
              "category_id",
              "rating"
            ],
            "optional": true,
            "field": "order_by",
            "description": "<p>Order by field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "asc",
              "desc"
            ],
            "optional": true,
            "field": "sort",
            "description": "<p>Sorting direction</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "distance",
            "description": "<p>Distance in KM</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "date_from",
            "description": "<p>Date from format YYYY-MM-DD</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "date_to",
            "description": "<p>Date to format YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>Is product added in user's favorite products.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "distance",
            "description": "<p>Product distance from user's location.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": [\n      {\n          \"id\": 14,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"is_favorite\": false,\n          \"distance\": 152.88,\n          \"images\": [\n              {\n                  \"id\": 1,\n                  \"product_id\": 14,\n                  \"image\": \"/images/9a61f579504ea5bc589546ecb3dadff8.jpeg\",\n                  \"created_at\": \"2019-08-21 20:31:10\",\n                  \"updated_at\": \"2019-08-21 20:31:10\"\n              },\n              {\n                  \"id\": 2,\n                  \"product_id\": 14,\n                  \"image\": \"/images/cde4a713c55edd651e921a3dbe7a53f6.jpeg\",\n                  \"created_at\": \"2019-08-21 20:31:10\",\n                  \"updated_at\": \"2019-08-21 20:31:10\"\n              },\n              {\n                  \"id\": 3,\n                  \"product_id\": 14,\n                  \"image\": \"/images/fa0999082f53ae12e7c65d26b63d9c61.jpeg\",\n                  \"created_at\": \"2019-08-28 23:04:24\",\n                  \"updated_at\": \"2019-08-28 23:04:24\"\n              },\n              {\n                  \"id\": 4,\n                  \"product_id\": 14,\n                  \"image\": \"/images/ecab2622d712d000026cf1f88db76621.jpeg\",\n                  \"created_at\": \"2019-08-28 23:09:35\",\n                  \"updated_at\": \"2019-08-28 23:09:35\"\n              }\n          ],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n              \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      },\n      {\n          \"id\": 13,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n             \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      },\n      {\n          \"id\": 12,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"is_favorite\": false,\n          \"distance\": 152.88,\n          \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n              \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      }\n     \n    \n  ],\n  \"links\": {\n      \"first\": \"http://tokyo.test/api/products?page=1\",\n      \"last\": \"http://tokyo.test/api/products?page=1\",\n      \"prev\": null,\n      \"next\": null\n  },\n  \"meta\": {\n      \"current_page\": 1,\n      \"from\": 1,\n      \"last_page\": 1,\n      \"path\": \"http://tokyo.test/api/products\",\n      \"per_page\": 15,\n      \"to\": 14,\n      \"total\": 14\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products?per_page=10&order_by=id&sort=desc",
    "title": "Show All",
    "description": "<p>Show all products for current user</p>",
    "name": "Show_All",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "per_page",
            "description": "<p>[per_page=10] Number of results per page.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "id",
              "title",
              "price",
              "category_id"
            ],
            "optional": true,
            "field": "order_by",
            "description": "<p>Order by field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "asc",
              "desc"
            ],
            "optional": true,
            "field": "sort",
            "description": "<p>Sorting direction</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>Is product added in user's favorite products.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "distance",
            "description": "<p>Product distance from user's location.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n  {\n  \"data\": [\n      {\n          \"id\": 14,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"images\": [\n              {\n                  \"id\": 1,\n                  \"product_id\": 14,\n                  \"image\": \"/images/9a61f579504ea5bc589546ecb3dadff8.jpeg\",\n                  \"created_at\": \"2019-08-21 20:31:10\",\n                  \"updated_at\": \"2019-08-21 20:31:10\"\n              },\n              {\n                  \"id\": 2,\n                  \"product_id\": 14,\n                  \"image\": \"/images/cde4a713c55edd651e921a3dbe7a53f6.jpeg\",\n                  \"created_at\": \"2019-08-21 20:31:10\",\n                  \"updated_at\": \"2019-08-21 20:31:10\"\n              },\n              {\n                  \"id\": 3,\n                  \"product_id\": 14,\n                  \"image\": \"/images/fa0999082f53ae12e7c65d26b63d9c61.jpeg\",\n                  \"created_at\": \"2019-08-28 23:04:24\",\n                  \"updated_at\": \"2019-08-28 23:04:24\"\n              },\n              {\n                  \"id\": 4,\n                  \"product_id\": 14,\n                  \"image\": \"/images/ecab2622d712d000026cf1f88db76621.jpeg\",\n                  \"created_at\": \"2019-08-28 23:09:35\",\n                  \"updated_at\": \"2019-08-28 23:09:35\"\n              }\n          ],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n              \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      },\n      {\n          \"id\": 13,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n             \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      },\n      {\n          \"id\": 12,\n          \"title\": \"Burmashine\",\n          \"description\": \"za budalene komsiite\",\n          \"price_per_day\": 3200,\n          \"deposit\": 500,\n          \"available_from\": \"2019-08-21\",\n          \"available_to\": \"2019-08-23\",\n          \"hour_from\": \"10:00\",\n          \"hour_to\": \"16:00\",\n          \"images\": [],\n          \"category\": {\n              \"id\": 13,\n              \"name\": \"Submain11\"\n          },\n          \"user\": {\n              \"id\": 1,\n              \"first_name\": \"John\",\n              \"last_name\": \"Snow\",\n              \"email\": \"johnsnow@winterfell.com\",\n              \"username\": \"john.snow\",\n              \"show_location\": 0,\n              \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n              \"roles\": []\n          }\n      }\n     \n    \n  ],\n  \"links\": {\n      \"first\": \"http://tokyo.test/api/products?page=1\",\n      \"last\": \"http://tokyo.test/api/products?page=1\",\n      \"prev\": null,\n      \"next\": null\n  },\n  \"meta\": {\n      \"current_page\": 1,\n      \"from\": 1,\n      \"last_page\": 1,\n      \"path\": \"http://tokyo.test/api/products\",\n      \"per_page\": 15,\n      \"to\": 14,\n      \"total\": 14\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products/:id/feedbacks",
    "title": "Show All Feedbacks",
    "description": "<p>Show all Product feedbacks</p>",
    "name": "Show_Feedbacks",
    "group": "Product",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating for the product feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment for the product.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments for the feedback.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product Object.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"data\": [\n    {\n    \"rating\": 5,\n    \"comment\": \"very good\",\n    \"comments\": [],\n    \"user\": {\n        \"id\": 6,\n        \"first_name\": \"MrsJaleel\",\n        \"last_name\": \"Schuppe\",\n        \"email\": \"Jaleel4@gmail.com\",\n        \"description\": null,\n        \"username\": \"Jaleel4@gmail.com\",\n        \"date_of_birth\": \"\",\n        \"phone\": \"\",\n        \"gender\": \"\",\n        \"avatar\": \"\",\n        \"show_location\": 1,\n        \"token\": \"S1lgtKyAPqKGVuVqqLrcEDudzcqkBVkZRvfVUSKvYobeiduw44ZQ8DVEfyXr\",\n        \"roles\": [\n            {\n            \"id\": 2,\n            \"name\": \"user\"\n            }\n        ],\n    },\n    \"product\": {\n        \"id\": 2,\n        \"title\": \"Product 2\",\n        \"description\": \"this is my sec product\",\n        \"price_per_day\": 25000,\n        \"deposit\": 9850,\n        \"available_from\": \"2019-11-01\",\n        \"available_to\": \"2019-11-07\",\n        \"hour_from\": \"10:00\",\n        \"hour_to\": \"16:00\",\n        \"is_favorite\": false,\n        \"distance\": 5985.64,\n        \"rating\": 5,\n        \"images\": [\n            {\n                \"id\": 1,\n                \"product_id\": 2,\n                \"image\": \"/images/89c53aca3dfc2bf7524d0bb7642a78e1.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            },\n            {\n                \"id\": 2,\n                \"product_id\": 2,\n                \"image\": \"/images/3abb8f80b51d63a2bd0cb9e3ab022b55.jpeg\",\n                \"created_at\": \"2019-11-08 20:21:01\",\n                \"updated_at\": \"2019-11-08 20:21:01\"\n            }\n        ],\n        \"category\": {\n            \"id\": 1,\n            \"name\": \"Main Category\"\n        },\n        \"user\": {\n            \"id\": 2,\n            \"first_name\": \"Marija1\",\n            \"last_name\": \"Kovacevic\",\n            \"email\": \"user@example.com\"\n        }\n        }\n    }\n    ],\n    \"links\": {\n        \"first\": \"http://rentout.test/api/products/2/feedbacks?page=1\",\n        \"last\": \"http://rentout.test/api/products/2/feedbacks?page=1\",\n        \"prev\": null,\n        \"next\": null\n    },\n    \"meta\": {\n        \"current_page\": 1,\n        \"from\": 1,\n        \"last_page\": 1,\n        \"path\": \"http://rentout.test/api/products/2/feedbacks\",\n        \"per_page\": 15,\n        \"to\": 1,\n        \"total\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductFeedbacksController.php",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/products/:id",
    "title": "Show Product",
    "description": "<p>Show product by ID</p>",
    "name": "Show_Product",
    "group": "Product",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "is_favorite",
            "description": "<p>Is product added in user's favorite products.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "distance",
            "description": "<p>Product distance from user's location.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\n  \"data\": {\n    \"id\": 10,\n    \"title\": \"Burmashine\",\n    \"description\": \"za budalene komsiite\",\n    \"price_per_day\": 3200,\n    \"deposit\": 500,\n    \"available_from\": \"2019-08-21\",\n    \"available_to\": \"2019-08-23\",\n    \"hour_from\": \"10:00\",\n    \"hour_to\": \"16:00\",\n    \"is_favorite\": false,\n    \"distance\": 152.88,\n    \"images\": [],\n    \"category\": {\n        \"id\": 13,\n        \"name\": \"Submain11\"\n    },\n    \"user\": {\n        \"id\": 1,\n        \"first_name\": \"John\",\n        \"last_name\": \"Snow\",\n        \"email\": \"johnsnow@winterfell.com\",\n        \"username\": \"john.snow\",\n        \"show_location\": 0,\n        \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n        \"roles\": []\n    }\n\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProductNotFound",
            "description": "<p>The id of the Product was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"errors\": \"Resource does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "put",
    "url": "/products/:id",
    "title": "Update product",
    "description": "<p>Update product by id</p>",
    "name": "Update",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Product status.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Product status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"data\": {\n    \"id\": 15,\n    \"title\": \"Black & Decker 500W Triming Machine\",\n    \"description\": \"For outdoor use only this machine is very lound and strong. Be careful!\",\n    \"price_per_day\": \"12000\",\n    \"deposit\": \"2000\",\n    \"available_from\": \"2019-10-10\",\n    \"available_to\": \"2019-10-15\",\n    \"hour_from\": \"10:00\",\n    \"hour_to\": \"16:00\",\n    \"images\": [\n        {\n            \"id\": 5,\n            \"product_id\": 15,\n            \"image\": \"/images/5de4b85eb314c69e25f54d5d40091397.png\",\n            \"created_at\": \"2019-10-11 14:54:03\",\n            \"updated_at\": \"2019-10-11 14:54:03\"\n        }\n    ],\n    \"category\": {\n        \"id\": 10,\n        \"name\": \"Main1\"\n    },\n    \"user\": {\n        \"id\": 1,\n        \"first_name\": \"John\",\n        \"last_name\": \"Snow\",\n        \"email\": \"johnsnow@winterfell.com\",\n        \"username\": \"john.snow\",\n        \"show_location\": 0,\n        \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n        \"roles\": []\n    }\n},\n\"message\": \"Resource was succesfuly created\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ProductNotFound.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Not Found:",
          "content": "HTTP/1.1 404 Not Found\n  {\n        \"errors\": \"Resource does not exist\"\n }",
          "type": "json"
        },
        {
          "title": "Unprocessable Entity:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n        \"errors\": {\n            \"title\": [\n                \"The title field is required.\"\n            ],\n            \"description\": [\n                \"The description field is required.\"\n            ],\n            \"category_id\": [\n                \"The category id field is required.\"\n            ],\n            \"price_per_day\": [\n                \"The price per day field is required.\"\n            ],\n            \"deposit\": [\n                \"The deposit field is required.\"\n            ],\n            \"available_from\": [\n                \"The available from field is required.\"\n            ],\n            \"available_to\": [\n                \"The available to field is required.\"\n            ],\n            \"hour_from\": [\n                \"The hour from field is required.\"\n            ],\n            \"hour_to\": [\n                \"The hour to field is required.\"\n            ],\n            \"images\": [\n                \"The images field is required.\"\n            ],\n           \"status\": [\n              \"The status field is required.\"\n            ],\n       }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "put",
    "url": "/products/{id}/status",
    "title": "Update status",
    "description": "<p>Update product status by id</p>",
    "name": "UpdateStatus",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "0",
              "1"
            ],
            "optional": false,
            "field": "status",
            "description": "<p>Product status.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Product name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Product description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "price_per_day",
            "description": "<p>Price per day in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "deposit",
            "description": "<p>Depoist in cents.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Product status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_from",
            "description": "<p>Date from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "available_to",
            "description": "<p>Date to.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_from",
            "description": "<p>Hour from.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "hour_to",
            "description": "<p>Hour to.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "images",
            "description": "<p>Product images.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.id",
            "description": "<p>Image ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "images.product_id",
            "description": "<p>Product ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "images.image",
            "description": "<p>Image path.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Product category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category.id",
            "description": "<p>Category ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category.name",
            "description": "<p>Category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Product owner</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user.id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.first_name",
            "description": "<p>User firstname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user.last_name",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"data\": {\n    \"id\": 15,\n    \"title\": \"Black & Decker 500W Triming Machine\",\n    \"description\": \"For outdoor use only this machine is very lound and strong. Be careful!\",\n    \"price_per_day\": \"12000\",\n    \"deposit\": \"2000\",\n    \"available_from\": \"2019-10-10\",\n    \"available_to\": \"2019-10-15\",\n    \"hour_from\": \"10:00\",\n    \"hour_to\": \"16:00\",\n    \"images\": [\n        {\n            \"id\": 5,\n            \"product_id\": 15,\n            \"image\": \"/images/5de4b85eb314c69e25f54d5d40091397.png\",\n            \"created_at\": \"2019-10-11 14:54:03\",\n            \"updated_at\": \"2019-10-11 14:54:03\"\n        }\n    ],\n    \"category\": {\n        \"id\": 10,\n        \"name\": \"Main1\"\n    },\n    \"user\": {\n        \"id\": 1,\n        \"first_name\": \"John\",\n        \"last_name\": \"Snow\",\n        \"email\": \"johnsnow@winterfell.com\",\n        \"username\": \"john.snow\",\n        \"show_location\": 0,\n        \"token\": \"QCbrBI2dcPpjAWkgVwwuMbGfkxHmobrv8qHcKLzozARpcpr5HwJLA2w5RTri\",\n        \"roles\": []\n    }\n},\n\"message\": \"Resource was succesfuly created\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ProductNotFound.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Not Found:",
          "content": "HTTP/1.1 404 Not Found\n  {\n        \"errors\": \"Resource does not exist\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ProductsController.php",
    "groupTitle": "Product"
  },
  {
    "type": "put",
    "url": "/user/all",
    "title": "All users",
    "description": "<p>Get All users</p>",
    "name": "AllYsers",
    "group": "User",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n     \"data\": [\n      {\n          \"id\": 12,\n          \"first_name\": \"Ruthie\",\n          \"last_name\": \"Schumm\",\n          \"email\": \"ruthie@buyer.com\",\n          \"rating\": {\n              \"rating\": 0,\n              \"num_ratings\": 0\n          }\n      },\n      {\n          \"id\": 11,\n          \"first_name\": \"John\",\n          \"last_name\": \"Quigley\",\n          \"email\": \"john@seller.com\",\n          \"rating\": {\n              \"rating\": 5,\n              \"num_ratings\": 3\n          }\n      },\n      {\n          \"id\": 10,\n          \"first_name\": \"Hristijan\",\n          \"last_name\": \"Stojanoski\",\n          \"email\": \"hs@dev.com\",\n          \"rating\": {\n              \"rating\": 0,\n              \"num_ratings\": 0\n          }\n      },\n      ],\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/avatar",
    "title": "Change avatar",
    "description": "<p>Change avatar as logged user</p>",
    "name": "ChangeAvatar",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "avatar",
            "description": "<p>base64 encoded image</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"avatar\": \"/images/8213f121fc70398104c14d9be17a9c6e.jpeg\"              \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"avatar\": [\n              \"The avatar field is required.\"\n          ]                            \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/password",
    "title": "Change password",
    "description": "<p>Change password as logged user</p>",
    "name": "ChangePassword",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "old_password",
            "description": "<p>Current password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>New password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"message\": \"Please check your email\"              \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"old_password\": [\n              \"The old password field is required.\"\n          ],\n          \"new_password\": [\n              \"The new password field is required.\"\n          ]                  \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/phone",
    "title": "Change phone",
    "description": "<p>Change phone as logged user</p>",
    "name": "ChangePhone",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"phone\": \"23545235\"           \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"phone\": [\n              \"The phone field is required.\"\n          ]                            \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user",
    "title": "Edit",
    "description": "<p>Edit user info</p>",
    "name": "EditUser",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>User description.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>User gender.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date_of_birth",
            "description": "<p>Date of birth.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>User phone.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "api_token",
            "description": "<p>Authentication token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "roles",
            "description": "<p>User roles information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "roles.id",
            "description": "<p>Role ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roles.name",
            "description": "<p>Role name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 12,\n          \"first_name\": \"Aegon\",\n          \"last_name\": \"Targaryen\",\n          \"email\": \"johnsnow@winterfell.com\",\n          \"username\": \"john.snow\",\n          \"token\": \"OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg\",\n          \"roles\": [\n              {\n                  \"id\": 2,\n                  \"name\": \"user\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 422 Unprocessable Entity\n{\n    \"errors\": {\n        \"first_name\": [\n            \"The first name field is required.\"\n        ],\n        \"last_name\": [\n            \"The last name field is required.\"\n        ],\n        \"gender\": [\n            \"The gender field is required.\"\n        ],\n        \"date_of_birth\": [\n            \"The date of birth field is required.\"\n        ],\n        \"phone\": [\n            \"The phone field is required.\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/forgotpassword",
    "title": "Forgot password",
    "description": "<p>Send request to email to reset password</p>",
    "name": "ForgotPassword",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"message\": \"Please check your email\"              \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"email\": [\n              \"The email field is required.\"\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ForgotpasswordController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "description": "<p>Login as admin or tenant</p>",
    "name": "LoginUser",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authentication token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "roles",
            "description": "<p>User roles information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "roles.id",
            "description": "<p>Role ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roles.name",
            "description": "<p>Role name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 12,\n          \"first_name\": \"John\",\n          \"last_name\": \"Snow\",\n          \"email\": \"johnsnow@winterfell.com\",\n          \"username\": \"john.snow\",\n          \"token\": \"OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg\",\n          \"roles\": [\n              {\n                  \"id\": 2,\n                  \"name\": \"user\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"email\": [\n              \"The email field is required.\"\n          ],\n          \"password\": [\n              \"The password field is required.\"\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/LoginController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register",
    "description": "<p>Register new user</p>",
    "name": "RegisterUser",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirm",
            "description": "<p>Password confirmation.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>Address.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "zip",
            "description": "<p>Postal code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authentication token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "roles",
            "description": "<p>User roles information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "roles.id",
            "description": "<p>Role ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roles.name",
            "description": "<p>Role name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 12,\n          \"first_name\": \"John\",\n          \"last_name\": \"Snow\",\n          \"email\": \"johnsnow@winterfell.com\",\n          \"username\": \"john.snow\",\n          \"token\": \"OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg\",\n          \"roles\": [\n              {\n                  \"id\": 2,\n                  \"name\": \"user\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"first_name\": [\n              \"The first name field is required.\"\n          ],\n          \"last_name\": [\n              \"The last name field is required.\"\n          ],\n          \"email\": [\n              \"The email field is required.\"\n          ],\n          \"username\": [\n              \"The username field is required.\"\n          ],\n          \"password\": [\n              \"The password field is required.\"\n          ],\n          \"password_confirm\": [\n              \"The password confirm field is required.\"\n          ],\n          \"country_id\": [\n              \"The country id field is required.\"\n          ],\n          \"address_1\": [\n              \"The address 1 field is required.\"\n          ],\n          \"zip\": [\n              \"The zip field is required.\"\n          ],\n          \"city\": [\n              \"The city field is required.\"\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/RegisterController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/forgotpassword/reset",
    "title": "Reset password",
    "description": "<p>Reset password using token</p>",
    "name": "ResetPassword",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token recieved via email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>New password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirm",
            "description": "<p>Repeat password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"message\": \"Password was changed\"              \n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n  {\n      \"errors\": {\n          \"token\": [\n              \"The token field is required.\"\n          ],\n          \"password\": [\n              \"The password field is required.\"\n          ],\n          \"password_confirm\": [\n              \"The password confirm field is required.\"\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ForgotpasswordController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/social/login",
    "title": "Social login",
    "description": "<p>Login with social network</p>",
    "name": "SocialLogin",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "facebook",
              "google"
            ],
            "optional": false,
            "field": "provider",
            "description": "<p>Social network provider ().</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>access token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authentication token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "roles",
            "description": "<p>User roles information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "roles.id",
            "description": "<p>Role ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roles.name",
            "description": "<p>Role name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 12,\n          \"first_name\": \"John\",\n          \"last_name\": \"Snow\",\n          \"email\": \"johnsnow@winterfell.com\",\n          \"username\": \"john.snow\",\n          \"token\": \"OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg\",\n          \"roles\": [\n              {\n                  \"id\": 2,\n                  \"name\": \"user\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/Api/SocialLogin/LoginController.php",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user",
    "title": "User",
    "description": "<p>Get user info</p>",
    "name": "UserInfo",
    "group": "User",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>First name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>User info.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authentication token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "roles",
            "description": "<p>User roles information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "roles.id",
            "description": "<p>Role ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "roles.name",
            "description": "<p>Role name.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"data\": {\n          \"id\": 12,\n          \"first_name\": \"John\",\n          \"last_name\": \"Snow\",\n          \"email\": \"johnsnow@winterfell.com\",\n          \"description\" : \"dsadsa\",\n          \"username\": \"john.snow\",\n          \"token\": \"OEZVappGCAMUSgtafA7BUEHPZVTSnIcTVT7IyO3IQ40tPpIDSVz0Uaj7Dskg\",\n          \"roles\": [\n              {\n                  \"id\": 2,\n                  \"name\": \"user\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/user/report",
    "title": "Report user",
    "description": "<p>Report and Unreport User</p>",
    "name": "UserReporting",
    "group": "User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>User ID that is being reported.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"data\": [],\n\"message\": \"User was unreported\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>Validation errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n {\n    \"errors\": {\n        \"user_id\": [\n            \"The user_id field is required.\"\n        ]\n   }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserReportsController.php",
    "groupTitle": "User"
  }
] });
