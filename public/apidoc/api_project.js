define({
  "name": "Rentout API",
  "version": "1.0.0",
  "description": "API Documentation for Rentout API",
  "title": "Rentout API",
  "url": "http://api.apprentout.com/api",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-02-05T19:58:23.565Z",
    "url": "http://apidocjs.com",
    "version": "0.20.0"
  }
});
